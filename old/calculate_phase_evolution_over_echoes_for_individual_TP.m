clear all, close all, clc


%% EPI
PE_dir = 'PA'; % in case of AP, --unwarpdir=y- for fugue
TE_diff = 3; %ms
echoes = [5 6];
epi_variant = 'cmrr';
combination = 'storedPO';
dwell_time = 0.00039; %s effective echo spacing divided by grappa factor

% data.read_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855';
% data.readfile_subdirs = {sprintf('/%s_TE%i/%s/', epi_variant, echoes(1)*TE_diff, combination), sprintf('/%s_TE%i/%s/', epi_variant, echoes(2)*TE_diff, combination)}; 
% data.write_dir = fullfile(data.read_dir,'/phase_evolution_over_echoes', sprintf('%s_%s/singleTP', epi_variant, combination));

data.read_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/';
data.readfile_subdirs = {'/cmrr_TE40/storedPO_PA','/cmrr_TE43/storedPO_PA'}; 
data.write_dir = fullfile(data.read_dir,'/phase_evolution_cmrr_separate_TP');
 
data.filename_save = fullfile(data.write_dir, sprintf('compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
data.filename_save_mag = fullfile(data.write_dir, sprintf('m_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));

data.filename_save_unwrapped = fullfile(data.write_dir, sprintf('unw_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
data.filename_save_unwrapped_DC = fullfile(data.write_dir, sprintf('DC_unw_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));

data.filename_epi_fm_noTEcorr = fullfile(data.write_dir,  sprintf('fieldmap_%i_%i_noTEcorr.nii', echoes(2), echoes(1)));

data.filename_GE_fm = fullfile(data.read_dir, '/phase_evolution_over_echoes/GE_144x144x48/fieldmap_2_1.nii'); 
filename_mask_file = fullfile(data.read_dir,'/eroded_epi_mask.nii');

data.filename_mag_1 = fullfile(data.read_dir, data.readfile_subdirs(1), 'mag', 'Image_rescaled.nii');
data.filename_phase_1 = fullfile(data.read_dir, data.readfile_subdirs(1), 'phase', 'Image_rescaled.nii');
data.filename_mag_2 = fullfile(data.read_dir, data.readfile_subdirs(2), 'mag', 'Image_rescaled.nii');
data.filename_phase_2 = fullfile(data.read_dir, data.readfile_subdirs(2), 'phase', 'Image_rescaled.nii');

phase_1_nii = load_nii(data.filename_phase_1{1});
phase_1 = phase_1_nii.img;
mag_1_nii = load_nii(data.filename_mag_1{1});
mag_1 = single(mag_1_nii.img);
     
phase_2_nii = load_nii(data.filename_phase_2{1});
phase_2 = phase_2_nii.img;
mag_2_nii = load_nii(data.filename_mag_2{1});
mag_2 = single(mag_2_nii.img);

compl_1 = single(1i * phase_1);
compl_1 = exp(compl_1);
compl_1 = mag_1 .* compl_1;

compl_2 = single(1i * phase_2);
compl_2 = exp(compl_2);
compl_2 = mag_2 .* compl_2;

dim = size(compl_1);
hermitian = zeros(dim(1:3));
complexDifference = compl_2(:,:,:,:).*conj(compl_1(:,:,:,:));

compl_diff_nii = make_nii(angle(complexDifference));
save_nii(compl_diff_nii, data.filename_save);

m_compl_diff_nii = make_nii(abs(complexDifference));
save_nii(m_compl_diff_nii, data.filename_save_mag);

unwrap_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; prelude -a %s -p %s -o %s -m %s -v',  unix_format(data.filename_mag_1{1}), unix_format(data.filename_save), unix_format(data.filename_save_unwrapped), unix_format(filename_mask_file)) % BB modified
[res, message] = unix(unwrap_command);
disp(res);

DC_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fugue -i %s --dwell=%f --loadfmap=%s -u %s -v', unix_format(data.filename_save_unwrapped), dwell_time, unix_format(data.filename_GE_fm), unix_format(data.filename_save_unwrapped_DC)) % BB modified
[res, message] = unix(DC_command);
disp(res);

TE_diff_s = TE_diff/1000; %ms

rescale_to_fm_command_noTEcorr = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fslmaths %s -div %f %s', unix_format(data.filename_save_unwrapped_DC), TE_diff_s, unix_format(data.filename_epi_fm_noTEcorr)) % BB modified
[res, message] = unix(rescale_to_fm_command_noTEcorr);
disp(res);
