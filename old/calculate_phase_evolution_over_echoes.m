clear all, close all, clc

%% GE
% data.read_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19701007SMRB_201711021515';
% data.readfile_subdir = '/GE_AP'; 
% data.write_dir = fullfile(data.read_dir,'/phase_evolution_over_echoes');
% mask_file=fullfile(data.read_dir,data.readfile_subdir,'/eroded_mask.nii');
% echoes = [4 5];
% TE_diff = 7; %ms
% 
% data.filename_mag = fullfile(data.read_dir, data.readfile_subdir,'aspire', 'mag', 'Image_rescaled.nii');
% data.filename_phase = fullfile(data.read_dir, data.readfile_subdir,'aspire', 'phase', 'Image_rescaled.nii');
% data.filename_save = fullfile(data.write_dir,  data.readfile_subdir, sprintf('compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
% data.filename_save_unwrapped = fullfile(data.write_dir,  data.readfile_subdir, sprintf('unw_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
% data.fm_filename = fullfile(data.write_dir,  data.readfile_subdir, sprintf('fieldmap_%i_%i.nii', echoes(2), echoes(1)));
% 
% phase_nii = load_nii(data.filename_phase);
% phase = phase_nii.img;
% mag_nii = load_nii(data.filename_mag);
% mag = mag_nii.img;
% 
% compl = single(1i * phase);
% compl = exp(compl);
% compl = mag .* compl;
% 
% dim = size(compl);
% hermitian = zeros(dim(1:3));
% complexDifference = compl(:,:,:,echoes(2)).*conj(compl(:,:,:,echoes(1)));
% 
% compl_diff_nii = make_nii(angle(complexDifference));
% save_nii(compl_diff_nii, data.filename_save);
% 
% unwrap_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; prelude -a %s -p %s -o %s -m %s -v',  unix_format(data.filename_mag), unix_format(data.filename_save), unix_format(data.filename_save_unwrapped), unix_format(mask_file)) % BB modified
% [res, message] = unix(unwrap_command);
% disp(res);
% 
% TE_diff_s = TE_diff/1000; %ms
% 
% rescale_to_fm_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fslmaths %s -div %f %s', unix_format(data.filename_save_unwrapped), TE_diff_s, unix_format(data.fm_filename)) % BB modified
% [res, message] = unix(rescale_to_fm_command);
% disp(res);
% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EPI
PE_dir = 'AP'; % in case of AP, --unwarpdir=y- for fugue
TE_diff = 7; %ms
epi_variant = 'cmrr';
combination = 'storedPO_mean_PA_AP';
dwell_time = 0.00039; %s effective echo spacing divided by grappa factor

data.read_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19701007SMRB_201711021515';
data.readfile_subdirs = {'/storedPO_mean_PA_AP/cmrr_TE28_AP','/storedPO_mean_PA_AP/cmrr_TE35_AP'}; 
data.write_dir = fullfile(data.read_dir,'/phase_evolution_over_echoes/cmrr_AP_GE_5_4');
 
data.filename_save = fullfile(data.write_dir, 'compl_diff.nii');
data.filename_save_mag = fullfile(data.write_dir, 'm_compl_diff.nii');

data.filename_save_unwrapped = fullfile(data.write_dir, 'unw_compl_diff.nii');
data.filename_save_unwrapped_averaged = fullfile(data.write_dir, 'mean_unw_compl_diff.nii');
data.filename_save_unwrapped_averaged_DC = fullfile(data.write_dir, 'DC_mean_unw_compl_diff.nii');

data.filename_epi_fm_noTEcorr = fullfile(data.write_dir,  'fieldmap_noTEcorr.nii');
data.filename_GE_fm = fullfile(data.read_dir, '/phase_evolution_over_echoes/GE_PA/fieldmap_5_4.nii'); 
filename_mask_file = fullfile(data.read_dir,'/storedPO_mean_PA_AP/eroded_mask_epi_PA.nii');

data.filename_mag_1 = fullfile(data.read_dir, data.readfile_subdirs(1), 'results', 'combined_mag_epi.nii');
data.filename_phase_1 = fullfile(data.read_dir, data.readfile_subdirs(1), 'results', 'combined_phase_epi.nii');
data.filename_mag_2 = fullfile(data.read_dir, data.readfile_subdirs(2), 'results', 'combined_mag_epi.nii');
data.filename_phase_2 = fullfile(data.read_dir, data.readfile_subdirs(2), 'results', 'combined_phase_epi.nii');

% data.read_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855';
% data.readfile_subdirs = {sprintf('/%s_TE%i/%s/', epi_variant, echoes(1)*TE_diff, combination), sprintf('/%s_TE%i/%s/', epi_variant, echoes(2)*TE_diff, combination)}; 
% data.write_dir = fullfile(data.read_dir,'/phase_evolution_over_echoes', sprintf('%s_%s/singleTP', epi_variant, combination));

% data.read_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/';
% data.readfile_subdirs = {'/cmrr_TE31/storedPO_PA','/cmrr_TE34/storedPO_PA'}; 
% data.write_dir = fullfile(data.read_dir,'/phase_evolution_cmrr');
 
% data.filename_save = fullfile(data.write_dir, sprintf('compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
% data.filename_save_mag = fullfile(data.write_dir, sprintf('m_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
% 
% data.filename_save_unwrapped = fullfile(data.write_dir, sprintf('unw_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
% data.filename_save_unwrapped_averaged = fullfile(data.write_dir, sprintf('mean_unw_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
% data.filename_save_unwrapped_averaged_DC = fullfile(data.write_dir, sprintf('mean_DC_unw_compl_diff_echoes_%i_%i.nii', echoes(2), echoes(1)));
% 
% data.filename_epi_fm_noTEcorr = fullfile(data.write_dir,  sprintf('fieldmap_%i_%i_noTEcorr.nii', echoes(2), echoes(1)));
% % data.filename_epi_fm_TEcorr_orig = fullfile(data.write_dir,  sprintf('fieldmap_%i_%i_TEcorr_orig_FM_5_4.nii', echoes(2), echoes(1)));
% % data.filename_epi_fm_TEcorr_inverted = fullfile(data.write_dir,  sprintf('fieldmap_%i_%i_TEcorr_inverted_FM_5_4.nii', echoes(2), echoes(1)));
% 
% data.filename_GE_fm = fullfile(data.read_dir, '/phase_evolution_over_echoes/GE_PA/fieldmap_2_1.nii'); 
% filename_mask_file = fullfile(data.read_dir,'/eroded_epi_mask.nii');
% % data.filename_TE_local_orig = fullfile(data.read_dir, '/phase_evolution_over_echoes', 'TE_local_norm_notFlagged_mul0.007_orig_FM_5_4.nii'); 
% % data.filename_TE_local_inverted = fullfile(data.read_dir, '/phase_evolution_over_echoes', 'TE_local_norm_notFlagged_mul0.007_inverted_FM_5_4.nii'); 
% 
% data.filename_mag_1 = fullfile(data.read_dir, data.readfile_subdirs(1), 'mag', 'Image_rescaled.nii');
% data.filename_phase_1 = fullfile(data.read_dir, data.readfile_subdirs(1), 'phase', 'Image_rescaled.nii');
% data.filename_mag_2 = fullfile(data.read_dir, data.readfile_subdirs(2), 'mag', 'Image_rescaled.nii');
% data.filename_phase_2 = fullfile(data.read_dir, data.readfile_subdirs(2), 'phase', 'Image_rescaled.nii');

s = mkdir(data.write_dir);
if s == 0
    error('No permission to make directory %s\n', data.write_dir);
end
    
phase_1_nii = load_nii(data.filename_phase_1{1});
phase_1 = phase_1_nii.img;
mag_1_nii = load_nii(data.filename_mag_1{1});
mag_1 = single(mag_1_nii.img);
     
phase_2_nii = load_nii(data.filename_phase_2{1});
phase_2 = phase_2_nii.img;
mag_2_nii = load_nii(data.filename_mag_2{1});
mag_2 = single(mag_2_nii.img);

compl_1 = single(1i * phase_1);
compl_1 = exp(compl_1);
compl_1 = mag_1 .* compl_1;

compl_2 = single(1i * phase_2);
compl_2 = exp(compl_2);
compl_2 = mag_2 .* compl_2;

dim = size(compl_1);
hermitian = zeros(dim(1:3));
complexDifference = compl_2(:,:,:,:).*conj(compl_1(:,:,:,:));

compl_diff_nii = make_nii(angle(complexDifference));
save_nii(compl_diff_nii, data.filename_save);

m_compl_diff_nii = make_nii(abs(complexDifference));
save_nii(m_compl_diff_nii, data.filename_save_mag);

unwrap_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; prelude -a %s -p %s -o %s -m %s -v',  unix_format(data.filename_mag_1{1}), unix_format(data.filename_save), unix_format(data.filename_save_unwrapped), unix_format(filename_mask_file)) % BB modified
[res, message] = unix(unwrap_command);
disp(res);

average_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fslmaths %s -Tmean %s', unix_format(data.filename_save_unwrapped),unix_format(data.filename_save_unwrapped_averaged)) % BB modified
[res, message] = unix(average_command);
disp(res);

DC_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fugue -i %s --dwell=%f --loadfmap=%s --unwarpdir=y- -u %s -v', unix_format(data.filename_save_unwrapped_averaged), dwell_time, unix_format(data.filename_GE_fm), unix_format(data.filename_save_unwrapped_averaged_DC)) % BB modified
[res, message] = unix(DC_command);
disp(res);

TE_diff_s = TE_diff/1000; %ms

rescale_to_fm_command_noTEcorr = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fslmaths %s -div %f %s', unix_format(data.filename_save_unwrapped_averaged_DC), TE_diff_s, unix_format(data.filename_epi_fm_noTEcorr)) % BB modified
[res, message] = unix(rescale_to_fm_command_noTEcorr);
disp(res);
% % 
% % rescale_to_fm_command_TEcorr_orig = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fslmaths %s -div %s %s', unix_format(data.filename_save_unwrapped_averaged_DC), unix_format(data.filename_TE_local_orig), unix_format(data.filename_epi_fm_TEcorr_orig)) % BB modified
% % [res, message] = unix(rescale_to_fm_command_TEcorr_orig);
% % disp(res);
% % 
% % rescale_to_fm_command_TEcorr_inverted = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fslmaths %s -div %s %s', unix_format(data.filename_save_unwrapped_averaged_DC), unix_format(data.filename_TE_local_inverted), unix_format(data.filename_epi_fm_TEcorr_inverted)) % BB modified
% % [res, message] = unix(rescale_to_fm_command_TEcorr_inverted);
% % disp(res);