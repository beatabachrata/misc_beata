main_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19790301PDCR_20200123_abdomen/segmentation/';
segm_name = 'Segmentation_preview_label.nii';

segm_nii = load_nii(fullfile(main_dir,segm_name));
segm = single(segm_nii.img());

segm(segm==2) = 0;
segm(segm==1) = 0;
segm(segm==3) = 1;

% 
% for i = size(segm,3):-1:1
%     segm(:,:,i+1) = segm(:,:,i);
% 
%     if (i == 1)  
%         segm(:,:,i) = 1;
%     end
% end
% 
% 
% segm(size(segm,1):288,:,:) = 1;


save_nii(make_nii(segm),fullfile(main_dir,'mask_nice.nii'))














