clc,clear,close all;
phase_struct = load('/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/phase_values_GEvsEPI_phantom.mat');
phase = phase_struct.phase;

%% increasing phase
plot(phase(1,1:6),phase(8,1:6),':g*')
hold on
plot(phase(1,4:6),phase(9,4:6),':go')
plot(phase(1,4:6),phase(10,4:6),':gx')

plot(phase(1,1:6),phase(11,1:6),':k*')
plot(phase(1,4:6),phase(12,4:6),':ko')
plot(phase(1,4:6),phase(13,4:6),':kx')

plot(phase(1,1:6),phase(23,1:6),':r*')
plot(phase(1,4:6),phase(24,4:6),':ro')
plot(phase(1,4:6),phase(25,4:6),':rx')

%% slowly increasing phase
figure 
plot(phase(1,1:6),phase(5,1:6),':r*')
hold on
plot(phase(1,4:6),phase(6,4:6),':ro')
plot(phase(1,4:6),phase(7,4:6),':r*')

plot(phase(1,1:6),phase(2,1:6),':g*')
plot(phase(1,4:6),phase(3,4:6),':go')
plot(phase(1,4:6),phase(4,4:6),':g*')

%% decreasing phase
figure 
plot(phase(1,1:6),phase(14,1:6),':k*')
hold on
plot(phase(1,4:6),phase(15,4:6),':ko')
plot(phase(1,4:6),phase(16,4:6),':kx')

plot(phase(1,1:6),phase(17,1:6),':g*')
plot(phase(1,4:6),phase(18,4:6),':go')
plot(phase(1,4:6),phase(19,4:6),':gx')

plot(phase(1,1:6),phase(20,1:6),':r*')
plot(phase(1,4:6),phase(21,4:6),':ro')
plot(phase(1,4:6),phase(22,4:6),':rx')

