clc,clear,close all;
data_8 = load('/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/fmri_qsm/figures/QSMs/fig_GE_vs_EPI/ROIs_GE_vs_EPI_8only.mat');
data_8_trans = data_8.M.';
means_8 = data_8_trans (2:2:end,:); 
stds_8 = data_8_trans (3:2:end,:); 

figure 
errorbar(1:8,means_8(:,1),stds_8(:,1),':.k','MarkerSize',15); % GE (VRC, 2nd echo)
hold;
errorbar(1:8,means_8(:,2),stds_8(:,2),':.r','MarkerSize',15); % ep2d_MB1_task (average MC)
errorbar(1:8,means_8(:,3),stds_8(:,3),':.b','MarkerSize',15); % ep2d_MB2_task (average MC)
errorbar(1:8,means_8(:,4),stds_8(:,4),':.g','MarkerSize',15); % ep3d_task (average MC)
legend('GE', '2D EPI MB1','2D EPI MB2','3D EPI'); 
xlabel('ROI','FontSize',11);
ylabel('Mean susceptibility over ROI [ppm]','FontSize',11);
title('Comparison of estimated susceptibilities','FontSize',11);
xtix = {'1','2','3','4','8','9','11','12'};   % Your labels
xtixloc = [1 2 3 4 5 6 7 8];      % Your label locations
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);

anova1(means_8(2:end,:));
xtix = {'GE','2D EPI MB1','2D EPI MB2','3D EPI'};   % Your labels
xtixloc = [1 2 3 4];      % Your label locations
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
ylabel('Mean susceptibility over ROI [ppm]','FontSize',11);
title('Analysis of variance','FontSize',11);



data_all = load('/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/fmri_qsm/figures/QSMs/fig_GE_vs_EPI/ROIs_GE_vs_EPI_all.mat');
data_all_trans = data_all.M.';
means_all = data_all_trans (2:2:end,:); 
stds_all = data_all_trans (3:2:end,:);

figure 
errorbar(1:11,means_all(:,1),stds_all(:,1),':.k','MarkerSize',15); % GE (VRC, 2nd echo)
hold;
errorbar(1:11,means_all(:,2),stds_all(:,2),':.r','MarkerSize',15); % ep2d_MB1_task (average MC)
errorbar(1:11,means_all(:,3),stds_all(:,3),':.b','MarkerSize',15); % ep2d_MB2_task (average MC)
errorbar(1:11,means_all(:,4),stds_all(:,4),':.g','MarkerSize',15); % ep3d_task (average MC)
legend('GE', '2D EPI MB1','2D EPI MB2','3D EPI'); 
xlabel('ROI','FontSize',11);
ylabel('Mean susceptibility over ROI [ppm]','FontSize',11);
title('Comparison of estimated susceptibilities','FontSize',11);


anova1(means_all(2:end,:));
xtix = {'GE','2D EPI MB1','2D EPI MB2','3D EPI'};   % Your labels
xtixloc = [1 2 3 4];      % Your label locations
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
ylabel('Mean susceptibility over ROI [ppm]','FontSize',11);
title('Analysis of variance','FontSize',11);



figure 
errorbar(1:12,means_all(:,1),stds_all(:,1),':.k','MarkerSize',15); % GE (VRC, 2nd echo)
hold;
errorbar(1:12,means_all(:,2),stds_all(:,2),':.r','MarkerSize',15); % ep2d_MB1_task (average MC)
errorbar(1:12,means_all(:,3),stds_all(:,3),':.b','MarkerSize',15); % ep2d_MB2_task (average MC)
errorbar(1:12,means_all(:,4),stds_all(:,4),':.g','MarkerSize',15); % ep3d_task (average MC)
legend('ASPIRE echo 1', 'ASPIRE echo 2','VRC echo 1','VRC echo 2'); 
xlabel('ROI','FontSize',11);
ylabel('Mean susceptibility over ROI [ppm]','FontSize',11);

