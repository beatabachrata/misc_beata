%% Housekeeping & Definitions
clear
close all;

names = {};
dirs = {};

%% Define data paths and acquisition orientation
for scan = 14:16
    
    orient = 2; % 1-sag, 2-trans, 3-cor
    PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
    phase_oversampling = 0;
    is2D = 1;
    PE_FOV_fraction = 1;
    isTurbo = 0;
    
    switch scan 
        case 1
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19921204YVFD_20200601_breasts/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19921204YVFD_20200601_breasts/dats/meas_MID01674_FID100448_ke_gre_aspireTest_TE11_3_TR95.dat';
            output_name = 'conventional_TE11p3_TE6p8.nii';
            PE_dir = 3;             
        case 2
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19930611LNNH_20200602_breasts/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19930611LNNH_20200602_breasts/dats/meas_MID00057_FID100790_ke_gre_aspireTest_TE11_3_TR95.dat';
            output_name = 'conventional_TE11p3.nii';
            PE_dir = 3;           
        case 3
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19750413ABSH_20200604_abdomen/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19750413ABSH_20200604_abdomen/dats/meas_MID00507_FID101300_bb_caipigre_LinPha16_24us_TE11_3_TR95.dat';
            output_name = 'linPha16p24ms_aliased_TE11p3.nii';
        case 4
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19931027MRVS_20200605_breasts/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19931027MRVS_20200605_breasts/dats/meas_MID00583_FID101383_ke_gre_aspireTest_TE11_3_TR95.dat';
            output_name = 'conventional_TE11p3.nii';
            PE_dir = 3;             
        case 5
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19810706EAPB_20200612_abdomen/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19810706EAPB_20200612_abdomen/dats/meas_MID00106_FID102193_ke_gre_aspireTest_TE6_8_TR110.dat';
            output_name = 'conventional_TE6p8.nii';
        case 6
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19790301PDCR_20200609_abdomenSNR/nii_from_dats/dixon_longTR/scan1/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19790301PDCR_20200609_abdomenSNR/dats/meas_MID00952_FID101755_gre_2D_3ptDixon_1stTE2p2_spacing3p1_rBW540_TR110_scan1.dat';
            output_name = 'Image.nii';            
        case 7
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/tse/19940402MXRE_20200611_knee/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19940402MXRE_20200611_knee/dats/meas_MID01264_FID102067_li_sag_PD_tse_fatSat_strong.dat';
            output_name = 'fatSat_TE12_hamming35.nii';
            orient = 1; % 1-sag, 2-trans, 3-cor
            PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
            isTurbo = 1;
        case 8
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19940402MXRE_20200611_knee/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19940402MXRE_20200611_knee/dats/meas_MID01278_FID102081_gre_2D_fatSat.dat';
            output_name = 'fatSat.nii';
            orient = 1; % 1-sag, 2-trans, 3-cor
            PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
        case 9
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/tse/19870704RDKE_20200617_knee/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19870704RDKE_20200617_knee/dats/meas_MID00028_FID47134_li_sag_PD_tse.dat';
            output_name = 'conventional.nii';
            orient = 1; % 1-sag, 2-trans, 3-cor
            PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
            isTurbo = 1;
        case 10
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/tse/19930214TNNH_20200618_knee/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19930214TNNH_20200618_knee/dats/meas_MID00415_FID102706_li_sag_PD_tse.dat';
            output_name = 'conventional.nii';
            orient = 1; % 1-sag, 2-trans, 3-cor
            PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
            isTurbo = 1;
        case 11
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19860423TTKR_20200618_abdomen/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19860423TTKR_20200618_abdomen/dats/meas_MID00454_FID102745_bb_caipigre_LinPha16_24us_TE11_3_TR95.dat';
            output_name = 'linPha16p24ms_aliased_TE11p3.nii';
        case 12
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20200624/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20200624/dats/meas_MID00233_FID47803_bb_caipigre_3D_inPha_noPat_forQSM.dat';
            output_name = 'caipi_noPat.nii';
            orient = 1; 
            PE_dir = 2; 
            is2D = 0;
            
        case 13
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/testPipeline/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20200824/dats/meas_MID00047_FID52315_gre_TE_7_14_21_192x192.dat';
            output_name = 'gre_noPat.nii';
            orient = 3; 
            PE_dir = 3; 
            is2D = 0;      
            
        case 14
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19931013SAMT_20210423_7Tcontrast/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19931013SAMT_20210423_7Tcontrast/dats/meas_MID00130_FID51785_bb_caipigre_RECT_100us_FA15.dat';
            orient = 1; 
            PE_dir = 2;   
            is2D = 0;      
            output_name = 'RECT_100us.nii';
        case 15
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19931013SAMT_20210423_7Tcontrast/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19931013SAMT_20210423_7Tcontrast/dats/meas_MID00131_FID51786_bb_caipigre_RECT_8000us_FA15.dat';
            orient = 1; 
            PE_dir = 2;   
            is2D = 0;      
            output_name = 'RECT_8000us.nii';
        case 16
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19931013SAMT_20210423_7Tcontrast/nii_from_dats/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/19931013SAMT_20210423_7Tcontrast/dats/meas_MID00132_FID51787_bb_caipigre_SBmin_8000us_FA15.dat';
            orient = 1; 
            PE_dir = 2;   
            is2D = 0;      
            output_name = 'SB_8000us.nii';
   end


    %% Create output directory
    [s,mess] = mkdir(output_dir);
    fprintf('Writing results to %s\n', output_dir);
    if s == 0
        error('No permission to make directory %s/m', output_dir);
    end
    

    %% Load the data      
    [kspace, params] = loadData(dat_file);  
    kspace = flipdim(flipdim(flipdim(kspace,1),2),3);
  
    params.orient = orient; clear orient    
    params.PE_dir = PE_dir; clear PE_dir    
    params.is2D = is2D; clear is2D
    
    %% Preparation steps 2
    kspace = zerofillPartialFourier(kspace,params); 
%     sliceAcquisitionOrder = getSliceAcquisitionOrder(kspace,params.is2D);
%     kspace = kspace(:,:,:,sliceAcquisitionOrder.',:); %% not always required
    kspace = flipdim(kspace,4);    

        
    %% Reconstruct and save images
%     save_nii_from_kspace(kspace, output_dir, output_name, params);
    save_nii_uncomb(kspace, output_dir, sprintf('uncomb_%s',output_name), params);

end


