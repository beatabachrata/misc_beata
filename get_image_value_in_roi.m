clear

%   pulse shape analysis 
root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/superresolution';
QSM_dir = 'QSM';
QSM_subdir = 'romeo_sharp_star_ivw';

subjects_dir = {'19891204EAHC_20211103_SR'};
graph_title = '';

unzip = false;

rois = {'caudate_left.nii','caudate_right.nii','putamen_left.nii','putamen_right.nii','pallidum_left.nii','pallidum_right.nii',...
        'thalamus_left.nii','thalamus_right.nii','red_nuclei_left.nii','red_nuclei_right.nii','sub_nigra_left.nii','sub_nigra_right.nii','dentate_nuclei_left.nii','dentate_nuclei_right.nii'};  

for iSubj = 1:length(subjects_dir)
    subject = subjects_dir(iSubj);
    j = 1;

for iScan = [1:2]
    
    switch iScan
        case 1
            corr_subdir = 'gre/2D/orig';
            image_name = 'sepia_QSM.nii';
%         case 2
%             corr_subdir = 'gre/2D/sim_triplanar/sr/minc_pipeline/';
%             image_name = 'sepia_QSM.nii';
%         case 3
%             corr_subdir = 'gre/2D/sim_triplanar/sr/volgenmodel_nonlin/';
%             image_name = 'sepia_QSM_192.nii';
%         case 4
%             corr_subdir = 'epi/siemensMB/iso';
%             image_name = 'sepia_QSM.nii';
%         case 5
%             corr_subdir = 'epi/siemensMB/sr/minc_pipeline';
%             image_name = 'sepia_QSM.nii';
        case 2
            corr_subdir = 'epi/siemensMB/sr/volgenmodel_nonlin';
            image_name = 'sepia_QSM_192.nii';
    end
    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   generate readfile names
read_dir = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, corr_subdir, QSM_subdir);
read_file = char(fullfile(read_dir, image_name));
write_dir = fullfile(root_dir, subjects_dir(iSubj), QSM_dir);

if (unzip)
    zip_read_file = fullfile(read_dir, sprintf('%s.gz',image_name));
    gunzip(zip_read_file,read_dir);
end

% load the image
ima_nii = load_nii(read_file);
ima = ima_nii.img(:,:,:); 
clear image_nii

% if multi-echo, use just the first echo
if (size(ima,4) > 1) 
    ima = squeeze(ima(:,:,:,1));
end

 
for iROI=1:size(rois,2)
    
    % load the ROIS
%     roi_name = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, 'std_rois', rois(iROI));       
    roi_name = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, 'rois', rois(iROI));       
    roi = load_nii(char(roi_name));
    roi = roi.img;
    
    ima_in_roi = ima(find(roi(:,:,:)~=0));
    
    ima_in_roi_allROI(iROI,1:length(ima_in_roi)) = ima_in_roi.';
    std_in_roi(iROI) = std(ima_in_roi);
    median_in_roi(iROI) = median(ima_in_roi);

end

ima_in_roi_allScans(j,:,:) = ima_in_roi_allROI;
median_in_roi_allScans(j,:) = median_in_roi;
std_in_roi_allScans(j,:) = std_in_roi;

j=j+1;

end

median_in_roi_allSubj(:,:,iSubj) = median_in_roi_allScans;
std_in_roi_allSubj(:,:,iSubj) = std_in_roi_allScans;

end
ima_in_roi_allScans(ima_in_roi_allScans==0) = NaN;

% mean over  all subjects
median_in_roi_allSubj_mean = (mean(median_in_roi_allSubj,3));
std_in_roi_allSubj_mean = (mean(std_in_roi_allSubj,3));


ima_in_roi_allScans = permute(ima_in_roi_allScans,[2,1,3]);
ima_in_roi_allScans = reshape(ima_in_roi_allScans,[(size(ima_in_roi_allScans,1)*size(ima_in_roi_allScans,2)),size(ima_in_roi_allScans,3)]);

figure
x = ima_in_roi_allScans.';
group = [1:28];
positions = [1:14,(1:14)+0.4];
boxplot(x,group, 'positions', positions,'Notch','on');

set(gca,'xtick',[1.2:14.2])
set(gca,'xticklabel',{'CN l','CN r','PUT l','PUT r','GP l','GP r','THA l','THA r','RN l','RN r','SN l','SN r','DN l','DN r'},'fontsize',12)
color = ['c','b', 'c', 'b', 'c','b', 'c', 'b', 'c','b', 'c', 'b', 'c','b', 'c', 'b', 'c','b', 'c', 'b', 'c','b', 'c', 'b', 'c','b', 'c', 'b',];

h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end

c = get(gca, 'Children');
hleg1 = legend(c(1:2), 'Reference HR GRE', 'SR nonLinVM EPI');
hleg1.FontSize = 12;

axis([0.6 14.8 -0.04 0.32]) % 
ylabel('susceptibility [ppm]','FontSize',14)
title('Susceptibility values within ROIs in deep gray matter structures','FontSize',16)

p = zeros(1,14);
h = p;
[p(1),h(1)]= ranksum(ima_in_roi_allScans(1,:),ima_in_roi_allScans(15,:),'alpha',0.05/14);
[p(2),h(2)]= ranksum(ima_in_roi_allScans(2,:),ima_in_roi_allScans(16,:),'alpha',0.05/14);
[p(3),h(3)]= ranksum(ima_in_roi_allScans(3,:),ima_in_roi_allScans(17,:),'alpha',0.05/14);
[p(4),h(4)]= ranksum(ima_in_roi_allScans(4,:),ima_in_roi_allScans(18,:),'alpha',0.05/14);
[p(5),h(5)]= ranksum(ima_in_roi_allScans(5,:),ima_in_roi_allScans(19,:),'alpha',0.05/14);
[p(6),h(6)]= ranksum(ima_in_roi_allScans(6,:),ima_in_roi_allScans(20,:),'alpha',0.05/14);
[p(7),h(7)]= ranksum(ima_in_roi_allScans(7,:),ima_in_roi_allScans(21,:),'alpha',0.05/14);
[p(8),h(8)]= ranksum(ima_in_roi_allScans(8,:),ima_in_roi_allScans(22,:),'alpha',0.05/14);
[p(9),h(9)]= ranksum(ima_in_roi_allScans(9,:),ima_in_roi_allScans(23,:),'alpha',0.05/14);
[p(10),h(10)]= ranksum(ima_in_roi_allScans(10,:),ima_in_roi_allScans(24,:),'alpha',0.05/14);
[p(11),h(11)]= ranksum(ima_in_roi_allScans(11,:),ima_in_roi_allScans(25,:),'alpha',0.05/14);
[p(12),h(12)]= ranksum(ima_in_roi_allScans(12,:),ima_in_roi_allScans(26,:),'alpha',0.05/14);
[p(13),h(13)]= ranksum(ima_in_roi_allScans(13,:),ima_in_roi_allScans(27,:),'alpha',0.05/14);
[p(14),h(14)]= ranksum(ima_in_roi_allScans(14,:),ima_in_roi_allScans(28,:),'alpha',0.05/14)
