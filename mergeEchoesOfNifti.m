function mergeEchoesOfNifti(output_dir,output_file_name,iEcho,deleteSeparateEchoes)
% BB: merge several separate echo images (saved by save_nii_from_kspace_separateecho.m function) 
% into one 4 dimensional, multi-echo image. 

    % stack combine image
    for i = 1:iEcho
        % save names of all echo images
        input_name = sprintf('%s_echo%i.nii', fullfile(output_dir, output_file_name), i);
        output_name = fullfile(output_dir,sprintf('%s.nii',output_file_name));
        
        nii = load_nii(input_name);
        img = nii.img();
        img_all(:,:,:,i) = img; 
    end
    save_nii(make_nii(img_all),output_name)
    clear img_all
    
    % stack uncombine magnitude image
    for i = 1:iEcho
        % save names of all echo images
        input_name = fullfile(output_dir,sprintf('mag_%s_uncomb_echo%i.nii', output_file_name, i));
        output_name = fullfile(output_dir,sprintf('mag_%s_uncomb.nii',output_file_name));
        
        nii = load_nii(input_name);
        img = nii.img();
        img_all(:,:,:,:,i) = img; 
    end
    save_nii(make_nii(img_all),output_name)
    clear img_all

    % stack uncombine phase image
    for i = 1:iEcho
        % save names of all echo images
        input_name = fullfile(output_dir,sprintf('phase_%s_uncomb_echo%i.nii', output_file_name, i));
        output_name = fullfile(output_dir,sprintf('phase_%s_uncomb.nii',output_file_name));

        nii = load_nii(input_name);
        img = nii.img();
        img_all(:,:,:,:,i) = img; 
    end
    save_nii(make_nii(img_all),output_name)

    % delete separate echo images
    if (deleteSeparateEchoes)    
        for i = 1:iEcho
            % save names of all echo images
            comb_name = sprintf('%s_echo%i.nii%s', fullfile(output_dir, output_file_name), i);
            uncomb_mag_name = fullfile(output_dir, sprintf('mag_%s_uncomb_echo%i.nii', output_file_name, i));
            uncomb_pha_name = fullfile(output_dir, sprintf('phase_%s_uncomb_echo%i.nii', output_file_name, i));

            delete_command = sprintf('rm %s %s %s', unix_format(comb_name), unix_format(uncomb_mag_name), unix_format(uncomb_pha_name));
            [res, message] = unix(delete_command);
        end
    end
end