% Based on plot_freq_grad function from Raj Ramasawmy (magnetom.net)
% Spectrum analysis of gradients, for acoustic resonance screening.
% gradients in mT (assuming single vector, will crop to g(:,1,1))
% NHBLI, 2021


% ===================================
% 7T Magnetom scanner acoustic ranges
% ===================================
% GC Type                                      = SC72CD
% GC AcousticResonanceFrequency[0]        [Hz] = 1100
% GC AcousticResonanceBandwidth[0]        [Hz] = 300
% GC AcousticResonanceFrequency[1]        [Hz] = 550
% GC AcousticResonanceBandwidth[1]        [Hz] = 100


% ===================================
% 3T Prisma scanner acoustic ranges
% ===================================
% GC Type                                      = AS82
% GC AcousticResonanceFrequency[0]        [Hz] = 590
% GC AcousticResonanceBandwidth[0]        [Hz] = 100
% GC AcousticResonanceFrequency[1]        [Hz] = 1140
% GC AcousticResonanceBandwidth[1]        [Hz] = 220


grad = load('/data2/beata/virtualShare/pulse_files/7T/simulation/Gz/gre_Gz.txt');
grad_raster = 1*10e-6;
f1 = 1100;
bw1 = 300;
f2 = 550;
bw2 = 100;

% grad = load('/data2/beata/virtualShare/pulse_files/3T/simulation/Gz/gre_Gz.txt');
% grad_raster = 1*10e-6;
% f1 = 590;
% bw1 = 100;
% f2 = 1140;
% bw2 = 220;
% 
% grad = load('/data2/beata/virtualShare/pulse_files/3T/simulation/Gz/gre_Gz_slabSel.txt');
% grad_raster = 1*10e-6;
% f1 = 590;
% bw1 = 100;
% f2 = 1140;
% bw2 = 220;


% plot gradient waveform
figure;

subplot(2,1,1);
plot((0:(length(grad)-1))*1e3*grad_raster,grad);
xlabel('time (ms)');
ylabel('G (mT/m)');
title('Gradient Waveform');


%% FFT
% Copying from matlab's fft example   
n = 2^nextpow2(length(grad)*grad_raster*10e6);
Y = fft(grad,n);
freq = (1./grad_raster)*(0:(n/2))/n;
P = abs(Y/n).^2; % abs power squared

% positive freq spectrum
GRAD = P(1:n/2+1);
    

%% Plot spectrum and add "bad zones"
subplot(2,1,2);

plot(freq,abs(GRAD));
xlim([0 10000])
xlabel('freq (Hz)');
hold on;
title('Gradient Spectrum');
ylabel('|P(f)|^2')


x1=f1-bw1/2;
x2=f1+bw1/2;
plot_bad_zone(x1,x2,GRAD)

x1=f2-bw2/2;
x2=f2+bw2/2;
plot_bad_zone(x1,x2,GRAD)


%% Freq Analysis: FWHM 
GRAD = abs(GRAD);
y2   = max(GRAD);

% crop to frequencies of interest
ind1 = find(freq > 10000, 1, 'first');
GRAD = GRAD(1:ind1);
freq = freq(1:ind1);

% min peak threshold
peak_threshold = 0.25; % ignore relative amplitudes below

% find peaks and return stats
[pks,locs,w] = findpeaks(GRAD, 'MinPeakHeight', y2*peak_threshold);
plot(freq(locs), pks, 'bo')
peak_widths = mean(diff(freq))*w;
peak_freqs  = freq(locs);
peak_amps   = pks./y2;



function plot_bad_zone(x1,x2,ydata)
y1=0;
y2=1.1*max(abs(ydata));
x = [x1, x2, x2, x1];
y = [y1, y1, y2, y2];
patch(x, y, [0.8500 0.3250 0.0980], 'FaceAlpha', 0.25, 'LineStyle', 'none');
ylim([0 y2])
end