% SNR_calc_main.m
% Simon Robinson. 31.10.2006
% Calculating Image SNR on the basis of ROIs, which can be defined and saved here
% and/or Time-Series SNR, which is done on a pixel-by-pixel basis
% Calculation done in calc_tSNR_func

clear, clc
warning off MATLAB:divideByZero


main_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20190726_cream/';
rois_name = {'roi_water.nii','roi_cream.nii'};
        
for variant = 1:10
        switch variant
            case 1
                data_subdir = 'nii_from_dats/SINC_FAs41';
                write_subdir = 'tSNR/SINC_FAs41/';
                image_name = 'image.nii';
            case 2
                data_subdir = 'nii_from_dats/noCAIPI_FAs41';
                write_subdir = 'tSNR/noCAIPI_FAs41/';
                image_name = 'image.nii';
            case 3
                data_subdir = 'caipirinha/FAs41/';
                write_subdir = 'tSNR/CAIPI_FAs41/';
                image_name = '0.00_recombined_notChemShiftCorr.nii';
            case 4
                data_subdir = 'caipirinha/FAw30_FAf53/';
                write_subdir = 'tSNR/CAIPI_FAw30_FAf53/';
                image_name = '0.00_recombined_notChemShiftCorr.nii';
                              
            case 5
                data_subdir = 'caipirinha/FAs41/';
                write_subdir = 'tSNR/Aliased/FAs41/';
                image_name = 'Aliased.nii';
            case 6
                data_subdir = 'caipirinha/FAw30_FAf53/';
                write_subdir = 'tSNR/Aliased/CAIPI_FAw30_FAf53/';
                image_name = 'Aliased.nii';
            case 7
                data_subdir = 'caipirinha/FAs41/';
                write_subdir = 'tSNR/water/FAs41/';
                image_name = 'water.nii';
            case 8
                data_subdir = 'caipirinha/FAw30_FAf53/';
                write_subdir = 'tSNR/water/FAw30_FAf53/';
                image_name = 'water.nii';
            case 9
                data_subdir = 'caipirinha/FAs41/';
                write_subdir = 'tSNR/CAIPI_FAs41/';
                image_name = '0.00_recombined_notChemShiftCorr.nii';
            case 10
                data_subdir = 'caipirinha/FAw30_FAf53/';
                write_subdir = 'tSNR/CAIPI_FAw30_FAf53/';
                image_name = '0.00_recombined_notChemShiftCorr.nii';
 
                
            case 11
                data_subdir = 'nii_from_dats/SINC/';
                write_subdir = 'tSNR/SINC/';
                image_name = 'image.nii';
            case 12
                data_subdir = 'nii_from_dats/noCAIPI/';
                write_subdir = 'tSNR/noCAIPI/';
                image_name = 'image.nii';
            case 13
                data_subdir = 'caipirinha/';
                write_subdir = 'tSNR/CAIPI/';
                image_name = '0.00_recombined_notChemShiftCorr.nii';            
                
            case 14
                data_subdir = 'caipirinha/';
                write_subdir = 'tSNR/Aliased/';
                image_name = 'Aliased.nii';
            case 15
                data_subdir = 'caipirinha/';
                write_subdir = 'tSNR/water/';
                image_name = 'water.nii';
            case 16
                data_subdir = 'caipirinha/';
                write_subdir = 'tSNR/CAIPI/';
                image_name = '0.00_recombined_notChemShiftCorr.nii';
                
         end
%%
    write_dir = fullfile(main_dir,write_subdir);

    %% make results directory
    s = warning ('query', 'MATLAB:MKDIR:DirectoryExists') ;	    % get current state
    warning ('off', 'MATLAB:MKDIR:DirectoryExists') ;
    mkdir(write_dir);
    warning(s) ;						    % restore state


    %%
    for scan = 1:25

        scans_subdir = sprintf('scan%i',scan);
        readfile = fullfile(main_dir, data_subdir, scans_subdir, image_name);

        image_nii = load_nii(readfile);
        image = image_nii.img;

        image_4D(:,:,:,scan) = image;
    end

    [SNR,signal,noise] = calc_tSNR_func_bb(image_4D, write_dir);


    for iROI = 1:length(rois_name)
        roi_name = fullfile(main_dir,char(rois_name{iROI}));
        roi_nii = load_nii(roi_name);
        roi = roi_nii.img;

        meanSNR(variant,iROI) = sum(sum(sum(SNR.*roi))) / sum(sum(sum(roi)));
        meanSignal(variant,iROI) = sum(sum(sum(signal.*roi))) / sum(sum(sum(roi)));
        meanNoise(variant,iROI) = sum(sum(sum(noise.*roi))) / sum(sum(sum(roi)));
    end


end

disp('Finished');

warning on MATLAB:divideByZero
