% user defined section 
dimension = [128, 128];
datatype = 'uint16';
fileNumbersOf1TP = 30;
numberOfTPs = 1;
numChan = 1;
% end of user defined section

data = zeros(dimension(1), dimension(2), fileNumbersOf1TP, numberOfTPs);
currTP = 4;
i = 0;
sliceAnat = 1; 
chan = 1; 

while i < (fileNumbersOf1TP)
    i = i+1;
    if chan > numChan
        chan = 1;
        i = i+4;
        sliceAnat = sliceAnat+2;
        if (sliceAnat) > fileNumbersOf1TP/(numChan+4)
            sliceAnat = 0;
        end
    end
%     if i > fileNumbersOf1TP && currTP < numberOfTPs
%         currTP = currTP + 1;
%         chan = 1; 
%         sliceAnat = 1; 
%         i = 2; % because there is also MOSAIC image at the end of each time-point
%     end
    if i > fileNumbersOf1TP
        break;
    end
    storeInd = sliceAnat*numChan + chan;
    name = ['WriteToFile_' num2str(i + fileNumbersOf1TP*(currTP-1), '%04d') '.ima'];
    data(:,:,storeInd,currTP) = fread(fopen(name), dimension, datatype);
    chan = chan+1;
end
data_cut = data(:,:,1:960);

slc = 1;
currTP = 1;
for currTP = 1:numberOfTPs
    for chan = 1:numChan
        fslc = 1; 
        for islc = slc:numChan:960
            data_5D(:,:,fslc,chan,currTP) = data(:,:,islc,currTP);
            fslc = fslc + 1;
        end
        slc = slc + 1;
    end
    slc = 1;
    
end
save_nii(make_nii(data_5D), 'Image_phase.nii');
fclose('all');


	

% user defined section 
%	dimension = [64, 64];
%	datatype = 'uint16';
%	fileNumbersOf1TP = 1280;
%	numberOfTPs = 2;
%	numChan = 32;
%	% end of user defined section

%	data = zeros(dimension(1), dimension(2), fileNumbersOf1TP, numberOfTPs);
%	currTP = 1;
%	i = 0;
%	sliceAnat = 1; 
%	chan = 1; 

%	while i <= fileNumbersOf1TP
%	    i = i+1;
%	    if chan > 32
%		chan = 1;
%		sliceAnat = sliceAnat+2;
%		if sliceAnat > fileNumbersOf1TP/numChan
%		    sliceAnat = 0;
%		end
%	    end
%	    if i > fileNumbersOf1TP && currTP < numberOfTPs
%		currTP = currTP + 1;
%		chan = 1; 
%		sliceAnat = 1; 
%		i = 2; % because there is also MOSAIC image at the end of each time-point
%	    end
%	    storeInd = sliceAnat*numChan + chan;
%	    name = ['WriteToFile_' num2str(i + fileNumbersOf1TP*(currTP-1), '%04d') '.ima'];
%	    data(:,:,storeInd,currTP) = fread(fopen(name), dimension, datatype);
%	    chan = chan+1;
%	end

%	slc = 1;
%	currTP = 1;
%	for currTP = 1:numberOfTPs
%	    for chan = 1:numChan
%		fslc = 1; 
%		for islc = slc:32:fileNumbersOf1TP
%		    data_5D(:,:,fslc,chan,currTP) = data(:,:,islc,currTP);
%		    fslc = fslc + 1;
%		end
%		slc = slc + 1;
%	    end
%	    slc = 1;
%	end
%	save_nii(make_nii(data_5D), 'Image.nii');
%	fclose('all');

