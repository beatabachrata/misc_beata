% SNR_calc_main.m
% Simon Robinson. 31.10.2006
% Calculating Image SNR on the basis of ROIs, which can be defined and saved here
% and/or Time-Series SNR, which is done on a pixel-by-pixel basis
% Calculation done in calc_tSNR_func

clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set parameters and counters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   pulse shape analysis 
root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM';
QSM_dir = 'QSM';

% subjects_dir = {'19930611LNNH_20210519_headneck3T','19750413ABSH_20210511_headneck3T','19980113RPCZ_20210309_headneck3T'}; 
% graf_label = {'echo 1','echo 2','echo 3','echo 4','echo 5','echoes 1-5'};
% graf_title = 'Neck (3 Tesla)';
% % graf_title = 'Brain (3 Tesla)';
% scans = [1:5,7];

subjects_dir = {'19980113RPCZ_20210426_headneck7T','19810706EAPB_20210510_headneck7T','19850315PTHI_20210521_headneck7T'};
graf_label = {'echo 1','echo 2','echo 3','echo 4','echo 5','echo 6','echoes 1-6'};
% graf_title = 'Brain (7 Tesla)';
graf_title = 'Neck (7 Tesla)';
scans = [1:7];

QSM_subdir = 'results/romeo_pdf_star_ivw';
caipi_subdir = 'caipirinha/random';
image_name = 'sepia_QSM.nii';
unzip = true;

cf_bkg = false ; % true - calculates SNR values w.r.t a background ROI, false - just calculates mean values in the ROI
% ROIs_signal = {'red_nuclei_left.nii','red_nuclei_right.nii','pallidus_left.nii','pallidus_right.nii','putamen_left.nii','putamen_right.nii','thalamus_left.nii','thalamus_right.nii'};%,'caudate_left.nii','caudate_right.nii'};  
% ROIs_bkg = {'bkg_red_nuclei_left.nii','bkg_red_nuclei_right.nii','bkg_pallidus_left.nii','bkg_pallidus_right.nii','bkg_putamen_left.nii','bkg_putamen_right.nii','bkg_thalamus_left.nii','bkg_thalamus_right.nii'};%,'bkg_caudate_left.nii','bkg_caudate_right.nii'};  
ROIs_signal = {'fat_back_1.nii','fat_back_2.nii','fat_fascia_1.nii','fat_fascia_2.nii','fat_side_1.nii','fat_side_2.nii','fat_front_1.nii','fat_front_2.nii'};  
ROIs_bkg = {'bkg_fat_back_1.nii','bkg_fat_back_2.nii','bkg_fat_fascia_1.nii','bkg_fat_fascia_2.nii','bkg_fat_side_1.nii','bkg_fat_side_2.nii','bkg_fat_front_1.nii','bkg_fat_front_2.nii'};  
ROI_noise = 'noise.nii';

for iSubj = 1:length(subjects_dir)
    subject = subjects_dir(iSubj);
    
for iScan = 1:length(scans)
    scan = scans(iScan);
    
    switch scan
        case 1
            corr_subdir = 'veryNewMask_echo1_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 2
            corr_subdir = 'veryNewMask_echo2_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 3
            corr_subdir = 'veryNewMask_echo3_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 4
            corr_subdir = 'veryNewMask_echo4_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 5
            corr_subdir = 'veryNewMask_echo5_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 6
            corr_subdir = 'veryNewMask_echo6_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 7
            corr_subdir = 'veryNewMask_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
% % 
%     switch scan
%         case 1
%             corr_subdir = 'brainOnly_echo1_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
%         case 2
%             corr_subdir = 'brainOnly_echo2_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
%         case 3
%             corr_subdir = 'brainOnly_echo3_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
%         case 4
%             corr_subdir = 'brainOnly_echo4_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
%         case 5
%             corr_subdir = 'brainOnly_echo5_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
%         case 6
%             corr_subdir = 'brainOnly_echo6_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
%         case 7
%             corr_subdir = 'brainOnly_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
end
    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check if the number of signal and noise ROIs is the same
if (length(ROIs_signal) ~= length(ROIs_bkg))
    error('the number of signal and background ROIs should be the same')
    break;
end

%   generate readfile names
read_dir = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, caipi_subdir, corr_subdir, QSM_subdir);
read_file = char(fullfile(read_dir, image_name));
write_dir = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, caipi_subdir);

if (unzip)
    zip_read_file = fullfile(read_dir, sprintf('%s.gz',image_name));
    gunzip(zip_read_file,read_dir);
end

% load the image
ima_nii = load_nii(read_file);
ima = ima_nii.img(:,:,:); 
clear image_nii

% if multi-echo, use just the first echo
if (size(ima,4) > 1) 
    ima = squeeze(ima(:,:,:,1));
end


CNR = zeros(1,size(ROIs_signal,2));

for iROI=1:size(ROIs_signal,2)
    
    % load the ROIS
    roi_name = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, 'CNR_rois', ROIs_signal(iROI));   
    roi_bkg_name = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, 'CNR_rois', ROIs_bkg(iROI));  
    roi_noise_name = fullfile(root_dir, subjects_dir(iSubj), QSM_dir, 'CNR_rois', ROI_noise);  
    
    roi_nii = load_nii(char(roi_name));
    roi = roi_nii.img;
    roi_nii = load_nii(char(roi_bkg_name));
    roi_bkg = roi_nii.img;
    roi_nii = load_nii(char(roi_noise_name));
    roi_noise = roi_nii.img;
    
    clear roi_nii
    
    [CNR(iROI),chi(iROI)] = calc_CNR_func_bb(ima, roi, roi_bkg, roi_noise);

end

CNR_allScans(iScan,:) = CNR;
chi_allScans(iScan,:) = chi;

end

CNR_all_norm = bsxfun(@rdivide, CNR_allScans, CNR_allScans(1,:));
dev = std(CNR_all_norm,0,2);

CNR_norm_allSubj(:,:,iSubj) = CNR_all_norm;
chi_allSubj(:,:,iSubj) = chi_allScans;
dev_allSubj(:,iSubj) = dev;

end

% mean over all rois and then over all subjects
CNR_allSubj_norm_mean = (mean(mean(CNR_norm_allSubj,2),3).');
CNR_allSubj_norm_mean_tr = CNR_allSubj_norm_mean.';
dev_allSubj_mean = mean(dev_allSubj,2);

values = cell([6,1]);
for iVal = 1:length(CNR_allSubj_norm_mean_tr)
    values{iVal} = (sprintf('%.2f +- %.2f', string(CNR_allSubj_norm_mean_tr(iVal)), string(dev_allSubj_mean(iVal))));
end

figure;
b = bar(CNR_allSubj_norm_mean,0.6,'FaceColor','flat');
hold on
xticklabels(graf_label)
axis([0.4 (length(CNR_allSubj_norm_mean)+0.6) 0 (max(CNR_allSubj_norm_mean+dev_allSubj_mean.')+0.5)])

er = errorbar(1:length(dev_allSubj_mean),CNR_allSubj_norm_mean,dev_allSubj_mean,'.');    
er.Color = [0 0 0];                            
% text(1:length(CNR_allSubj_norm_mean),(CNR_allSubj_norm_mean+dev_allSubj_mean.'+0.05),values.','vert','bottom','horiz','center','FontSize', 14); 
% text(1:length(CNR_allSubj_norm_mean),[1.05,1.35,1.75,2.00,2.42,2.25,2.45],values.','vert','bottom','horiz','center','FontSize', 14); % Brain 7T
text(1:length(CNR_allSubj_norm_mean),[1.05,1.45,1.08,0.80,0.65,0.47,2.53],values.','vert','bottom','horiz','center','FontSize', 14); % Neck 7T

set(gca,'FontSize',16)
ylabel('CNR','FontSize',18)
title(graf_title,'FontSize',18)

for iColor = 1:(length(b.CData)-1)
    b.CData(iColor,:) = [0 128/255 255/255];
end
b.CData(end,:) = [153/255 0 153/255];

disp('Finished');
