clear all

%% Set simulation constants
B0 = 2.89326;
gyro = 42.58;
M0 = 50;
TEs = 0:1:60;
j = 1;
fit_TEs = [6.82,13.64,20.46];

%% Define signal model
% SMURF visible fat peaks (normalized)
freq_ppm = [-0.3, -0.1, -0.9];
relAmps = [9.58 76.32 14.10];
freq_ppm = [0, -0.1, 0]; % seems to work better
relAmps = [0, 76.32, 0]; % seems to work better
relAmps = relAmps*100/sum(relAmps);
T2s = 30;
mag_fat_model = sprintf('abs(M0*exp(-TE/T2s)*(%d*exp(-i*(-%d*42.58*2.8933*2*pi*TE/1000))+%d*exp(-i*(-%d*42.58*2.8933*2*pi*TE/1000))+%d*exp(-i*(-%d*42.58*2.8933*2*pi*TE/1000))))',relAmps(1),freq_ppm(1),relAmps(2),freq_ppm(2),relAmps(3),freq_ppm(3));


% SMURF visible water + nearby fat peaks (normalized) - fitting with different T2s values didn't work, this gives however very good results
% freq_ppm = [0, -0.39, 0.60];
% relAmps = [92.00 3.59 4.42];
freq_ppm = [0, 0, 0]; % seems to work better
relAmps = [92.00, 0, 0]; % seems to work better
relAmps = relAmps*100/sum(relAmps);
T2s = 24;
mag_water_model = sprintf('abs(M0*exp(-TE/T2s)*(%d*exp(-i*(-%d*42.58*2.8933*2*pi*TE/1000))+%d*exp(-i*(-%d*42.58*2.8933*2*pi*TE/1000))+%d*exp(-i*(-%d*42.58*2.8933*2*pi*TE/1000))))',relAmps(1),freq_ppm(1),relAmps(2),freq_ppm(2),relAmps(3),freq_ppm(3));


% % % Water
% % freq_ppm = [0 -3.500]; 
% % relAmps = [1 1];
% 
% Fat
freq_ppm = [-3.80, -3.40, -2.60, -1.94, -0.39, 0.60];
relAmps = [0.087 0.693 0.128 0.004 0.039 0.048];

freq_ppm = [-3.80, -3.40, -2.60];
relAmps = [0.087 0.693 0.128]./0.9080;

%water
freq_ppm = [-0.39, 0, 0.60];
relAmps = [0.039 1 0.048]./1.087;

% freq_ppm = [-3.40];
% relAmps = [1];

TEs = 0.1:0.1:100;

for iTE = 1:length(TEs)    
    TE = TEs(iTE);
    phaseOffset = freq_ppm * gyro * B0 * TE * 2 * pi /1000;
    phaseOffset = mod(phaseOffset,2*pi);
    
    signal_complex = M0 .* relAmps .* exp(-TE./T2s) .* exp(-i*phaseOffset);
	signal(iTE) = (sum(signal_complex));

end

figure
plot(TEs,abs(signal))
% hold on
% 
% 
% for i = 1:length(fit_TEs)
%     fit_signal(i) = abs(signal(TEs==fit_TEs(i)));
% end
% 
% [fitresult, gof] = fit_simulated_signal_evolution(fit_TEs, fit_signal, mag_fat_model);
% fitresult{1,1}.M0
% fitresult{1,1}.T2s

figure
% fat_fit_signal = [2.0408, 1.9798, 1.8495];
fat_fit_signal = [1.6358, 1.4334, 1.3212];
[fitresult, gof] = fit_signal_evolution(fit_TEs, fat_fit_signal, mag_fat_model);
title(sprintf('Fat - fitted T2s = %.*f', 2, fitresult{1,1}.T2s))

figure
water_fit_signal = [1.5573, 1.2017, 0.8683];
[fitresult, gof] = fit_signal_evolution(fit_TEs, water_fit_signal, mag_water_model);
title(sprintf('Water - fitted T2s = %.*f', 2, fitresult{1,1}.T2s))

% 
% %vars = [M0, T2s, TE]
% f_mag_model = @(vars,fit_TEs) vars(1).*exp(-fit_TEs/vars(2)).*(9.58*exp(-i*(-3.8*42.58*2.8933*2*pi*fit_TEs/1000))+76.32*exp(-i*(-3.4*42.58*2.8933*2*pi*fit_TEs/1000))+14.1*exp(-i*(-2.6*42.58*2.8933*2*pi*fit_TEs/1000)));
% vars0 = [50,35]
% [X] = lsqcurvefit(f_mag_model, vars0, fit_TEs, fit_signal);
% 
% 
% %vars = [M0, T2s]
% cx_model = @(vars,fit_TEs)abs(vars(1)*exp(-fit_TEs/vars(2))*(9.58*exp(-i*(-3.8*42.58*2.8933*2*pi*fit_TEs/1000))+76.32*exp(-i*(-3.4*42.58*2.8933*2*pi*fit_TEs/1000))+14.1*exp(-i*(-2.6*42.58*2.8933*2*pi*fit_TEs/1000))));
% [X] = lsqcurvefit(fcx_model, [0,0], fit_TEs, abs(fit_signal));
% fcx_model=@(vars,fit_TEs)(vars(1)*exp(-fit_TEs/vars(2))*(9.58*exp(-i*(-3.8*42.58*2.8933*2*pi*fit_TEs/1000))+76.32*exp(-i*(-3.4*42.58*2.8933*2*pi*fit_TEs/1000))+14.1*exp(-i*(-2.6*42.58*2.8933*2*pi*fit_TEs/1000))));
% 
% [Q,R] = qr(cx_model,0)
% p = Q\(R*fit_signal)
% 
% %% combined fitting, two T2*
% fat_freq_ppm = [-3.80, -3.40, -2.60, -1.94, -0.39, 0.60];
% fat_relAmps = [0.087 0.693 0.128 0.004 0.039 0.048];
% 
% B0 = 2.89326;
% gyro = 42.58;
% M0_w = 50;
% M0_f = 50;
% T2s_w = 20;
% T2s_f = 30;
% TEs = 0.1:0.1:60;
% 
% for iTE = 1:length(TEs)
%     TE = TEs(iTE);
%     
%     phaseOffset_f = fat_freq_ppm * gyro * B0 * TE * 2 * pi /1000;
%     phaseOffset_f = mod(phaseOffset_f,2*pi);
%     
%     fat_signal_complex = M0_f * exp(-TE/T2s_f) .* fat_relAmps .* exp(-i*phaseOffset_f);
%     water_signal_complex = M0_w * exp(-TE/T2s_w);
%     
% 	signal(iTE) = abs(sum(fat_signal_complex) + water_signal_complex);
% 
% end
% 
% figure
% plot(TEs,signal,':*')
% 
% % % mixed fat-water with separate T2* fit function (for MATLAB's curve fitting)
% % abs(a*exp(-x/b)+(1-a)*exp(-x/c)*(8.7*exp(-i*(-3.8*42.58*2.8933*2*pi*x/1000))+69.3*exp(-i*(-3.4*42.58*2.8933*2*pi*x/1000))+12.8*exp(-i*(-2.6*42.58*2.8933*2*pi*x/1000))+0.4*exp(-i*(-1.94*42.58*2.8933*2*pi*x/1000))+3.9*exp(-i*(-0.39*42.58*2.8933*2*pi*x/1000))+4.8*exp(-i*(0.6*42.58*2.8933*2*pi*x/1000))))


% 





