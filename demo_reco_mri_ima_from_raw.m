clear
close all;

% For memory purposes echo-by-echo processing might be required


%% Define data paths and acquisition orientation  
% GRAPPA = 2 => would require special reco
% dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/ima_offline/highRes';
% dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/dats/meas_MID00167_FID15635_ke_gre_aspireRelease_highRes.dat';

% dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/ima_offline/veryLowRes';
% dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/dats/meas_MID00166_FID15634_ke_gre_aspireRelease_veryLowRes.dat';

% nicest images
dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/ima_offline/lowResME';
dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/dats/meas_MID00168_FID15636_ke_gre_aspireRelease_lowResME.dat';

% bit motion corrupted
% dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/ima_offline/lowResSE';
% dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/Measurement_Data/beata/Brain/daten/dats/meas_MID00173_FID15641_ke_gre_aspireRelease_lowResSE.dat';


%% Create output directory
[s,mess] = mkdir(dir);
fprintf('Writing results to %s\n', dir);
if s == 0
    error('No permission to make directory %s/m', dir);
end


%%  Load and data 
% Load the data using mapVBVD function by Philipp Ehses (https://github.com/CIC-methods/FID-A/blob/master/inputOutput/mapVBVD/mapVBVD_tutorial.m)
data = mapVBVD(dat_file);
data = data.image{''};

% Reduce data precision (for memory purposes)
data = single(data);
 
% Reshape kspace to [coil, readout(col), phase-encoding(lin), slc, echo]
% MR scanner applies oversampling along readout to ...........................................
data = permute(data,[2,1,3,4,5]);   


%% Flip data along all dimensions beside echo
data = flip(flip(flip(flip(data,1),2),3),4);


%% Zerofill partial Fourier
dims = size(data);
if (dims(3) < dims(2)/2) % there is oversampling
    
    % Pre-filter data suppress ringing artefact due to sharp edge in the k-space
    filterWidth = 0.1; % affects only the edge 10% of the k-space 
    dims_filter = dims;
	dims_filter(3) = 1; % do not repmat in the filter dimension

	% Calculate 1D Hamming filter (applied only to the truncated side of k-space)
	filter = hamming(ceil(filterWidth*(dims(3)*2)));
    filter = filter(ceil(length(filter)/2)+1:end);
 	filter = cat(1, ones([dims(3)-numel(filter) 1]), filter);

    % Replicate 1d-Hamming to the matrix size
	reshape_to = horzcat(ones([1 2]), numel(filter));
	filter = reshape(filter,reshape_to);
    filter = repmat(filter, dims_filter);
					
    % Convolve the data with the filter
	data = data .* filter;
	clear filter
    
    % Get a full-sized zero-matrix
    dims_full = dims;
    dims_full(3) = dims(2)/2;
    data_full = single(zeros(dims_full));
    
    % Assign the original k-space values
    data_full(:,:,1:dims(3),:,:) = data;
    
%    % Alternatively complex-conjugate recontruction can be applied 
%     conjugate = conj(flip(flip(data,2),3));
%     data_full(:,:,((end-dims(3)+1):end),:,:) = conjugate;
%     data_full(:,:,(1:dims(3)),:,:) = data;
    
    data = data_full;
    clear data_full
end


%% Save magnitude of a k-space of first channel (nifti format)
nii = make_nii(squeeze(abs(data(1,:,:,:,:))));
save_nii(nii,fullfile(dir,'kspace_ch1.nii'));
clear nii

% %% Use only part of the k-space
% factor = 1/4;
% dims = size(data);
% 
% % Use only outer part of k-space 
% data(:,(dims(2)/2-dims(2)*factor/2+1):(dims(2)/2+dims(2)*factor/2),(dims(3)/2-dims(3)*factor/2+1):(dims(3)/2+dims(3)*factor/2),:,:) = 0;
% 
% % Use only centre part of k-space 
% % data(:,[1:(dims(2)/2-dims(2)*factor/2),(dims(2)/2+dims(2)*factor/2+1:end)],[1:(dims(3)/2-dims(3)*factor/2),(dims(3)/2+dims(3)*factor/2+1:end)],:,:) = 0;


%% Fourier transform data from k-space to image-space (along all spatial dimensions)
for dim = [2 3 4]
    data = ifftshift(ifft(fftshift(data,dim),[],dim),dim);
end


%% Remove oversampling along readout
data = data(:,(size(data,2)*0.25 + 1):(size(data,2)*0.75),:,:,:);


%% Flip data for correct visualization (depends on image orientation)
data = flip(flip(data,2),3);


%% Save phase of a first channel (nifti format)
nii = make_nii(squeeze(angle(data(1,:,:,:,:))));
save_nii(nii,fullfile(dir,'phase_ch1.nii'));


%% Combine data over channels (Root Sum of Squares) - only magnitude from now on
data = squeeze(sqrt(sum((abs(data).^2),1)));


%% Save image (nifti format)
nii = make_nii(data);
save_nii(nii,fullfile(dir,'image.nii'));

    
    
 
    



