%% Housekeeping & Definitions
clear, clc, close all

% common paths
analysis_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM';
sort_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot';
orient = 2; % 1-sag, 2-trans, 3-cor
PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
is2D = 1;
phase_oversampling = 0;
PE_FOV_fraction = 1;
sequence = '';

%% Case specific definitions
for scan = 34:39
        
    switch scan 
        case 1   
            subj = '19750413ABSH_201906061000_abdomen';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00355_FID64369_gre_prescan.dat';
            caipi_subdir = 'caipirinha';
            output_subdir = 'fieldmap/removeAspirePO_chemShiftCorr';
            filename_main_phase = 'phase_recombined_chemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_recombined_chemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.2;
            orient = 2; 
            PE_dir = 1; 
        case 2
            subj = '19841222SGGL_20190612_breasts';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00049_FID16950_gre_prescan_TE{2_3,4_6,7_5}_TR40_rBW690.dat';
            caipi_subdir = 'caipirinha';
            output_subdir = 'fieldmap/removeAspirePO';
            filename_main_phase = 'phase_recombined_chemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_recombined_chemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1;
            orient = 2; 
            PE_dir = 3; 
        case 3
            subj = '19930214TNNH_20190508';
            sequence = 'tse';
            prescan_dat_file = 'meas_MID00439_FID60846_gre_prescan_128x128_TEs{2_32,4_64,7_54}.dat';
            caipi_subdir = 'caipirinha';
            output_subdir = 'fieldmap/removeAspirePO_notChemShiftCorr';
            filename_main_phase = 'phase_recombined_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_recombined_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.4;
            orient = 1; 
            PE_dir = 2;                            
            
        case 4 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/recombined_type1Type2Corr';
            filename_main_phase = 'phase_-1.74_recombined_chemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_-1.74_recombined_chemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 5 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/recombined_type1Corr';
            filename_main_phase = 'phase_-1.00_recombined_chemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_-1.00_recombined_chemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 6 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/recombined_type2Corr';
            filename_main_phase = 'phase_-1.74_recombined_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_-1.74_recombined_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 7 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/recombined_notCorr';
            filename_main_phase = 'phase_-1.00_recombined_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_-1.00_recombined_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 8 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/water';
            filename_main_phase = 'phase_water_uncomb.nii';
            filename_main_mag = 'mag_water_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 9 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/fat_type1Type2Corr';
            filename_main_phase = 'phase_-1.74_fat__uncomb.nii';
            filename_main_mag = 'mag_-1.74_fat__uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 10 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/fat_type1Corr';
            filename_main_phase = 'phase_-1.00_fat__uncomb.nii';
            filename_main_mag = 'mag_-1.00_fat__uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 11 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/fat_type2Corr';
            filename_main_phase = 'phase_-1.74_fat__notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_-1.74_fat__notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 12 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/fat_notCorr';
            filename_main_phase = 'phase_-1.00_fat__notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_-1.00_fat__notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;              
        case 13 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/recombined_type1RelaxCorr';
            filename_main_phase = 'phase_-1.74_recombined_notChemShiftCorr_rescaled_uncomb.nii';
            filename_main_mag = 'mag_-1.74_recombined_notChemShiftCorr_rescaled_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 14 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/recombined_type1Type2RelaxCorr';
            filename_main_phase = 'phase_-1.74_recombined_chemShiftCorr_rescaled_uncomb.nii';
            filename_main_mag = 'mag_-1.74_recombined_chemShiftCorr_rescaled_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 15 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/fat_type2RelaxCorr';
            filename_main_phase = 'phase_-1.74_fat__notChemShiftCorr_rescaled_uncomb.nii';
            filename_main_mag = 'mag_-1.74_fat__notChemShiftCorr_rescaled_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  
        case 16 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00027_FID75090_bb_caipigre_3D_prescan_1_1slcThick.dat';
            caipi_subdir = 'caipirinha/3D/iPat3/1.1x1.1x1.1_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/3D/fat_type1Type2RelaxCorr';
            filename_main_phase = 'phase_-1.74_fat__rescaled_uncomb.nii';
            filename_main_mag = 'mag_-1.74_fat__rescaled_uncomb.nii';
            inplane_voxel_size = 1.1;
            is2D = 0;
            orient = 3; 
            PE_dir = 3;  

        case 17 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_newg/notCorr';
            filename_main_phase = 'phase_0.50_recombined_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_0.50_recombined_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;                   
        case 18 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/type2Corr';
            filename_main_phase = 'phase_1.59_recombined_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_1.59_recombined_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;                
        case 19 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/type1Type2Corr';
            filename_main_phase = 'phase_1.59_recombined_chemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_1.59_recombined_chemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;                
        case 20 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/type1Type2RelaxCorr';
            filename_main_phase = 'phase_1.59_recombined_chemShiftCorr_rescaled_uncomb.nii';
            filename_main_mag = 'mag_1.59_recombined_chemShiftCorr_rescaled_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;                
        case 21 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/fat_notCorr';
            filename_main_phase = 'phase_0.50_fat_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_0.50_fat_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;                              
        case 22 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/fat_type2Corr';
            filename_main_phase = 'phase_1.59_fat_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_1.59_fat_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;  
        case 23 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/fat_type1Type2Corr';
            filename_main_phase = 'phase_1.59_fat_uncomb.nii';
            filename_main_mag = 'mag_1.59_fat_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;  
        case 24 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/fat_type1Type2RelaxCorr';
            filename_main_phase = 'phase_1.59_fat_rescaled_uncomb.nii';
            filename_main_mag = 'mag_1.59_fat_rescaled_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;  
        case 25 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/water';
            filename_main_phase = 'phase_water_uncomb.nii';
            filename_main_mag = 'mag_water_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3; 
         case 26 % ISMRM abstract 2019 (QSM)
            subj = '19860423TTKR_20190912';
            sequence = '';
            prescan_dat_file = 'meas_MID00048_FID75111_bb_caipigre_2D_prescan.dat';
            caipi_subdir = 'caipirinha/2D_relaxDiffsCorr_new';
            output_subdir = 'removeAspirePO/2D_relaxDiffsCorr_new/aliased';
            filename_main_phase = 'phase_aliased_uncomb.nii';
            filename_main_mag = 'mag_aliased_uncomb.nii';
            inplane_voxel_size = 1.25;
            orient = 3; 
            PE_dir = 3;   
            
         case 27 
            subj = '19870704RDKE_20200617_knee';
            sequence = 'gre';
            prescan_dat_file = 'meas_MID00039_FID47145_ke_gre_aspireTest_3D_prescan_TEs_2_3_4_6.dat';
            caipi_subdir = 'caipirinha/3D';
            output_subdir = 'removeAspirePO/3D';
            filename_main_phase = 'phase_1.07_recombined_chemShiftCorr_rescaled_uncomb.nii';
            filename_main_mag = 'mag_1.07_recombined_chemShiftCorr_rescaled_uncomb.nii';
            inplane_voxel_size = 0.588;
            orient = 1; 
            PE_dir = 2;   
            is2D = 0;            
            
         case 27 
            subj = '19860423TTKR_20200706_headneck';
            prescan_dat_file = 'meas_MID00032_FID48782_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha/TE7p66_hamming20y/';
            output_subdir = 'removeAspirePO/caipirinha/TE7p66_hamming20y/recombined_type1Type2RelaxCorr';
            filename_main_phase = 'phase_0.74_recombined_chemShiftCorr_rescaled_uncomb.nii';
            filename_main_mag = 'mag_0.74_recombined_chemShiftCorr_rescaled_uncomb.nii';
            inplane_voxel_size = 1.1;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;  
        case 28 
            subj = '19860423TTKR_20200706_headneck';
            prescan_dat_file = 'meas_MID00032_FID48782_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha/TE7p66_hamming20y/';
            output_subdir = 'removeAspirePO/caipirinha/TE7p66_hamming20y/recombined_type1Type2Corr';
            fil58ename_main_phase = 'phase_0.74_recombined_chemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_0.74_recombined_chemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;    
         case 29 
            subj = '19860423TTKR_20200706_headneck';
            prescan_dat_file = 'meas_MID00032_FID48782_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha/TE7p66_hamming20y/';
            output_subdir = 'removeAspirePO/caipirinha/TE7p66_hamming20y/recombined_type2Corr';
            filename_main_phase = 'phase_0.74_recombined_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_0.74_recombined_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0; 
         case 30 
            subj = '19860423TTKR_20200706_headneck';
            prescan_dat_file = 'meas_MID00032_FID48782_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha/TE7p66_hamming20y/';
            output_subdir = 'removeAspirePO/caipirinha/TE7p66_hamming20y/recombined_notCorr';
            filename_main_phase = 'phase_0.00_recombined_notChemShiftCorr_uncomb.nii';
            filename_main_mag = 'mag_0.00_recombined_notChemShiftCorr_uncomb.nii';
            inplane_voxel_size = 1.1;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;  
           

        case 31
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_noPat_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_noPat_192x192/recombined_type2Corr';
            filename_main_phase = {'phase_1.16_recombined_notChemShiftCorr_uncomb_echo1.nii','phase_1.32_recombined_notChemShiftCorr_uncomb_echo2.nii','phase_1.48_recombined_notChemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_1.16_recombined_notChemShiftCorr_uncomb_echo1.nii','mag_1.32_recombined_notChemShiftCorr_uncomb_echo2.nii','mag_1.48_recombined_notChemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;
        case 32
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_iPat2_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_iPat2_192x192/recombined_type2Corr';
            filename_main_phase = {'phase_0.83_recombined_notChemShiftCorr_uncomb_echo1.nii','phase_0.99_recombined_notChemShiftCorr_uncomb_echo2.nii','phase_1.15_recombined_notChemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_0.83_recombined_notChemShiftCorr_uncomb_echo1.nii','mag_0.99_recombined_notChemShiftCorr_uncomb_echo2.nii','mag_1.15_recombined_notChemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;
        case 33
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_iPat3_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_iPat3_192x192/recombined_type2Corr';
            filename_main_phase = {'phase_1.16_recombined_notChemShiftCorr_uncomb_echo1.nii','phase_1.32_recombined_notChemShiftCorr_uncomb_echo2.nii','phase_1.48_recombined_notChemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_1.16_recombined_notChemShiftCorr_uncomb_echo1.nii','mag_1.32_recombined_notChemShiftCorr_uncomb_echo2.nii','mag_1.48_recombined_notChemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;          
        case 34
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_noPat_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_noPat_192x192/recombined_type1Type2Corr';
            filename_main_phase = {'phase_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','phase_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','phase_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','mag_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','mag_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;
        case 35
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_iPat2_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_iPat2_192x192/recombined_type1Type2Corr';
            filename_main_phase = {'phase_0.83_recombined_chemShiftCorr_uncomb_echo1.nii','phase_0.99_recombined_chemShiftCorr_uncomb_echo2.nii','phase_1.15_recombined_chemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_0.83_recombined_chemShiftCorr_uncomb_echo1.nii','mag_0.99_recombined_chemShiftCorr_uncomb_echo2.nii','mag_1.15_recombined_chemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;
        case 36
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_iPat3_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_iPat3_192x192/recombined_type1Type2Corr';
            filename_main_phase = {'phase_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','phase_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','phase_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','mag_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','mag_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;
        case 37
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_noPat_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_noPat_192x192/recombined_type1Type2RelaxCorr';
            filename_main_phase = {'phase_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','phase_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','phase_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','mag_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','mag_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;
        case 38
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_iPat2_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_iPat2_192x192/recombined_type1Type2RelaxCorr';
            filename_main_phase = {'phase_0.83_recombined_chemShiftCorr_uncomb_echo1.nii','phase_0.99_recombined_chemShiftCorr_uncomb_echo2.nii','phase_1.15_recombined_chemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_0.83_recombined_chemShiftCorr_uncomb_echo1.nii','mag_0.99_recombined_chemShiftCorr_uncomb_echo2.nii','mag_1.15_recombined_chemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;
        case 39
            subj = 'p_bb_20200824';
            sequence = '';
            prescan_dat_file = 'meas_MID00041_FID52309_gre_prescan.dat';
            caipi_subdir = 'caipirinha/multiecho_iPat3_192x192/';
            output_subdir = 'removeAspirePO/caipirinha/multiecho_iPat3_192x192/recombined_type1Type2RelaxCorr';
            filename_main_phase = {'phase_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','phase_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','phase_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            filename_main_mag = {'mag_1.16_recombined_chemShiftCorr_uncomb_echo1.nii','mag_1.32_recombined_chemShiftCorr_uncomb_echo2.nii','mag_1.48_recombined_chemShiftCorr_uncomb_echo3.nii'};
            inplane_voxel_size = 0.8333;
            orient = 3; 
            PE_dir = 3;   
            is2D = 0;            
    end
    
    
%% Set processing defaults
processing.aspire_echoes = [1 2];
processing.slicewise = 'all_at_once';
processing.smoothingKernelSizeInMM = 5;
processing.weightedCombination = 1;

% Path definitions    
write_dir = fullfile(analysis_dir, sequence, subj, output_subdir);

% Make directory for results
s = mkdir(write_dir);
if s == 0
    error('No permission to make directory %s/m', write_dir);
end
    

%% Load data and reshape
kspace_prescan = mapVBVD(fullfile(sort_dir,subj,'dats/',prescan_dat_file));

if (iscell(kspace_prescan))
    kspace_prescan = kspace_prescan{1,end};
end

% final order should be - coil, x(n), echo, y(2n), z
kspace_prescan = reshapeKspace(kspace_prescan.image(),is2D,0); 

% Get dimensions
dims.n_echoes = size(kspace_prescan,3);
dims.n_channels = size(kspace_prescan,1);
dims.n_slices = size(kspace_prescan,5);
dims.voxel_size = inplane_voxel_size; 

% partial Fourier reconstruction
kspace_prescan = flipdim(flipdim(flipdim(flipdim(kspace_prescan,1),2),3),4);
kspace_prescan = zerofillPartialFourier(kspace_prescan,phase_oversampling,PE_FOV_fraction,0); 

sliceAcquisitionOrder = getSliceAcquisitionOrder(permute(kspace_prescan,[1,2,5,4,3]),is2D);
kspace_prescan = kspace_prescan(:,:,:,sliceAcquisitionOrder.',:); %% not always required

%
if (is2D == 1)
    ima_prescan = FFTOfMRIData_bb(kspace_prescan,0,[2 3],1);
else
    ima_prescan = FFTOfMRIData_bb(kspace_prescan,0,[2 3 4],1);
end  
clear kspace_prescan

ima_prescan = permute(ima_prescan,[3,2,4,5,1]);
ima_prescan = ima_prescan(:,(size(ima_prescan,2)*0.25 +1):(size(ima_prescan,2)*0.75),:,:,:);


% Save prescan nii
save_nii(make_nii(sum(abs(ima_prescan),5)), fullfile(write_dir, 'SoS_mag_prescan.nii'))
save_nii(make_nii(abs(ima_prescan)), fullfile(write_dir, 'mag_prescan.nii'))
% save_nii(make_nii(angle(ima_prescan)), fullfile(write_dir, 'phase_prescan.nii'))



%% Read in the main data and get complex 
for iEcho = 1:size(filename_main_phase,2)
    phase_main_nii = load_nii(fullfile(analysis_dir, sequence, subj, caipi_subdir, filename_main_phase{iEcho}));
    mag_main_nii = load_nii(fullfile(analysis_dir, sequence, subj, caipi_subdir, filename_main_mag{iEcho}));

    mag_main = single(mag_main_nii.img);
    phase_main = single(rescale(phase_main_nii.img, -pi, pi));

    % Get complex data and stack all echoes
    compl_main(:,:,:,iEcho,:) = mag_main .* exp(single(1i * phase_main));
end
clear mag_main_nii mag_main phase_main_nii phase_main

% compl_main = flipdim(compl_main,4);

% Save main nii
save_nii(make_nii(sum(abs(compl_main),length(size(compl_main)))), fullfile(write_dir, 'SoS_mag.nii'))
save_nii(make_nii(abs(compl_main)), fullfile(write_dir, 'mag.nii'))
save_nii(make_nii(angle(compl_main)), fullfile(write_dir, 'phase.nii'))



%% Get RPO
% Hermitian inner product combination
complexDifference = ima_prescan(:,:,:,processing.aspire_echoes(2),:) .* conj(ima_prescan(:,:,:,processing.aspire_echoes(1),:));
hermitian = sum(complexDifference,5);
clear complexDifference

% Subtract hermitian to get PO
po = complex(zeros([size(ima_prescan,1) size(ima_prescan,2) size(ima_prescan,3) size(ima_prescan,5)], 'single'));
for cha = 1:dims.n_channels
    po_temp = double(ima_prescan(:,:,:,processing.aspire_echoes(1),cha)) .* double(conj(hermitian));
    po(:,:,:,cha) = single(po_temp ./ abs(po_temp));
end
save_nii(make_nii(angle(po)), fullfile(write_dir, 'po.nii'))
clear po_temp hermitian ima_prescan

% Smooth PO
weight_gre = ones(size(po));
smoothed_po = complex(zeros(size(po),'single'));
for cha = 1:dims.n_channels
    smoothed_po(:,:,:,cha) = weightedGaussianSmooth(po(:,:,:,cha), ceil(processing.smoothingKernelSizeInMM/dims.voxel_size), weight_gre(:,:,:,cha));
end             
clear po weight_gre
smoothed_po_highRes = imresize(smoothed_po, [size(compl_main,1),size(compl_main,2)]);
clear smoothed_po

% Stack POs for multi-echo main scan
if (ndims(compl_main) > 4)
    for i = 1:size(compl_main,4)
        stacked_smoothed_po_highRes(:,:,:,i,:) = smoothed_po_highRes;
    end
    smoothed_po_highRes = stacked_smoothed_po_highRes; 
    clear stacked_smoothed_po_highRes
end



%% Remove PO from main data
compl_main = squeeze(compl_main) .* squeeze(conj(smoothed_po_highRes)) ./ squeeze(abs(smoothed_po_highRes));
clear smoothed_po_highRes

% save_nii(make_nii(abs(compl_main)), fullfile(write_dir, 'mag_poRemoved.nii'))
% save_nii(make_nii(angle(compl_main)), fullfile(write_dir, 'phase_poRemoved.nii'))


% Combine main data
combined_main = weightedCombination(compl_main, abs(compl_main));

save_nii(make_nii(abs(combined_main)), fullfile(write_dir, 'mag_poRemoved_comb.nii'))
save_nii(make_nii(angle(combined_main)), fullfile(write_dir, 'phase_poRemoved_comb.nii'))


% Calculate Q metric
weightedMagnitudeSum = weightedCombination(abs(compl_main), abs(compl_main));
ratio = abs(combined_main) ./ weightedMagnitudeSum;
save_nii(make_nii(ratio), fullfile(write_dir, 'ratio.nii'))

clear compl_main combined_main weightedMagnitudeSum ratio

end

 

