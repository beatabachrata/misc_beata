% SNR_calc_main.m
% Simon Robinson. 31.10.2006
% Calculating Image SNR on the basis of ROIs, which can be defined and saved here
% and/or Time-Series SNR, which is done on a pixel-by-pixel basis
% Calculation done in calc_tSNR_func

%clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set parameters and counters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off MATLAB:divideByZero

%   pulse shape analysis 
root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/';
subject_subdir = 'p_bb_20190726_oil/';
write_root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/';

% ROIs = {'roi_cream.nii','roi_water.nii','roi_water_overlap.nii'};

ROIs = {'roi_oil.nii','roi_water.nii'};

nScans = 5;

SNR_over_ROI = zeros(nScans,length(ROIs));
mean_over_ROI = zeros(nScans,length(ROIs));
std_over_ROI = zeros(nScans,length(ROIs));


for scan = 38:42
    
    switch scan
        
        case 22
            sequence_subdir = 'gre';
            scan_subdir = 'caipirinha/TR150_FAw26_FAf47/';
            noise_subdir = 'caipirinha/0V_TR150_FAw26_FAf47/';
            scan_name = '0.00_recombined_notChemShiftCorr.nii';
            noise_name = '0.00_recombined_notChemShiftCorr.nii';
        case 23
            sequence_subdir = 'gre';
            scan_subdir = 'caipirinha/TR150_FAs36/';
            noise_subdir = 'caipirinha/0V_TR150_FAs36/';
            scan_name = '0.00_recombined_notChemShiftCorr.nii';
            noise_name = '0.00_recombined_notChemShiftCorr.nii';
        case 24
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'noCAIPI_TR150_FAw26_FAf47.nii';
            noise_name = 'noCAIPI_TR150_FAw26_FAf47.nii';
        case 25
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'noCAIPI_TR150_FA36.nii';
            noise_name = 'noCAIPI_TR150_FA36.nii';
        case 26
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'SINC_TR150_FA36.nii';
            noise_name = 'SINC_TR150_FA36.nii';
                      
        case 27
            sequence_subdir = 'gre';
            scan_subdir = 'caipirinha/TR500_FAw46_FAf73/';
            noise_subdir = 'caipirinha/0V_TR500_FAw46_FAf73/';
            scan_name = '0.00_recombined_notChemShiftCorr.nii';
            noise_name = '0.00_recombined_notChemShiftCorr.nii';
        case 28
            sequence_subdir = 'gre';
            scan_subdir = 'caipirinha/TR500_FAs59/';
            noise_subdir = 'caipirinha/0V_TR500_FAs59/';
            scan_name = '0.00_recombined_notChemShiftCorr.nii';
            noise_name = '0.00_recombined_notChemShiftCorr.nii';
       case 29
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'noCAIPI_TR500_FAw46_FAf73.nii';
            noise_name = 'noCAIPI_TR500_FAw46_FAf73.nii';
        case 30
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'noCAIPI_TR500_FA59.nii';
            noise_name = 'noCAIPI_TR500_FA59.nii';
        case 31
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'SINC_TR500_FA59.nii';
            noise_name = 'SINC_TR500_FA59.nii';
            
        case 32
            sequence_subdir = 'tse';
            scan_subdir = 'caipirinha/TR500/';
            noise_subdir = 'caipirinha/0V_TR500/';
            scan_name = '0.00_recombined_notChemShiftCorr.nii';
            noise_name = '0.00_recombined_notChemShiftCorr.nii';
        case 33
            sequence_subdir = 'tse';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'noCAIPI_TR500.nii';
            noise_name = 'noCAIPI_TR500.nii';
        case 34
            sequence_subdir = 'tse';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'SINC_TR500.nii';
            noise_name = 'SINC_TR500.nii';
        case 35
            sequence_subdir = 'tse';
            scan_subdir = 'caipirinha/TR3000/';
            noise_subdir = 'caipirinha/0V_TR3000/';
            scan_name = '0.00_recombined_notChemShiftCorr.nii';
            noise_name = '0.00_recombined_notChemShiftCorr.nii';
        case 36
            sequence_subdir = 'tse';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'noCAIPI_TR3000.nii';
            noise_name = 'noCAIPI_TR3000.nii';
        case 37
            sequence_subdir = 'tse';
            scan_subdir = 'nii_from_dats/mainScans/';
            noise_subdir = 'nii_from_dats/0V_noiseScans/';
            scan_name = 'SINC_TR3000.nii';
            noise_name = 'SINC_TR3000.nii';

        case 38
            sequence_subdir = 'gre';
            scan_subdir = 'caipirinha/TR150_FAw26_FAf47/';
            noise_subdir = 'caipirinha/TR150_FAw26_FAf47/';
            scan_name = 'mean_0.00_recombined_notChemShiftCorr.nii';
            noise_name = 'diff_0.00_recombined_notChemShiftCorr.nii';
        case 39
            sequence_subdir = 'gre';
            scan_subdir = 'caipirinha/TR150_FAs36/';
            noise_subdir = 'caipirinha/TR150_FAs36/';
            scan_name = 'mean_0.00_recombined_notChemShiftCorr.nii';
            noise_name = 'diff_0.00_recombined_notChemShiftCorr.nii';
        case 40
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/';
            noise_subdir = 'nii_from_dats/';
            scan_name = 'mean_noCAIPI_TR150_FAw26_FAf47_scan1.nii';
            noise_name = 'diff_noCAIPI_TR150_FAw26_FAf47_scan1.nii';
        case 41
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/';
            noise_subdir = 'nii_from_dats/';
            scan_name = 'mean_noCAIPI_TR150_FAs36_scan1.nii';
            noise_name = 'diff_noCAIPI_TR150_FAs36_scan1.nii';
        case 42
            sequence_subdir = 'gre';
            scan_subdir = 'nii_from_dats/';
            noise_subdir = 'nii_from_dats/';
            scan_name = 'mean_SINC_TR150_FAs36_scan1.nii';                                  
            noise_name = 'diff_SINC_TR150_FAs36_scan1.nii';
        case 43
            sequence_subdir = 'tse';
            scan_subdir = 'caipirinha/TR500/';
            noise_subdir = 'caipirinha/TR500/';
            scan_name = 'mean_0.00_recombined_notChemShiftCorr.nii';
            noise_name = 'diff_0.00_recombined_notChemShiftCorr.nii';
        case 44
            sequence_subdir = 'tse';
            scan_subdir = 'nii_from_dats/';
            noise_subdir = 'nii_from_dats/';
            scan_name = 'mean_noCAIPI_TR500_scan1.nii';
            noise_name = 'diff_noCAIPI_TR500_scan1.nii';
        case 45
            sequence_subdir = 'tse';
            scan_subdir = 'nii_from_dats/';
            noise_subdir = 'nii_from_dats/';
            scan_name = 'mean_SINC_TR500_scan1.nii';  
            noise_name = 'diff_SINC_TR500_scan1.nii';            
    end
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   generate readfile names
read_dir_main = fullfile(root_dir, sequence_subdir, subject_subdir, scan_subdir);
readfile_main = fullfile(read_dir_main, scan_name);
read_dir_noise = fullfile(root_dir, sequence_subdir, subject_subdir, noise_subdir);
readfile_noise = fullfile(read_dir_noise, noise_name);

% % make write directory
% s = warning ('query', 'MATLAB:MKDIR:DirectoryExists') ;	    % get current state
% warning ('off', 'MATLAB:MKDIR:DirectoryExists') ;
% mkdir(results_dir);
% warning (s);
% restore state

for iROI=1:size(ROIs,2)
    
    roi_filename_image=fullfile(root_dir, sequence_subdir, subject_subdir, ROIs(iROI));   
    
    [SNR_over_ROI(scan-37, iROI),mean_over_ROI(scan-37,iROI),std_over_ROI(scan-37,iROI)] = calc_iSNR_func_noiseScan_bb(readfile_main, readfile_noise, roi_filename_image);

end

end

SNR_over_ROI
mean_over_ROI
std_over_ROI


disp('Finished');

warning on MATLAB:divideByZero
