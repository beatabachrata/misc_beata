root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/19801024OGZR_20211014_SR/nifti/';
readfile_dirs = {'89'};

for scan = 1:size(readfile_dirs,2)

    load_filename = fullfile(root_dir, readfile_dirs{scan}, 'Image.nii');
    save_filename = fullfile(root_dir, readfile_dirs{scan}, 'reform', 'Image.nii');
    
    s = mkdir(fullfile(root_dir, readfile_dirs{scan}, 'reform'));
    if s == 0
        error('No permission to make directory/m');
    end

    % load 
    nii = load_nii(load_filename);
    nii.img = rescale(nii.img,0,4095);
    nii.hdr.dime.datatype = 64;
    save_nii(nii,save_filename); 
end