clear, clc
dir = '/data2/beata/virtualShare/pulse_files/timeReduction_new/simulation';
% pulse parameters
sampling = 2e-6;        % s 
gyro = 4257.6;
% simulation parameters
sim_thk = 1;            % cm
sim_bw = [-890 450];    % Hz
fmid = [-440 0];        % Hz
FA = 90;
i=0;

for iPulse = 9
    switch iPulse
        case 1
            dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/nice_results/for_paper/pulses/';
            pulse = 'DBSS1_0Pi_FA90_m440Hz_3mm_scaled';
            rf_scale = 0.1;
            gz_max = 3.334666;  % Gauss/cm
            load_saved = 1; 
            caipiPhase = 0;
        case 2
            dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/nice_results/for_paper/pulses/';
            pulse = 'DBSS2_1Pi_FA90_m440Hz_3mm_scaled';
            rf_scale = 0.0743874;
            gz_max = 3.334666;  % Gauss/cm
            load_saved = 1;  
            caipiPhase = pi;
        case 3
            dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/nice_results/for_paper/pulses/';
            pulse = 'new_DBSS1_0Pi_FA90_m440Hz_3mm_scaled';
            rf_scale = 0.1169;
            gz_max = 3.2733948;  % Gauss/cm
            load_saved = 1; 
            caipiPhase = 0;
        case 4
            dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/nice_results/for_paper/pulses/';
            pulse = 'new_DBSS2_1Pi_FA90_m440Hz_3mm_scaled';
            rf_scale = 0.085071;
            gz_max = 3.2733948;  % Gauss/cm
            load_saved = 1;  
            caipiPhase = pi;
            
        case 7
            dir = '/data2/beata/virtualShare/pulse_files/spectral_spatial/pulse_shapes_new/';
            pulse = 'LS_16.24ms_LS_0.56ms_90deg_350Hz_3.5mm';
            rf_scale = 0.1; 
            gz_max = 2.85828487;  % Gauss/cm
            load_saved = 1; 
            FA = 90;
            caipiPhase = 0;
         case 8
            dir = '/data2/beata/virtualShare/pulse_files/spectral_spatial/pulse_shapes_new/';
            pulse = 'LS_16.24ms_SiemensSinc_0.56ms_90deg_350Hz_3.5mm';
            rf_scale = 0.1; 
            gz_max = 2.85828487;  % Gauss/cm
            load_saved = 1; 
            FA = 90;
            caipiPhase = 0;
         case 9
            dir = '/data2/beata/virtualShare/pulse_files/spectral_spatial/pulse_shapes_new/';
            pulse = 'tseRefRF';
            rf_scale = 0.1089; 
%             gz_max = 0.5714;  % Gauss/cm  % 1.4 thick
            gz_max = 0.8000;  % Gauss/cm   % 1.0 thick

            load_saved = 1; 
            caipiPhase = 0;
            sampling = 5e-6;        % s 
            sim_bw = [-2000 2000];    % Hz
            sim_thk = 1.5;            % cm

    end
            
    
    if (load_saved == 0)
%         % Irregular gradient for slabSelective3D
%         g1 = [[1:16]/16, ones(1,103), [16:-1:0]/16]/(600/240);
%         g2 = -[[1:40]/40, ones(1,7), [40:-1:0]/40];
%         g3 = [[1:40]/80, ones(1,7)/2, [40:-1:0]/80];
%         g = ([g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2]);         
%         g = ([g,g3])*gz_max;  

%         % Irregular gradient for slabSelective3D - 2us
%         g1 = [[1:30]/30, ones(1,299), [30:-1:0]/30]/(660/200);
%         g2 = -[[1:100]/100, [99:-1:0]/100];
%         g3 = [[1:30]/60, ones(1,299)/2, [30:-1:0]/60]/(660/200);
%         g = ([g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2]);         
%         g = ([g,g3])*gz_max;  
% 
%         g1 = [[0:18]/18, ones(1,74), [18:-1:0]/18];
%         g2 = [[0:9]/18, ones(1,83)/2, [9:-1:0]/18];
%        % g1 = [[0:40]/40, ones(1,30), [40:-1:0]/40];
%        % g2 = [[0:20]/40, ones(1,50)/2, [20:-1:0]/40];
%         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);         
%         g = ([g,-g2])*gz_max;  
               
%         % Gradient for 2D
%         g1 = [[0:40]/40, ones(1,30), [40:-1:0]/40];
%         g1 = [g1(2:112),0];
%         g_view = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1])*gz_max;         
%         g2 = [[0:20]/40, ones(1,50)/2, [20:-1:0]/40]*gz_max;
%         g_view = ([g_view,-g2]);
%         
% %         % Gradient for 2D under plateau 
%         g1 = [0, ones(1,110), 0];
%         g_sim = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);         
%         g2 = [0,ones(1,55), 0];
%         g_sim = ([g_sim,-g2])*gz_max;

%         % Gradient for 2D - 2us sampling
%         g1 = [[0:100]/100, ones(1,78), [100:-1:0]/100]*gz_max;
%         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);   % 16.24 ms   
% %         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);              % 14 ms    
% %         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);              % 14 ms   
% %         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1]);                             % 12.32 ms      
%         g2 = (-1)*[[0:50]/100, ones(1,128)/2, [50:-1:0]/100]*gz_max;    % 16.24 ms and 11.76 ms
% %         g2 = [[0:50]/100, ones(1,128)/2, [50:-1:0]/100]*gz_max;    % 14.56 ms and 11.20 ms ans 12.32ms
%         g = ([g,g2]);

        % Constant gradient
        g1 = ones(1,512)*gz_max;
        g2 = (-1)*ones(1,256)*gz_max; 
        g = ([g1,g2]);

        mag = zeros(size(g'));
        pha = zeros(size(g'));

        mag_scale = mag*rf_scale;
        re = mag_scale'.*cos(pha'); 
        im = mag_scale'.*sin(pha');
        rf = complex(re,im);
              
        g_view = g;
        
        save(fullfile(dir, sprintf('%s.mat',pulse)),'rf','g_view','g');
        
        
        rf = rf*FA/90;
        [f_plot,z_plot,m_plot] = ss_plot(g, g, rf, rf, sampling, 'se', sim_thk, sim_bw, gyro, fmid, 0, caipiPhase);
        set(gcf,'Name',pulse);
    
    else
        load(fullfile(dir, sprintf('%s.mat',pulse)));            
        rf = rf*FA/90;
        
% %         % Gradient for 2D
%         g1 = [[0:40]/40, ones(1,30), [40:-1:0]/40];
%         g1 = [g1(2:112),0];
%         g_view = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1])*gz_max;         
%         g2 = [[0:20]/40, ones(1,50)/2, [20:-1:0]/40]*gz_max;
%         g_view = ([g_view,-g2]);
%         
%         % Gradient for 2D under plateau 
%         g1 = [ones(1,110), 0, 0];
%         g_sim = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);         
%         g2 = [0,ones(1,55), zeros(1,36)];
%         g_sim = ([g_sim,-g2])*gz_max;

%         % Gradient for 2D - 2us sampling
%         g1 = [[0:100]/100, ones(1,78), [100:-1:0]/100]*gz_max;
%         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);         
%         g2 = [[0:50]/100, ones(1,128)/2, [50:-1:0]/100]*gz_max;
%         g = ([g,-g2]);
%         
%         % Gradient for 2D - 2us sampling
%         g1 = [ones(1,275),0,0,0,0,0]*gz_max;
%         if (iPulse == 36 || iPulse == 37 || iPulse == 38 || iPulse == 45 || iPulse == 46 || iPulse == 47 || iPulse == 48 || iPulse == 49 || iPulse == 50 || iPulse == 63 )
%             g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);   % 16.24 ms  
%         elseif (iPulse == 39 || iPulse == 40 || iPulse == 41 || iPulse == 54 || iPulse == 55 || iPulse == 56 || iPulse == 60 || iPulse == 61 || iPulse == 62)
%             g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);                 % 14 ms    
%         elseif (iPulse == 42 || iPulse == 43 || iPulse == 44 || iPulse == 51 || iPulse == 52 || iPulse == 53 || iPulse == 57 || iPulse == 58 || iPulse == 59 || iPulse == 64 || iPulse == 65 || iPulse == 66 || iPulse == 67)
%             g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);                               % 11.76 ms      
%         elseif ( iPulse == 68 || iPulse == 69 || iPulse == 70 || iPulse == 71)
%             g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1]);                               % 12.32 ms      
%         end
%         
%         if (iPulse == 68 || iPulse == 69 || iPulse == 70 || iPulse == 71)
%             g2 = (1)*[ones(1,275)/2,0,0,0,0,0]*gz_max; % odd
%         else
%             g2 = (-1)*[ones(1,275)/2,0,0,0,0,0]*gz_max; % even
%         end
%         g = ([g,g2]);
%         rf((length(rf)+1):(length(rf)+50)) = 0;         
%         rf=rf(1:length(g));         
        
%         g_sim = g;
        
%         % slab-sel 3D 
%         g1 = [[1:30]/30, ones(1,359), [30:-1:0]/30]/(780/280);
%         g2 = -ones(1,140);
%         g3 = [[1:30]/60, ones(1,359)/2, [30:-1:0]/60]/(780/280);
%         g = ([g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2,g1,g2]);         
%         g = ([g,g3])*gz_max; 
%         
%         rf_new = zeros(size(g));
%         for i = 1:size(rf,2)
%             modI = mod(i,560);
%             if (modI < 30 || modI >= 360)
%                 rf_new(i) = 0; 
%             else
%                 rf_new(i) = rf(i-29);
%             end
%          end
%          rf(3306:3340) = 0; 

%            save(fullfile(dir, sprintf('%s.mat',pulse)),'rf','g_sim','g_view');

%         % Gradient for 2D - 2us sampling
%         g1 = [ones(1,275),0,0,0,0,0]*gz_max;
%         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);   % 16.24 ms  
%         
%         rf_view=rf(1:length(g));         
%         
%         g2 = (-1)*[ones(1,100)]*sum(g1)/2/100; % even
%         g = ([g,g2]);
%         
%         rf((length(rf)+1):(length(rf)+50)) = 0;    
%         rf=rf(1:length(g));
% 
%         g_sim = g;
%         
%         
%         g1 = [[0:100]/100, ones(1,78), [100:-1:0]/100]*gz_max;     
%         g = [g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1];   % 16.24 ms   
%         g2 = (-1)*[[0:50]/100, ones(1,128)/2, [50:-1:0]/100]*gz_max;    % 16.24 ms and 11.76 ms
%         g2 = -g2*((sum(g1)/sum(g2)/2));    % 16.24 ms and 11.76 ms
%         g_view = ([g,g2]);
 
%         g1 = [[0:100]/100, ones(1,78), [100:-1:0]/100]*gz_max;
%         g = ([g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1,-g1,g1]);   % 16.24 ms   
%         g2 = (-1)*[[0:50]/100, ones(1,128)/2, [50:-1:0]/100]*gz_max;    % 16.24 ms and 11.76 ms
%         g = ([g,g2]);

        % Constant gradient
        g1 = ones(1,512);
        g2 = (-1)*ones(1,256); 
        g = ([g1,g2])*gz_max;
        
        mag = abs(rf);
        pha = angle(rf);
        mag_scaled = mag*11.75/0.117998; %11.75/refVoltage (voltage or 1ms long RECT RF pulse - should be constant)
        re = mag_scaled'.*cos(pha'); 
        im = mag_scaled'.*sin(pha');
        rf_view = complex(re,im);

        g_sim = g;
        g_view = g*10;
        
        [f_plot,z_plot,m_plot,m_centre] = ss_plot(g_sim, g_view, rf, rf_view, sampling, 'ex', sim_thk, sim_bw, gyro, fmid, 0, 0);
        set(gcf,'Name',pulse);
        i=i+1;
        m_plot_all(:,:,i) = m_plot;
        m_centre_all(:,:,i) = m_centre;
          
    end

end

