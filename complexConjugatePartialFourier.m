function kspace = complexConjugatePartialFourier(kspace,oversample,fracFOV,isTurbo) 
  
        dims = size(kspace);
        kspace_conj_flipped = conj(flipdim(flipdim(kspace,2),3));
        
        if ( (dims(2)*(1+oversample)) / round(((dims(3) + 2)/fracFOV)) > 2)
            temp = zeros(dims(1), dims(2), round(ceil(dims(2)/2)*(1+oversample)*fracFOV/2)*2, dims(4));
            
            if (isTurbo == 0)
                temp(:,:,((end-dims(3)+1):end),:,:) = kspace_conj_flipped;
                temp(:,:,(1:dims(3)),:,:) = kspace;
            else
                temp(:,:,(((end-dims(3))/2 +1):(end-(end-dims(3))/2)),:,:) = kspace;
            end
            
            kspace = temp;

        else
            fprintf('\nzerofillPartialFourier: partial Fourier was not used - skipping')
        end
        
end