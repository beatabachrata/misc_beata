function [fat_weight,water_weight] = smurf_ff_bias_correction(is7T,TEs)
%% BB: correct for single peak assumption and TE-dependent signal cancellation

    gyro = 42.58;
    if (is7T)
        B0 = 6.980936;
    else
        B0 = 2.89326;
    end

    % FAT
    fat_freq_ppm = [-3.80, -3.40, -2.60];
%     relAmps = [0.087 0.693 0.128]./sum([0.087 0.693 0.128]);
    fat_relAmps = [0.087 0.693 0.128];
    
   % WATER
    water_freq_ppm = [0 -0.39, 0.60];
%     relAmps = [1 0.039 0.048]/sum([1 0.039 0.048]);
    water_relAmps = [1 0.039 0.048];

    
%     if (length(TE) > 1)
%         error('smurf_ff_bias_correction with multiple TEs not implemented')
%         return;
%     end
    
    for iTE = 1:length(TEs)

        phaseOffset = fat_freq_ppm * gyro * B0 * 2 * TEs(iTE) * pi /1000;
        phaseOffset = mod(phaseOffset,2*pi);  
        fat_weight(iTE) = abs(sum(fat_relAmps .* exp(-1i*phaseOffset))); % the acquired proportion of signal

        phaseOffset = water_freq_ppm * gyro * B0 * 2 * TEs(iTE) * pi /1000;
        phaseOffset = mod(phaseOffset,2*pi);  
        water_weight(iTE) = abs(sum(water_relAmps .* exp(-1i*phaseOffset))); % the acquired proportion of signal

    end

end
 





