clear, clc

%% Set common
main_dir='/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19900813KREK_20190826/';
phase_filename = 'phase_poRemoved_comb.nii';
mag_filename = 'mag_poRemoved_comb.nii';
mask_filename = 'mask.nii';
TE = 11; %ms
unwrapped_filename = 'phase_unw_prelude.nii';
fm_filename = 'fm_prelude.nii';


%% Set different
for scan = 1:2
      
    switch scan 
        case 1 
            input_subdir = 'removeAspirePO/3D/iPat3/1.3x1.3x1.3/chemShiftCorr';
            output_subdir = 'QSM/3D/iPat3/1.3x1.3x1.3/chemShiftCorr';
        case 2
            input_subdir = 'removeAspirePO/3D/iPat3/1.3x1.3x1.3/notChemShiftCorr';
            output_subdir = 'QSM/3D/iPat3/1.3x1.3x1.3/notChemShiftCorr';
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    input_dir = fullfile(main_dir, input_subdir);
    output_dir = fullfile(main_dir, output_subdir);
    phase = fullfile(main_dir, input_subdir, phase_filename);
    mag = fullfile(main_dir, input_subdir, mag_filename);
    unwrapped = fullfile(main_dir, output_subdir, unwrapped_filename);
    mask = fullfile(main_dir, mask_filename);
    fm = fullfile(main_dir, output_subdir, fm_filename);

    %% Make directory for results
    if ~exist(output_dir, 'dir')
        mkdir(fullfile(main_dir, output_dir))
    end

    %% Unwrap phase
    unw_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; prelude -p %s -a %s -o %s -m %s -v',  unix_format(phase), unix_format(mag), unix_format(unwrapped), unix_format(mask));
    unix(unw_command);

    %% Rescale phase to fieldmap
    rescale_command = sprintf('export LD_LIBRARY_PATH="/usr/lib/fsl:/usr/lib/fsl"; fslmaths %s -div 2 -div 3.14159265359 -mul 1000 -div %i %s',  unix_format(unwrapped), unix_format(TE), unix_format(fm));
    unix(rescale_command);

    sprintf('***finished case %i ***\n',scan)

end 