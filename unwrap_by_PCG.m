data_for_unwrap.data_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20190823_deadHead/vrc/3D/noPat/recombined_notCorr/';
data_for_unwrap.results_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20190823_deadHead/unwrap_pcg/3D/noPat/';
data_for_unwrap.fn_mask = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20190823_deadHead/vrc/roi_liver.nii';
data_for_unwrap.fn_phase = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20190823_deadHead/vrc/water_fat/comb_p2_echo_1.nii';
data_for_unwrap.max_iter=5; % number of loops/iterations
data_for_unwrap.epsi_con=0.00001;
data_for_unwrap.N=1 ; % number of congurence iteration steps (suggest N=10)

%data_for_unwrap.fn_t2star_map = fullfile(data_for_unwrap.results_dir, 't2starmap.nii');
data_for_unwrap.fn_phase_unwrapped_pcg = fullfile(data_for_unwrap.results_dir, 'phase_uw-pcguw.nii');
data_for_unwrap.fn_phase_unwrapped_lap = fullfile(data_for_unwrap.results_dir, 'phase_uw-lap.nii');

data_for_unwrap.fn_phase_copy = fullfile(data_for_unwrap.results_dir, 'phase.nii');
data_for_unwrap.unwraptype = 'laplacian';

[s,mess] = mkdir(data_for_unwrap.results_dir);
fprintf('Writing results to %s\n', data_for_unwrap.results_dir);
if s == 0
    error('No permission to make directory %s/m', data_for_unwrap.results_dir);
end


PCQ_unwrap_3D_bb(data_for_unwrap);
