function save_nii_uncomb_noCut(kspace, dir, name, is2D)


if (is2D == 1)
    ima = FFTOfMRIData_bb(kspace,0,[2 3],1); 
else
    ima = FFTOfMRIData_bb(kspace,0,[2 3 4],1);
end

ima = permute(ima, [2 3 4 1 5]);

save_nii(make_nii(squeeze(abs(ima))),fullfile(dir,sprintf('mag_%s',name)));
save_nii(make_nii(squeeze(angle(ima))),fullfile(dir,sprintf('phase_%s',name)));


end
