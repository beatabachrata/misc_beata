%% Housekeeping & Definitions
clear, clc, close all
addpath(genpath('/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/matlab'))

% common paths
analysis_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM';
sort_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot';
data.orient = 2; % 1-sag, 2-trans, 3-cor
data.PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
data.is2D = 1;
isCaipiOffsetPiHalf = 0;

%% Case specific definitions
for scan = 1
        
    switch scan 
        case 1
            subj = '19780113DVBN_20200831_headneck';
            sequence = '';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            main_dat_file = 'meas_MID00039_FID52973_gre_3D_inPhaseTE_6p8_13p6_20p4_TR33_iPat3.dat';
            output_subdir = 'removeAspirePO/GRE/';
            data.is2D = 0;
            inplane_voxel_size = 1;
            data.orient = 3; 
            data.PE_dir = 3;  

    end
    
    
%% Path definitions    
data.write_dir = fullfile(analysis_dir, sequence, subj, output_subdir);


%% Load data and reshape
kspace_prescan = mapVBVD(fullfile(sort_dir,subj,'dats',prescan_dat_file));

if (iscell(kspace_prescan))
    kspace_prescan = kspace_prescan{1,end};
end

data.kspace_prescan = reshapeKspace(kspace_prescan.image(),data.is2D,0);
data.kspace_prescan = HammingFilter(data.kspace_prescan,[2,3],25,'OuterProduct',1); 
data.kspace_prescan = flipdim(flipdim(flipdim(data.kspace_prescan,2),3),4);
data.kspace_prescan = zerofillPartialFourier(data.kspace_prescan,0,1,0); 
clear kspace_prescan


kspace_main = mapVBVD(fullfile(sort_dir,subj,'dats',main_dat_file));

if (iscell(kspace_main))
    kspace_main = kspace_main{1,end};
end

data.kspace_main = reshapeKspace(kspace_main.image(),data.is2D,0); 
data.kspace_main = HammingFilter(data.kspace_main,[2,3],25,'OuterProduct',1); 
data.kspace_main = flipdim(flipdim(flipdim(data.kspace_main,2),3),4);
data.kspace_main = zerofillPartialFourier(data.kspace_main,0,1,0); 
clear kspace_main

% final order should be - coil, x(n), echo, y(2n), z


%% Set processing defaults
processing.po_weigthedSmoothing = 0;
processing.aspire_echoes = [1 2];
processing.slicewise = 'all_at_once';
processing.smoothingKernelSizeInMM = 5;
processing.weightedCombination = 1;


%% Get dimensions
dims.n_echoes = size(data.kspace_prescan,5);
dims.n_channels = size(data.kspace_prescan,1);
dims.n_slices = size(data.kspace_prescan,4);
dims.voxel_size = inplane_voxel_size(1);


%% Make directory for results
s = mkdir(data.write_dir);
if s == 0
    error('No permission to make directory %s', data.write_dir);
end


%% CALCULATION
% read in the GRE data and get complex + weight (sum of mag) 
if (data.is2D == 1)
    compl_prescan = FFTOfMRIData_bb(data.kspace_prescan,0,[2 3],1);
    compl_main = FFTOfMRIData_bb(data.kspace_main,0,[2 3],1);
else
    compl_prescan = FFTOfMRIData_bb(data.kspace_prescan,0,[2 3 4],1);
    compl_main = FFTOfMRIData_bb(data.kspace_main,0,[2 3 4],1);
end  
data = rmfield(data,'kspace_prescan');
data = rmfield(data,'kspace_main');

compl_prescan = compl_prescan(:,(size(compl_prescan,2)*0.25 +1):(size(compl_prescan,2)*0.75),:,:,:);
compl_main = compl_main(:,(size(compl_main,2)*0.25 +1):(size(compl_main,2)*0.75),:,:,:);

if (data.orient == 1 && data.PE_dir == 2)
    compl_prescan = permute(compl_prescan,[1,3,2,4,5]);
    compl_main = permute(compl_main,[1,3,2,4,5]);
    compl_prescan = flipdim(compl_prescan,3); 
    compl_main = flipdim(compl_main,3); 
elseif (data.orient == 2 && data.PE_dir == 2)
    compl_prescan = flipdim(compl_prescan,3); 
    compl_main = flipdim(compl_main,3); 
elseif (data.orient == 2 && data.PE_dir == 3)
    compl_prescan = permute(compl_prescan,[1,3,2,4,5]);
    compl_main = permute(compl_main,[1,3,2,4,5]);
elseif (data.orient == 3 && data.PE_dir == 3)
    compl_prescan = permute(compl_prescan,[1,3,2,4,5]);
    compl_main = permute(compl_main,[1,3,2,4,5]);
end

%% Reslice
sliceAcquisitionOrder = getSliceAcquisitionOrder(compl_prescan,data.is2D);
compl_prescan = compl_prescan(:,:,:,sliceAcquisitionOrder.',:); %% not always required
compl_main = compl_main(:,:,:,sliceAcquisitionOrder.',:); %% not always required

% compl_prescan = flipdim(compl_prescan,4); 
% compl_main = flipdim(compl_main,4); 

compl_prescan = permute(compl_prescan,[2,3,4,5,1]);
compl_main = permute(compl_main,[2,3,4,5,1]);


save_nii(make_nii(abs(compl_prescan)), fullfile(data.write_dir, 'mag_prescan.nii'))
save_nii(make_nii(angle(compl_prescan)), fullfile(data.write_dir, 'phase_prescan.nii'))
save_nii(make_nii(abs(compl_main)), fullfile(data.write_dir, 'mag_main.nii'))
save_nii(make_nii(angle(compl_main)), fullfile(data.write_dir, 'phase_main.nii'))



%% get RPO
% hermitian inner product combination
complexDifference = compl_prescan(:,:,:,processing.aspire_echoes(2),:) .* conj(compl_prescan(:,:,:,processing.aspire_echoes(1),:));
hermitian = sum(complexDifference,5);
clear complexDifference


%% subtract hermitian to get PO
po = complex(zeros([size(compl_prescan,1) size(compl_prescan,2) size(compl_prescan,3) size(compl_prescan,5)], 'single'));
for cha = 1:dims.n_channels
    po_temp = double(compl_prescan(:,:,:,processing.aspire_echoes(1),cha)) .* double(conj(hermitian));
    po(:,:,:,cha) = single(po_temp ./ abs(po_temp));
end
save_nii(make_nii(angle(po)), fullfile(data.write_dir, 'po.nii'))
clear po_temp

% if ~processing.po_weigthedSmoothing
%     weight_gre = [];
% else
%     % use the sum of magnitudes as weight (all channels and all echoes summed up)
%     weight_gre = sum(abs(compl_prescan(:,:,:,:)), 4);
% end
clear compl_gre

smoothed_po = complex(zeros(size(po),'single'));
for cha = 1:dims.n_channels
    smoothed_po(:,:,:,cha) = weightedGaussianSmooth(po(:,:,:,cha), ceil(processing.smoothingKernelSizeInMM/dims.voxel_size));
%     smoothed_po(:,:,:,cha) = weightedGaussianSmooth(po(:,:,:,cha), ceil(processing.smoothingKernelSizeInMM/dims.voxel_size), weight_gre);
end             
save_nii(make_nii(angle(smoothed_po)), fullfile(data.write_dir, 'smoothed_po.nii'))
clear po

smoothed_po_highRes = imresize(smoothed_po, [size(compl_main,1),size(compl_main,2)]);
clear smoothed_po


%% adjust for multi-echo main scan
if (ndims(compl_main) > 4)
    for i = 1:size(compl_main,4)
        stacked_smoothed_po_highRes(:,:,:,i,:) = smoothed_po_highRes;
    end
    smoothed_po_highRes = stacked_smoothed_po_highRes; 
    clear stacked_smoothed_po_highRes
end
save_nii(make_nii(angle(smoothed_po_highRes)), fullfile(data.write_dir, 'smoothed_po_highRes.nii'))


%% remove PO from main data
compl_main = squeeze(compl_main) .* squeeze(conj(smoothed_po_highRes)) ./ squeeze(abs(smoothed_po_highRes));

save_nii(make_nii(abs(compl_main)), fullfile(data.write_dir, 'mag_poRemoved.nii'))
save_nii(make_nii(angle(compl_main)), fullfile(data.write_dir, 'phase_poRemoved.nii'))


%% combine main data
combined_main = weightedCombination(compl_main, abs(compl_main));

save_nii(make_nii(abs(combined_main)), fullfile(data.write_dir, 'mag_poRemoved_comb.nii'))
save_nii(make_nii(angle(combined_main)), fullfile(data.write_dir, 'phase_poRemoved_comb.nii'))


%% ratio
weightedMagnitudeSum = weightedCombination(abs(compl_main), abs(compl_main));
ratio = abs(combined_main) ./ weightedMagnitudeSum;
save_nii(make_nii(ratio), fullfile(data.write_dir, 'ratio.nii'))

end

 


