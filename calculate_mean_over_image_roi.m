clear, clc

nCase = 0;

analysis_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20190726_cream/';

%%
for i = 16
    
switch i 

    case 1
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'nii_from_dats';
        caipi_shift_dir = 'caipi_0.5_1.5';
        corr = 'water';
        roi_name = 'roi.nii';
    case 2
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'nii_from_dats';
        caipi_shift_dir = 'caipi_0.5_1.5';
        corr = 'fat';
        roi_name = 'roi_fat.nii';
    case 3
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'caipi';
        caipi_shift_dir = 'caipi_0.5_1.5';
        corr = 'recombined_notChemShiftCorr';
        roi_name = 'roi.nii';
    case 4
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'caipi';
        caipi_shift_dir = 'caipi_0.5_1.5';
        corr = 'water';
        roi_name = 'roi.nii';
    case 5
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'caipi';
        caipi_shift_dir = 'caipi_0.5_1.5';
        corr = 'fat';
        roi_name = 'roi.nii';
    case 6
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'nii_from_dats';
        caipi_shift_dir = 'caipi_0_1';
        corr = 'water';
        roi_name = 'roi.nii';
    case 7
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'nii_from_dats';
        caipi_shift_dir = 'caipi_0_1';
        corr = 'fat';
        roi_name = 'roi_fat.nii';
    case 8
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'caipi';
        caipi_shift_dir = 'caipi_0_1';
        corr = 'recombined_notChemShiftCorr';
        roi_name = 'roi.nii';
    case 9
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'caipi';
        caipi_shift_dir = 'caipi/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20190704/TE_evolution/caipi_0_0_0_1';
        corr = 'water';
        roi_name = 'roi.nii';
    case 10
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'caipi';
        caipi_shift_dir = 'caipi_0_1';
        corr = 'fat';
        roi_name = 'roi.nii';
    case 11
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'nii_from_dats';
        caipi_shift_dir = 'gre';
        corr = 'combined';
        roi_name = 'roi.nii';    
    case 12
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'nii_from_dats';
        caipi_shift_dir = 'caipi_0_0';
        corr = 'combined';
        roi_name = 'roi.nii';    
    case 13s
        TE_array = 10.1:0.2:13.3;
        sub_dir = 'nii_from_dats';
        caipi_shift_dir = 'caipi_1_1';
        corr = 'combined';
        roi_name = 'roi.nii';    

    case 14
        TE_array = 10.5:0.1:17.5;
        sub_dir = 'caipirinha';
        corr = 'fatPhaseCorr';
        roi_name = 'roi.nii';    
    case 15
        TE_array = 10.5:0.1:17.5;
        sub_dir = 'caipirinha';
        corr = 'fatPhaseNotCorr';
        roi_name = 'roi.nii';    
       
end

for iTE = 1:length(TE_array)
    
    %% set directories
    input_dir = fullfile(analysis_dir, sub_dir,sprintf('TE_%.*f',1,TE_array(iTE)));
    % input_dir = fullfile(analysis_dir, sub_dir,caipi_shift_dir);
    output_dir = fullfile(analysis_dir,'TE_evolution_fatPhaseCorr',corr);

    [s,mess] = mkdir(output_dir);
    fprintf('Writing results to %s\n', output_dir);
    if s == 0
        ff_measured('No permission to make directory %s/m', output_dir);
    end


    %% load ROI
    roi_nii = load_nii(fullfile(output_dir,roi_name));
    roi = double(roi_nii.img);

    if strcmp(corr, 'fatPhaseCorr')
        fatPhaseCorr = mod(TE_array(iTE),(1000/434))/(1000/434)*2;
    elseif strcmp(corr, 'fatPhaseNotCorr')
        fatPhaseCorr = 0;
    end

    ima_name = sprintf('%.*f_recombined_chemShiftCorr.nii',2,fatPhaseCorr);

%     if strcmp(sub_dir, 'nii_from_dats')
%         image_nii = load_nii(fullfile(input_dir, sprintf('%s.nii',ima_name)));
%     elseif strcmp(sub_dir, 'caipi')
%         image_nii = load_nii(fullfile(input_dir, ima_name, sprintf('%s.nii',species)));
%     end

    image_nii = load_nii(fullfile(input_dir, ima_name));
    image = double(image_nii.img);
    meanOverROI(iTE) = sum(sum(sum(image.*roi))) / sum(sum(sum(roi)));

end

meanOverROI = meanOverROI * 10^6;

digits(5);
vpa(TE_array)
vpa(meanOverROI)
save(fullfile(output_dir,'TE_signal_dependence'));

%load(fullfile(output_dir,'signal_evolution.mat'));

plot(TE_array,meanOverROI,'x')
title(corr)

end
