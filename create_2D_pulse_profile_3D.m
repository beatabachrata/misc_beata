addpath(genpath('/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/matlab'))
addpath(genpath('/ceph/mri.meduniwien.ac.at/departments/radiology/neuro/home/bbachrata/data/programs/LarsonLab-Spectral-Spatial-RF-Pulse-Design-bb69713'))
clear, clc
dir = '/data2/beata/virtualShare/pulse_files/timeReduction_new/simulation';
% pulse parameters
sampling = 2e-6;        % s 
gyro = 4257.6;
% simulation parameters
sim_thk = 1;            % cm
sim_bw = [-3000 3000];    % Hz
fmid = [-1010 0];        % Hz
FA = 15;

for iPulse = 2
    switch iPulse
         case 1
            dir = '/data2/beata/virtualShare/pulse_files/7T/simulation/0Pi/';
            pulse = 'new_gre_3D_DB_0PiBB_max_8ms_1030Hz_FA15_1p5_0p1_2us';
            rf_scale = 0.00274; 
            
         case 2
            dir = '/data2/beata/virtualShare/pulse_files/3T/simulation/';
            pulse = 'gre_3D_0Pi_BB_max_11p76_350Hz_FA30_1p5_0p1';
            rf_scale = 0.0127; 

         case 3 
            dir = '/data2/beata/virtualShare/pulse_files/7T/simulation/';
            pulse = 'gre_8ms_minPhase_920Hz_1p5_0p1_15deg'; 
            rf_scale = 0.0138; 
    end
            
                
    mag = zeros(10,1);
    pha = zeros(10,1);

    mag_orig = mag;
    pha_orig = pha;
    
%     mag = mag_orig;
%     pha = pha_orig;

    mag_scaled = mag*rf_scale*11.75/0.161; %11.75/refVoltage (voltage or 1ms long RECT RF pulse - should be constant) 0.161 for 7T and 0.117998 for 3T
    re = mag_scaled'.*cos(pha'); 
    im = mag_scaled'.*sin(pha');
    rf = complex(re,im);
    rf = rf*FA/90;

    g_sim = zeros(length(rf),1);

    [f_plot,z_plot,m_plot,m_centre] = ss_plot_onlySpectral(g_sim, g_sim, rf, rf, sampling, 'ex', sim_thk, sim_bw, gyro, fmid, 0);
    set(gcf,'Name',pulse);
          

end

