%% Housekeeping & Definitions
clear, clc, close all
addpath(genpath('/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/matlab'))
addpath(genpath('/ceph/mri.meduniwien.ac.at/departments/radiology/neuro/home/bbachrata/data/programs/QSM'))

% common paths
analysis_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre';
sort_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot';
orient = 3; % 1-sag, 2-trans, 3-cor
PE_dir = 3; % 1 = HF, 2 = AP, 3 = RL
is2D = 0;
phase_oversampling = 0;
PE_FOV_fraction = 1;

%% Case specific definitions
for scan = 27:34
        
    switch scan 
        case 1 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46/recombined_type2Corr_type1Corr_T1Corr_T2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
        case 2
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46/recombined_type2Corr_type1Corr_T1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
        case 3 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46/recombined_type2Corr_type1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_uncomb.nii';
        case 4 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46/recombined_type2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_uncomb.nii';
        case 5 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46/recombined/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_uncomb.nii';
            filename_main_mag = 'mag_recombined_uncomb.nii';
        case 6
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE_6p2_12p4_18p6/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE_6p2_12p4_18p6/recombined_type2Corr_type1Corr_T1Corr_T2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
        case 7
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE_6p2_12p4_18p6/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE_6p2_12p4_18p6/recombined_type2Corr_type1Corr_T1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
        case 8 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE_6p2_12p4_18p6/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE_6p2_12p4_18p6/recombined_type2Corr_type1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_uncomb.nii';
        case 9 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE_6p2_12p4_18p6/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE_6p2_12p4_18p6/recombined_type2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_uncomb.nii';
        case 10 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE_6p2_12p4_18p6/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE_6p2_12p4_18p6/recombined/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_uncomb.nii';
            filename_main_mag = 'mag_recombined_uncomb.nii';
        case 11 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE2_6p5_13p0_19p5/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE2_6p5_13p0_19p5/recombined_type2Corr_type1Corr_T1Corr_T2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
        case 12
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE2_6p5_13p0_19p5/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE2_6p5_13p0_19p5/recombined_type2Corr_type1Corr_T1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
        case 13 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE2_6p5_13p0_19p5/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE2_6p5_13p0_19p5/recombined_type2Corr_type1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_uncomb.nii';
        case 14 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE2_6p5_13p0_19p5/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE2_6p5_13p0_19p5/recombined_type2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_uncomb.nii';
        case 15 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/randomTE2_6p5_13p0_19p5/';
            output_subdir = 'removeAspirePO/caipirinha_new/randomTE2_6p5_13p0_19p5/recombined/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_uncomb.nii';
            filename_main_mag = 'mag_recombined_uncomb.nii';
        case 16 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/recombined_type2Corr_type1Corr_T1Corr_T2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_T2Corr_uncomb.nii';
        case 17
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/recombined_type2Corr_type1Corr_T1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
        case 18 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/recombined_type2Corr_type1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_uncomb.nii';
        case 19 
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/recombined_type2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_uncomb.nii';
        case 20
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/';
            output_subdir = 'removeAspirePO/caipirinha_new/inPhaseTE2_6p82_13p64_20p46_interpPrescan/recombined/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_uncomb.nii';
            filename_main_mag = 'mag_recombined_uncomb.nii';
            
        case 21
            subj = '19780113DVBN_20200831_headneck';
            prescan_dat_file = 'meas_MID00032_FID52966_ke_gre_aspire_1_2_prescan.dat';
            caipi_subdir = 'grappa/GRE_inPhase/';
            output_subdir = 'removeAspirePO/GRE/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_unaliased_uncomb.nii';
            filename_main_mag = 'mag_unaliased_uncomb.nii';

        case 22
            subj = 'alex_7T';
            prescan_dat_file = 'meas_MID00569_FID15191_gre_prescan.dat';
            caipi_subdir = 'grappa/';
            output_subdir = 'removeAspirePO/GRE/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_unaliased_uncomb.nii';
            filename_main_mag = 'mag_unaliased_uncomb.nii';

        case 23
            subj = '19930611LNNH_20210201_panobrama';
            prescan_dat_file = 'meas_MID01142_FID09234_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/';
            output_subdir = 'removeAspirePO/recombined_type2Corr_type1Corr_T1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
        case 24
            subj = '19930611LNNH_20210201_panobrama';
            prescan_dat_file = 'meas_MID01142_FID09234_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/';
            output_subdir = 'removeAspirePO/recombined_type2Corr_type1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_uncomb.nii';
        case 25
            subj = '19930611LNNH_20210201_panobrama';
            prescan_dat_file = 'meas_MID01142_FID09234_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/';
            output_subdir = 'removeAspirePO/recombined_type2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_uncomb.nii';
        case 26
            subj = '19930611LNNH_20210201_panobrama';
            prescan_dat_file = 'meas_MID01142_FID09234_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/';
            output_subdir = 'removeAspirePO/recombined/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_uncomb.nii';
            filename_main_mag = 'mag_recombined_uncomb.nii';
        case 27
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01175_FID09267_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/10slc';
            output_subdir = 'removeAspirePO/10slc/recombined_type2Corr_type1Corr_T1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
        case 28
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01175_FID09267_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/10slc';
            output_subdir = 'removeAspirePO/10slc/recombined_type2Corr_type1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_uncomb.nii';
        case 29
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01175_FID09267_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/10slc';
            output_subdir = 'removeAspirePO/10slc/recombined_type2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_uncomb.nii';
        case 30
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01175_FID09267_ke_gre_aspireTest_prescan.dat';
            caipi_subdir = 'caipirinha/10slc';
            output_subdir = 'removeAspirePO/10slc/recombined/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_uncomb.nii';
            filename_main_mag = 'mag_recombined_uncomb.nii';
        case 31
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01177_FID09269_ke_gre_aspireTest_prescan_12slc.dat';
            caipi_subdir = 'caipirinha/12slc';
            output_subdir = 'removeAspirePO/12slc/recombined_type2Corr_type1Corr_T1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_T1Corr_uncomb.nii';
        case 32
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01177_FID09269_ke_gre_aspireTest_prescan_12slc.dat';
            caipi_subdir = 'caipirinha/12slc';
            output_subdir = 'removeAspirePO/12slc/recombined_type2Corr_type1Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_type1Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_type1Corr_uncomb.nii';
        case 33
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01177_FID09269_ke_gre_aspireTest_prescan_12slc.dat';
            caipi_subdir = 'caipirinha/12slc';
            output_subdir = 'removeAspirePO/12slc/recombined_type2Corr/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_type2Corr_uncomb.nii';
            filename_main_mag = 'mag_recombined_type2Corr_uncomb.nii';
        case 34
            subj = '19841222SGRA_20210201_panobrama';
            prescan_dat_file = 'meas_MID01177_FID09269_ke_gre_aspireTest_prescan_12slc.dat';
            caipi_subdir = 'caipirinha/12slc';
            output_subdir = 'removeAspirePO/12slc/recombined/';
            inplane_voxel_size = 1;
            filename_main_phase = 'phase_recombined_uncomb.nii';
            filename_main_mag = 'mag_recombined_uncomb.nii';
    end
    
    
%% Set processing defaults
processing.aspire_echoes = [1 2];
processing.slicewise = 'all_at_once';
processing.smoothingKernelSizeInMM = 5;
processing.weightedCombination = 1;

% Path definitions    
write_dir = fullfile(analysis_dir, subj, output_subdir);

% Make directory for results
s = mkdir(write_dir);
if s == 0
    error('No permission to make directory %s/m', write_dir);
end


%% Load prescan data
kspace_prescan_path = fullfile(sort_dir,subj,'dats/',prescan_dat_file);
[kspace_prescan, params] = loadData(kspace_prescan_path);

 
%% Preprocess
kspace_prescan = flip(flip(kspace_prescan,1),2);
kspace_prescan = zerofillPartialFourier(kspace_prescan,params); 
sliceOrder = getSliceAcquisitionOrder(kspace_prescan,params);
kspace_prescan = kspace_prescan(:,:,:,sliceOrder.',:); %% not always required

% Fourier transform to image space
if (params.is2D == 1)
    ima_prescan = FFTOfMRIData_bb(kspace_prescan,0,[2 3],1);
else
    ima_prescan = FFTOfMRIData_bb(kspace_prescan,0,[2 3 4],1);
end  
clear kspace_prescan

% 
ima_prescan = permute(ima_prescan,[2,3,4,5,1]);
ima_prescan = ima_prescan((size(ima_prescan,1)*0.25 +1):(size(ima_prescan,1)*0.75),:,:,:,:);

% Save prescan nii
save_nii(make_nii(sum(abs(ima_prescan),5)), fullfile(write_dir, 'SoS_mag_prescan.nii'))
save_nii(make_nii(abs(ima_prescan)), fullfile(write_dir, 'mag_prescan.nii'))
% save_nii(make_nii(angle(ima_prescan)), fullfile(write_dir, 'phase_prescan.nii'))

% Get dimensions
dims.n_slices = size(ima_prescan,3);
dims.n_echoes = size(ima_prescan,4);
dims.n_channels = size(ima_prescan,5);
dims.voxel_size = inplane_voxel_size; 


%% Read in the main data and get complex 
phase_main_nii = load_nii(fullfile(analysis_dir, subj, caipi_subdir, filename_main_phase));
mag_main_nii = load_nii(fullfile(analysis_dir, subj, caipi_subdir, filename_main_mag));

mag_main = single(mag_main_nii.img);
phase_main = single(rescale(phase_main_nii.img, -pi, pi));
clear mag_main_nii phase_main_nii

% Get complex data
compl_main = mag_main .* exp(single(1i * phase_main));
clear mag_main phase_main

% compl_main = flipdim(compl_main,4);
compl_main = permute(compl_main,[1,2,3,5,4]);

% % Add zero line if necessary for rectangular matrix
% dim_tmp = size(compl_main);
% if ((dim_tmp(1) ~= dim_tmp(2)) && ((dim_tmp(1) + 1) == dim_tmp(2)))
%     dim_tmp(1) = dim_tmp(1) + 1;
%     compl_main_rect = zeros(dim_tmp);
%     compl_main_rect(1:(end-1),:,:,:,:) = compl_main;
%     compl_main = compl_main_rect;
%     clear compl_main_r
% end

% Save main nii
save_nii(make_nii(sum(abs(compl_main),length(size(compl_main)))), fullfile(write_dir, 'SoS_mag.nii'))
save_nii(make_nii(abs(compl_main)), fullfile(write_dir, 'mag.nii'))
save_nii(make_nii(angle(compl_main)), fullfile(write_dir, 'phase.nii'))


%% Get RPO
% Hermitian inner product combination
complexDifference = ima_prescan(:,:,:,processing.aspire_echoes(2),:) .* conj(ima_prescan(:,:,:,processing.aspire_echoes(1),:));
hermitian = sum(complexDifference,5);
clear complexDifference

% Subtract hermitian to get PO
po = complex(zeros([size(ima_prescan,1) size(ima_prescan,2) size(ima_prescan,3) size(ima_prescan,5)], 'single'));
for cha = 1:dims.n_channels
    po_temp = double(ima_prescan(:,:,:,processing.aspire_echoes(1),cha)) .* double(conj(hermitian));
    po(:,:,:,cha) = single(po_temp ./ abs(po_temp));
end
clear po_temp hermitian ima_prescan
save_nii(make_nii(angle(po)), fullfile(write_dir, 'po.nii'))

% Smooth PO
weight_gre = ones(size(po));
smoothed_po = complex(zeros(size(po),'single'));
for cha = 1:dims.n_channels
    smoothed_po(:,:,:,cha) = weightedGaussianSmooth(po(:,:,:,cha), ceil(processing.smoothingKernelSizeInMM/dims.voxel_size), weight_gre(:,:,:,cha));
end             
clear po weight_gre
smoothed_po_highRes = imresize(smoothed_po, [size(compl_main,1),size(compl_main,2)]);
clear smoothed_po


%% Remove PO from main data
for i = 1:size(compl_main,4)
    compl_main(:,:,:,i,:) = squeeze(compl_main(:,:,:,i,:)) .* squeeze(conj(smoothed_po_highRes)) ./ squeeze(abs(smoothed_po_highRes));
end
clear smoothed_po_highRes

% save_nii(make_nii(abs(compl_main)), fullfile(write_dir, 'mag_poRemoved.nii'))
% save_nii(make_nii(angle(compl_main)), fullfile(write_dir, 'phase_poRemoved.nii'))


% Combine main data
combined_main = weightedCombination(compl_main, abs(compl_main));

save_nii(make_nii(abs(combined_main)), fullfile(write_dir, 'mag_poRemoved_comb.nii'))
save_nii(make_nii(angle(combined_main)), fullfile(write_dir, 'phase_poRemoved_comb.nii'))


% Calculate Q metric
weightedMagnitudeSum = weightedCombination(abs(compl_main), abs(compl_main));
ratio = abs(combined_main) ./ weightedMagnitudeSum;
save_nii(make_nii(ratio), fullfile(write_dir, 'ratio.nii'))

clear compl_main combined_main weightedMagnitudeSum ratio


fprintf('Processing scan %i finished!\n\n\n',scan)


end

 

