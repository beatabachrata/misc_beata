function save_nii_from_kspace_comb_noCut_multiecho(kspace, dir, name, is2D)

    if (is2D)
        ima = FFTOfMRIData_bb(kspace,0,[2 3],1); 
    else
        ima = FFTOfMRIData_bb(kspace,0,[2 3 4],1);
    end

    ima_nii = make_nii(squeeze(abs(ima)));
    save_nii(ima_nii,fullfile(dir,sprintf('%s.nii',name)));

end