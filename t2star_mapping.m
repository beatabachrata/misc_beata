clear, clc


for run = 3
    
    switch run
        case 1
            image_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/19790424BRBC/nifti/6/reform/Image.nii';
            TEs = [4 8]; % TEs, in ms
            write_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19790424BRBC_201904231030/t2star';
        case 2
            image_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/19921204YVFD_201905011315/nifti/5/reform/Image.nii';
            TEs = [2.3 4.6]; % TEs, in ms
            write_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19921204YVFD_201905011315/t2star';
            
        case 3
            image_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19950915AEL_20201029_tumorProtTest/aspire/sag_iPat3_4randomEchoes/recombined_type2Corr_type1Corr/results/combined_mag.nii';
            TEs = [4 8]; % TEs, in ms
            write_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19950915AEL_20201029_tumorProtTest/t2star/sag_iPat3_4randomEchoes/recombined_type2Corr_type1Corr';

    end
    
    
    %   Make directory for results
    [s,mess] = mkdir(write_dir);
    if s == 0
        error('No permission to make directory %s', write_dir);
    end

    ima_nii = load_nii(image_dir);
    echo1 = single(ima_nii.img(:,:,:,1));
    echo2 = single(ima_nii.img(:,:,:,2));
    
    ratio = echo1./echo2;
    t2star = ((TEs(2)-TEs(1)))./log(ratio);
    
    for i = 1:size(ratio(:))
        if (abs(ratio(i)) < 1.01)
            t2star(i) = 0;
        end
    end
        
%     diff = echo1-echo2;
    save_nii(make_nii(t2star),fullfile(write_dir,'t2star.nii'));
%     save_nii(make_nii(diff),fullfile(write_dir,'diff.nii'));


end