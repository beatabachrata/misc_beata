function [SNR,signal,noise] = calc_tSNR_func_bb(image_4D, write_dir)

x = size(image_4D,1);
y = size(image_4D,2);
z = size(image_4D,3);

SNR = zeros(x,y,z);
signal = zeros(x,y,z);
noise = zeros(x,y,z);

for slice = 1:z
    one_slice = squeeze(image_4D(:,:,slice,:));
    
    %calculate tSNR
    SNR(:,:,slice) = mean(one_slice, 3)./std(one_slice, 1, 3);
    signal(:,:,slice) = mean(one_slice, 3);
    noise(:,:,slice) = std(one_slice, 1, 3);
end

%turn this into an nii image
SNR(isnan(SNR))=0;
save_nii(make_nii(SNR), fullfile(write_dir, 'tSNR.nii'));


%   Signal
signal(isnan(signal))=0;
save_nii(make_nii(signal), fullfile(write_dir, 'mean_signal.nii'));

%   Noise
save_nii(make_nii(noise), fullfile(write_dir, 'noise.nii'));

