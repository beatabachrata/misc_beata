function [fitresult, gof] = fit_signal_evolution(fit_TE, fit_signal, model)

    % Initialize arrays to store fits and goodness-of-fit.
    fitresult = cell( 2, 1 );
    gof = struct( 'sse', cell( 2, 1 ), 'rsquare', [], 'dfe', [], 'adjrsquare', [], 'rmse', [] );

    [xData, yData] = prepareCurveData( fit_TE, fit_signal );

    % Set up fittype and options
    ft = fittype( model, 'independent', 'TE', 'dependent', 'y' );
    opts = fitoptions( 'Method', 'NonlinearLeastSquares');

    % Fit model to data
    [fitresult{1}, gof(1)] = fit( xData, yData, ft, opts );

    % Plot fit with data.
    plot( fitresult{1}, xData, yData );
    legend('data', 'fit');
    xlabel('TE')
    ylabel('signal')
    grid on

end


