function save_nii_uncomb_from_kspace(kspace, dir, name, is2D, orient_sag, phase_oversampling, remove_phase_oversampling)

if(~exist('remove_phase_oversampling','var'))
	remove_phase_oversampling = 1;
end

if (is2D == 1)
    ima = FFTOfMRIData_bb(kspace,1,[2 3],1);
    ima = ima(:,(size(ima,2)*0.25 +1):(size(ima,2)*0.75),:,:);

    if (remove_phase_oversampling == 1)
        dim_withOver = size(ima,3);
        dim_noOver = floor(((size(ima,3))/(1+phase_oversampling))/4)*4; % is multiple of 4 (for TSE the increment are actually 16 and for GRE 2)
        ima = ima(:,:,(floor(dim_withOver/2) - dim_noOver/2 + 1):(floor(dim_withOver/2) + dim_noOver/2),:);
    end

    if (orient_sag == 1 && isPEHF == 0)
        ima = permute(ima,[3,2,4,1]);
        ima = ima(:,size(ima,2):-1:1,:,:);
    else
        ima = permute(ima,[2,3,4,1]);
    end  
    
else
    ima = FFTOfMRIData_bb(kspace,1,[2 3 4],1);
    ima = ima(:,(size(ima,2)*0.25 +1):(size(ima,2)*0.75),:,:);

    if (remove_phase_oversampling == 1)
        dim_withOver = size(ima,3);
        dim_noOver = floor(((size(ima,3))/(1+phase_oversampling))/4)*4; % is multiple of 4 (for TSE the increment are actually 16 and for GRE 2)
        ima = ima(:,:,(floor(dim_withOver/2) - dim_noOver/2 + 1):(floor(dim_withOver/2) + dim_noOver/2),:);
    end

    if (orient_sag == 1 && isPEHF == 0)
        ima = permute(ima,[3,2,4,1]);
        ima = ima(:,size(ima,2):-1:1,:,:);
    else
        ima = permute(ima,[2,3,4,1]);
    end    
    
end

mag_nii = make_nii(squeeze(abs(ima)));
phase_nii = make_nii(squeeze(angle(ima)));

save_nii(mag_nii,fullfile(dir,sprintf('mag_%s',name)));
save_nii(phase_nii,fullfile(dir,sprintf('phase_%s',name)));

end