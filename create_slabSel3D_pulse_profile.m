clear, clc
dir = '/data2/beata/virtualShare/pulse_files/3T_slabSel3D/simulation';

% pulse parameters
sampling = 2e-6;        % s 
gyro = 4257.6;
% simulation parameters
% sim_thk = 2;            % cm
% sim_bw = [-2000 2000];    % Hz
% fmid = [-1010 0];        % Hz
sim_thk = 30;            % cm
sim_bw = [-1200 800];    % Hz
fmid = [-440 0];        % Hz

FA = 90;
i=0;


for iPulse = 7
    
    switch iPulse
         case 1 % 
            pulse = 'gre_slabSel3D_SBSS_SINC'; 
            load_saved = 1; 
            rf_scale = 0.0269; 
            gz_max = 0.11727;  % Gauss/cm
         case 2 % 
            pulse = 'gre_slabSel3D_SBSS_SLR10kHz'; 
            load_saved = 1; 
            rf_scale = 0.0749; 
            gz_max = 0.11727;  % Gauss/cm
         case 3 % 
            pulse = 'gre_slabSel3D_DBSS_SINC'; 
            load_saved = 1; 
            rf_scale = 0.04992; 
            gz_max = 0.11727;  % Gauss/cm
         case 4 % 
            pulse = 'gre_slabSel3D_DBSS_SLR10kHz'; 
            load_saved = 1; 
            rf_scale = 0.1343244; 
            gz_max = 0.11727;  % Gauss/cm
         case 5 % 
            pulse = 'gre_slabSel3D_DBSSpriorPhases_SLR10kHz'; 
            load_saved = 1; 
            rf_scale = 0.1343244; 
            gz_max = 0.11727;  % Gauss/cm
         case 6 % 
            pulse = 'gre_slabSel3D_trainOfSubpulsesDB_SLR10kHz'; 
            load_saved = 1; 
            rf_scale = 0.01; 
            gz_max = 0.11727;  % Gauss/cm
         case 7 % 
            pulse = 'gre_slabSel3D_DB_SLR10kHz'; 
            load_saved = 1; 
            rf_scale = 0.04; 
            gz_max = 0.11727;  % Gauss/cm
         case 8 % 
            pulse = 'gre_slabSel3D_trainOfSubpulsesSB_SLR10kHz'; 
            load_saved = 1; 
            rf_scale = 0.04; 
            gz_max = 0.11727;  % Gauss/cm
         case 9 % 
            pulse = 'gre_slabSel3D_SB_SLR10kHz'; 
            load_saved = 1; 
            rf_scale = 0.01; 
            gz_max = 0.11727;  % Gauss/cm

    end
    
    if (load_saved == 0)

        %%% Rectangular gradient is used for simulation - in IDEA
        %%% setFrequencyProportionalToGradient is used in combination with
        %%% trapezoidal oscillating gradients as used for display here
        
        % Gradient for simulation
        % 2D - 2us sampling
        g_sim_sub = [ones(1,355),-(355/205)*ones(1,205)];
        g_sim_ref = [ones(1,355)/2,0,0,0,0,0]; % odd
        g_sim_osc = ([g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub,g_sim_sub]);                                 
        g_sim = ([g_sim_osc,g_sim_ref])*gz_max;     
        
        mag = zeros(length(g_sim),1);
        pha = zeros(length(g_sim),1);

        mag_scale = mag*rf_scale;
        re = mag_scale'.*cos(pha'); 
        im = mag_scale'.*sin(pha');
        rf = complex(re,im);
                        
        save(fullfile(dir, sprintf('%s.mat',pulse)),'rf','g_sim');        
        
        return;
        
    else
                
        load(fullfile(dir, sprintf('%s.mat',pulse)),'rf','g_sim'); 
        rf = rf*FA/90;
        rf_sim = rf;

        [max_rf,index] = max(abs(rf_sim));
        index = index + 2;
        iso=(length(rf_sim)-index)*sampling;
            
%             
%         if (iPulse == 25 || iPulse == 26)
%             iso = 1400/1e6;
%         end
%             iso = iso + (length(rf_sim)-length(rf))*sampling; 
%             teContribution = (length(rf)-index)*sampling*1000
%         else 
%             iso=(length(rf)/2+(length(rf_sim)-length(rf)))*sampling; %% includes the refGrad duration
%         end
            
        % Rescale RF to uT
        mag = abs(rf);
        pha = angle(rf);
        mag_scaled = mag*11.75/0.117998; %11.75/refVoltage (voltage or 1ms long RECT RF pulse - should be constant)
        re = mag_scaled'.*cos(pha'); 
        im = mag_scaled'.*sin(pha');
        rf_view = complex(re,im);
        rf_view = rf_view(1:7840);
        
        [f_plot,z_plot,m_plot,m_centre] = ss_plot(g_sim, g_sim*10, rf, rf_view, sampling, 'ex', sim_thk, sim_bw, gyro, fmid, iso);

%         [f_plot,z_plot,m_plot,m_centre] = ss_plot_onlySpectral(g_sim, g_sim*10, rf_sim, rf_view, sampling, 'ex', sim_thk, sim_bw, gyro, fmid, iso);
        set(gcf,'Name',pulse);
        i=i+1;
        m_plot_all(:,:,i) = m_plot;
        m_centre_all(:,:,i) = m_centre;
        rf_view_all(:,i) = 1000;
        rf_view_all(1:length(rf_view),i) = rf_view;

   end

end

% close all
% 
% figure;
% z = [-sim_thk/2:(sim_thk/99):sim_thk/2];
% f = [sim_bw(1):(diff(sim_bw)/99):sim_bw(2)];
% f_fine = [sim_bw(1):(diff(sim_bw)/499):sim_bw(2)];
%     
% 
% nColumns = 3;
% for i=1:size(m_plot_all,nColumns)
% 
%     % RF 
%     s1 = subplot((size(m_plot_all,3)), nColumns, (i-1)*nColumns+nColumns-2);
%     for j = 1:length(rf_view_all(:,i))
%         if rf_view_all(j,i) == 1000
%             j = j-1;
%             break
%         end
%     end
%     t_rf = [0:length(rf_view_all(1:j,i))-1] * sampling * 1e3;
%     plot(t_rf,abs(rf_view_all(1:j,i)));
%     ax1 = gca;
%     
%     ylabel('RF [\muT]]','FontSize',14,'Color','k');
%     xlim([0,ceil(length(rf_view_all) * sampling * 1e3)])
%     ylim([-0.5,12])
%     
%     if (i == 1)
%         title('Dual-Band Spatial-Spectral RF Waveform','FontSize',14);
%         set(ax1,'Xticklabel',[])
%     elseif (i == size(m_plot_all,3))
%         xlabel('Time [ms]','FontSize',14);
%     else
%         set(ax1,'Xticklabel',[])
%     end
%     set(ax1,'fontsize',12) 
% 
%     s1.Position = s1.Position + [-0.1 -0.06+i*0.01 0.06 0.06];
%     
%     
%     
%     % Spectral plot at z = 0
%     s2 = subplot((size(m_plot_all,3)), nColumns, (i-1)*nColumns+nColumns-1);
%     pha = angle(m_centre_all(:,:,i));
%     pha = (pha)/pi ;
% 
%     [ax2,hLine3,hLine4] = plotyy(f_fine,abs(m_centre_all(:,:,i)),f_fine,pha);
% 
%     ylabel(ax2(1),'abs(M_{xy})','FontSize',14,'Color','k');
%     ylabel(ax2(2),'angle(M_{xy}) [\pi]','FontSize',14,'Color','r') % right y-axis
%     
%     ax2(1).YTick = 0:0.25:1; 
%     set(ax2(1),'YLim',[-0.1 1.1])
% %     ax1(1).YTick = 0:0.1:0.5; 
% %     set(ax1(1),'YLim',[-0.05 0.55])
%     ax2(2).YTick = -1:0.5:1;
%     set(ax2(2),'YLim',[-1.2 1.2])
% %     ax2(2).YTickLabels = {'0','0.4','0.8','1.2','1.6','2'};
% 
%     set(ax2,{'ycolor'},{'k';'r'})
%     
%     hLine3.LineStyle = '-';
%     hLine3.Color = 'k';
%     hLine4.LineStyle = ':';
%     hLine4.Color = 'r';
%     hLine4.LineWidth = 1.3;
% 
% %     hold on;
% %     plot(zeros(1,500)+0.5,':r','LineWidth',0.5);
% 
%     if (i == 1)
%         title('Spectral Excitation Profile','FontSize',14);
%         set(ax2,'Xticklabel',[])
%     elseif (i == size(m_plot_all,3))
%         xlabel('Frequency [Hz]','FontSize',14);
%     else
%         set(ax2,'Xticklabel',[])
%     end
%     set(ax2,'fontsize',12) 
%     set(ax2,'XLim',[-860,420])
%     ylabel('abs(M_{xy})','FontSize',14);
% 
%     s2.Position = s2.Position + [-0.055 -0.06+i*0.01 0.06 0.06];
% 
%     
%     
%     % abs(Mxy)
%     s3 = subplot((size(m_plot_all,3)), nColumns, (i-1)*nColumns+nColumns);
%     imagesc(f,z*10,abs(m_plot_all(:,:,i)));
%     colormap(s3, 'jet')  % jet, hot, gray
%     ylabel('Position [mm]','FontSize',14);
%     ax3 = gca;
%     ax3.YTick = -4.5:1.5:4.5;    
%     if (i == 1)
%         title('Magnitude of Spatial-Spectral Excitation Profile','FontSize',14)
%         set(ax3,'Xticklabel',[])
%     elseif (i == size(m_plot_all,3))
%         xlabel('Frequency [Hz]','FontSize',14);
%     else
%         set(ax3,'Xticklabel',[])
%     end
%     set(ax3,'fontsize',12) 
%     
%     s3.Position = s3.Position + [0.02 -0.06+i*0.01 0.06 0.06];
%   
%     
% end
% 
% 
