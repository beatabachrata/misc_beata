function save_kspace(kspace, dir, name, params)

if(~exist('params.remove_phase_oversampling','var'))
	params.remove_phase_oversampling = 0;
end


kspace_SoS = squeeze(sqrt(sum((abs(kspace).^2),1)));
kspace_SoS = kspace_SoS((size(kspace_SoS,1)*0.25 + 1):(size(kspace_SoS,1)*0.75),:,:);


if (params.remove_phase_oversampling == 1 && params.phase_oversampling ~= 0)
    dim_withOver = size(kspace_SoS,2);
    dim_noOver = floor(((size(kspace_SoS,2))/(1+phase_oversampling))/4)*4; % is multiple of 4 (for TSE the increment are actually 16 and for GRE 2)
    kspace_SoS = kspace_SoS(:,(floor(dim_withOver/2) - dim_noOver/2 + 1):(floor(dim_withOver/2) + dim_noOver/2),:);
end


orient = params.orient;
PE_dir = params.PE_dir;
if (orient == 1 && (PE_dir == 2 || PE_dir == 3))
    kspace_SoS = permute(kspace_SoS,[2,1,3]);
    kspace_SoS = flipdim(kspace_SoS,2);
elseif (orient == 2 && PE_dir == 2)
    kspace_SoS = flipdim(flipdim(kspace_SoS,2),3);
elseif (orient == 2 && PE_dir == 3)
    kspace_SoS = permute(kspace_SoS,[2,1,3]);
elseif (orient == 3 && PE_dir == 3)
    kspace_SoS = permute(kspace_SoS,[2,1,3]);
end
    

kspace_SoS_nii = make_nii(squeeze(kspace_SoS));
save_nii(kspace_SoS_nii,fullfile(dir,name));
    

end