% SNR_calc_main.m
% Simon Robinson. 31.10.2006
% Calculating Image SNR on the basis of ROIs, which can be defined and saved here
% and/or Time-Series SNR, which is done on a pixel-by-pixel basis
% Calculation done in calc_tSNR_func

% clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set parameters and counters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off MATLAB:divideByZero

%   pulse shape analysis 
root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM';
% subject_subdir = '19930611LNNH_20210519_headneck3T/QSM';
subject_subdir = '19980113RPCZ_20210426_headneck7T/QSM';
QSM_subdir = 'results/romeo_pdf_star_ivw';
caipi_subdir = 'caipirinha/random';
image_name = 'sepia_QSM.nii';
unzip = true;

cf_background = false ; % true - calculates SNR values w.r.t a background ROI, false - just calculates mean values in the ROI
ROI_background = 'bkg.nii';
% ROIs = {'red_nuclei_left.nii','red_nuclei_right.nii','pallidum_left.nii','pallidum_right.nii','putamen_left.nii','putamen_right.nii','caudate_left.nii','caudate_right.nii'};  
ROIs = {'putamen_right_ero.nii'};  
% ROIs = {'allDGM.nii'};


for scan = 1:7
    
    switch scan
        case 1
            corr_subdir = 'brain_echo1_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 2
            corr_subdir = 'brain_echo2_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 3
            corr_subdir = 'brain_echo3_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 4
            corr_subdir = 'brain_echo4_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 5
            corr_subdir = 'brain_echo5_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 6
            corr_subdir = 'brain_echo6_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
        case 7
            corr_subdir = 'brain_recombined_type2Corr_type1Corr_T1Corr_T2Corr';
    end
    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   generate readfile names
read_dir = fullfile(root_dir, subject_subdir, caipi_subdir, corr_subdir, QSM_subdir);
read_file = fullfile(read_dir, image_name);

if (unzip)
    zip_read_file = fullfile(read_dir, sprintf('%s.gz',image_name));
    gunzip(zip_read_file,read_dir);
end

write_dir = fullfile(root_dir, subject_subdir, caipi_subdir);

SNR_over_ROI = zeros(1,size(ROIs,2));
    
for iROI=1:size(ROIs,2)
    
    roi_name = fullfile(root_dir, subject_subdir, caipi_subdir, 'rois_SNR', ROIs(iROI));   
     
    if (cf_background)
        roi_bkg_name = fullfile(root_dir, subject_subdir, caipi_subdir, 'rois', ROI_background);  
    else
        roi_bkg_name = 'none';
    end
    
    SNR_over_ROI(iROI) = calc_SNR_func_bb(read_file, roi_name, cf_background, roi_bkg_name);

end

SNR_over_ROI

end

disp('Finished');

warning on MATLAB:divideByZero
