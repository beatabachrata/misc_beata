%% Version 1.0
% 16.12.2021
% Written by Beata Bachrata

% This script allows reconstruction of images from the raw data (only without GRAPPA acceleration)
% Image orientation and the path to dat files have to be defined.


%% Housekeeping & Definitions
clear;
close all;

save_uncomb = true; % required for fat-water recombination 


%% Define data paths and acquisition orientation
for scan = 3:4
   
    orient = 2; % 1-sag, 2-trans, 3-cor
    PE_dir = 2; % 1 = HF, 2 = AP, 3 = RL
    deleteSeparateEchoes = true;
    output_name = 'image.nii';

    switch scan 
        case 1
            output_dir = '/ceph/mri.meduniwien.ac.at/departments/radiology/neuro/home/bbachrata/data/sequences/SMURF/reco/test_data_output/coventional_2D';
            dat_file = '/ceph/mri.meduniwien.ac.at/departments/radiology/neuro/home/bbachrata/data/sequences/SMURF/reco/test_data/conventional_2D/meas_MID00723_FID59155_bb_smurftse_inPha_2D.dat';
        case 3
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/spine_20220329/aspire_monopol/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/spine_20220329/dats/meas_MID00154_FID90262_aspire_2d_monopolar_inphase_GRE_4_4_7_axial.dat';
        case 4
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/spine_20220329/aspire_monopolFatSat/';
            dat_file = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/spine_20220329/dats/meas_MID00164_FID90272_aspire_2d_monopolar_inphase_GRE_4_4_7_axial_fat_sat.dat';
    end
    
    
    %% Create output directory
    [s,mess] = mkdir(output_dir);
    fprintf('Writing results to %s\n', output_dir);
    if s == 0
        error('No permission to make directory %s/m', output_dir);
    end
    
    
    %% Load data (for memory purposes deleted while processing the first echo)
    [kspace, params] = loadData(dat_file);  
    params.orient = orient; clear orient
    params.PE_dir = PE_dir; clear PE_dir
    

    %% Preparation steps
    kspace = flip(flip(flip(kspace,1),2),3);
    kspace = zerofillPartialFourier(kspace,params); 
    sliceOrder = getSliceAcquisitionOrder(kspace,params);
    kspace = kspace(:,:,:,sliceOrder.',:); 

%     if (~params.is2D)
        kspace = flip(kspace,4);
%     end
     
    
    %% Save images
    save_nii_from_kspace(kspace, output_dir, output_name, params);
    if (save_uncomb)
        save_nii_from_kspace_uncomb(kspace, output_dir, output_name, params);    
    end
    
end


