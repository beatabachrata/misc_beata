clear, clc

nCase = 0;

%%
for lFile= 1:19
    
switch lFile 

    case 1
        IT = 0;
        image_subdir = '17';      
    case 2
        IT = 60;
        image_subdir = '15';        
    case 3
        IT = 120;
        image_subdir = '16';        
    case 4
        IT = 180;
        image_subdir = '18';        
    case 5
        IT = 240;
        image_subdir = '19';        
    case 6
        IT = 300;
        image_subdir = '20';        
    case 7
        IT = 360;
        image_subdir = '21';        
    case 8
        IT = 420;
        image_subdir = '22';        
    case 9
        IT = 570;
        image_subdir = '23';        
    case 10
        IT = 720;
        image_subdir = '24';        
    case 11
        IT = 800;
        image_subdir = '27';        
    case 12
        IT = 870;
        image_subdir = '25';        
    case 13
        IT = 970;
        image_subdir = '26';        
    case 14
        IT = 1070;
        image_subdir = '28';        
    case 15
        IT = 1200;
        image_subdir = '29';        
    case 16
        IT = 1350;
        image_subdir = '30';        
    case 17
        IT = 1500;
        image_subdir = '31';        
    case 18
        IT = 1650;
        image_subdir = '32';        
    case 19
        IT = 1800;
        image_subdir = '33'; 
end

subj_dir = 'p_bb_20181024_201810241630';
output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/p_bb_20181024/T1_mapping/conc_all';
output_filename = sprintf('IT%i.nii',IT);



[s,mess] = mkdir(output_dir);
fprintf('Writing results to %s\n', output_dir);
if s == 0
    ff_measured('No permission to make directory %s/m', output_dir);
end

nCase = nCase + 1;

image_nii = load_nii(fullfile('/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/',subj_dir,'nifti/', image_subdir, 'Image.nii'));
image = double(image_nii.img);
image_cropped = image(end:-1:1,:,:);

image_nii = make_nii(image_cropped);
save_nii(image_nii,fullfile(output_dir,output_filename));


%% create and save ROI as nii
% fslchfiletype NIFTI l10percent.img 10percent.nii

%% load ROI
roi_nii = load_nii(fullfile(output_dir,'roi_conc5.nii'));
roi = double(roi_nii.img);

meanOverROI(nCase) = sum(sum(sum(image_cropped.*roi))) / sum(sum(sum(roi)));
IT_array(nCase) = IT;

end
digits(5);
vpa(IT_array)
vpa(meanOverROI)

T1_mapping.meanOverROI=meanOverROI;
T1_mapping.TETR=IT_array;
save(fullfile(output_dir,'T1_mapping'));


%load(fullfile(output_dir,'signal_evolution.mat'));

figure;
hold on;

