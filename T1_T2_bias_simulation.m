clear, clc;
%% T1 & T2 BIAS SIMULATION
FA_water_deg = [3,5,9,9];
FA_fat_deg = [3,5,9,24];

TR = 25; %ms
T1_water = 1000; %ms % liver
T1_fat = 280; %ms   % liver
% TE = 11.6; % ms
% T2_water = 30; %ms
% T2_fat = 30; %ms

for lFA = 1:4
    FA_water = FA_water_deg(lFA)*3.14159/180;
    FA_fat = FA_fat_deg(lFA)*3.14159/180;
    
    nFR = 1;
    for ff_true = 0:0.05:0.2 
        wf_true = 1-ff_true;

        Sw_weight = ((1-exp(-TR/T1_water))*sin(FA_water))/(1-exp(-TR/T1_water)*cos(FA_water));
        Sf_weight = ((1-exp(-TR/T1_fat))*sin(FA_fat))/(1-exp(-TR/T1_fat)*cos(FA_fat));

%         Sw_weight = ((1-exp(-TR/T1_water))*sin(FA_water))*exp(-TE/T2_water)/(1-exp(-TR/T1_water)*cos(FA_water));
%         Sf_weight = ((1-exp(-TR/T1_fat))*sin(FA_fat))*exp(-TE/T2_fat)/(1-exp(-TR/T1_fat)*cos(FA_fat));

        ff_measured = (ff_true*Sf_weight / (wf_true*Sw_weight + ff_true*Sf_weight));

        ff_true_array(lFA,nFR) = ff_true;
        ff_measured_array(lFA,nFR) = ff_measured;
        error_array(lFA,nFR) = ff_measured-ff_true;
        error_array_rel(lFA,nFR) = (ff_measured-ff_true)/ff_true;
        nFR = nFR + 1;
    end
    FA_array(lFA) = FA_water_deg(lFA);
    lFA = lFA + 1;
end  

figure
hold on
plot(ff_true_array(1,:),100*error_array_rel(1,:),'or');
plot(ff_true_array(2,:),100*error_array_rel(2,:),'og');
plot(ff_true_array(3,:),100*error_array_rel(3,:),'ok');
plot(ff_true_array(4,:),100*error_array_rel(4,:),'ob');



