function [CNR,signal] = calc_CNR_func_bb(image, roi, roi_bkg, roi_noise)

% get the image values within ROI
signal_in_roi = image(find(roi(:,:,:)~=0));
bkg_in_roi = image(find(roi_bkg(:,:,:)~=0));
noise_in_roi = image(find(roi_noise(:,:,:)~=0));
    
signal = mean(signal_in_roi);
bkg = mean(bkg_in_roi);
noise = std(noise_in_roi);

% calculate the CNR
CNR = abs(signal - bkg) / noise;  

end
