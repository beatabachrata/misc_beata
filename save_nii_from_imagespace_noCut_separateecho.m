function save_nii_from_imagespace_noCut_separateecho(ima, dir, name, iEcho, params)

orient = params.orient;
PE_dir = params.PE_dir;
if (orient == 1 && (PE_dir == 2 || PE_dir == 3))
    ima = permute(ima,[2,1,3,4]);
    ima = flip(ima,2);
elseif (orient == 2 && PE_dir == 2)
    ima = flip(flip(ima,2),3);
elseif (orient == 2 && PE_dir == 3)
    ima = permute(ima,[2,1,3,4]);
elseif (orient == 3 && PE_dir == 3)
    ima = permute(ima,[2,1,3,4]);
end
   

ima_SoS_nii = make_nii(squeeze(ima));
save_nii(ima_SoS_nii,fullfile(dir,sprintf('%s_echo%i.nii',name,iEcho)));
    

end