clear all
figure

B0 = 2.89326;
gyro = 42.58;

for TE = 4.6
    
    % FAT
    freq_ppm = [-3.80, -3.40, -2.60];
%     relAmps = [0.087 0.693 0.128]./sum([0.087 0.693 0.128]);
    relAmps = [0.087 0.693 0.128];

    phaseOffset = freq_ppm * gyro * B0 * TE * 2 * pi /1000;
    phaseOffset = mod(phaseOffset,2*pi);  
    fat_acquired = abs(sum(relAmps .* exp(-1i*phaseOffset)));

    % WATER
    freq_ppm = [0 -0.39, 0.60];
%     relAmps = [1 0.039 0.048]/sum([1 0.039 0.048]);
    relAmps = [1 0.039 0.048];

    phaseOffset = freq_ppm * gyro * B0 * TE * 2 * pi /1000;
    phaseOffset = mod(phaseOffset,2*pi);  
    water_acquired = abs(sum(relAmps .* exp(-1i*phaseOffset)));

    j = 1;
    for iFF = 0:0.01:1
        fat = iFF*fat_acquired;
        water = (1-iFF)*water_acquired;
        FF_acquired(j) = fat/(fat + water);
        FF(j) = iFF;
        j = j+1;
    end

    plot(FF,FF_acquired)
    hold on
    
end

plot(FF,FF,':k', 'LineWidth',5)

% 





