function PCG_unwrap_3D_bb(data)

% Implementation of a 3D weighted least-squares phase unwrapping
% based on preconditioned conjugate gradient (PCG) method with a
% preconditioner taken from a laplace unwrapping with discrete consine
% transform (DCT).
%
% Written by: Barbara Dymerska
%
% Based on theory and c++ code from: "Two-Dimensional Phase Unwrapping, Theory, Algorithms,
% and Software" written by Dennis C. Ghiglia and Mark D. Pritt (chapter 5)
%
% input files:
% ph_file - full path to the corresponding phase file
% mask_nii - binary mask or quality mask with range of values [0,1] in nifti format loaded using load_nii
% max_iter - maximum number of iterations allowed, e.g. 5
% epsi_con - convergence parameter, e.g. 0.00001
% N - number of congruence steps, suggested N = 20

mask_nii = load_nii(data.fn_mask);
ph_nii = load_nii(data.fn_phase);
ph_nii = make_nii(single(rescale(ph_nii.img, -pi, +pi)));
% if ~exist(data.fn_phase_copy)
%     fprintf('Saving a copy of the phase\n');
%     centre_and_save_nii(ph_nii, data.fn_phase_copy, ph_nii.hdr.dime.pixdim);
% end

[mx,my,mz,TP] = size(ph_nii.img) ;

if size(mask_nii.img,4)~=TP
    mask_nii.img = repmat(mask_nii.img,[1 1 1 TP]) ;
end

pixdim = ph_nii.hdr.dime.pixdim(2:4) ;


% increasing dimentions for  DCT
phase_pad = padarray(ph_nii.img, [mx/2 my/2 0 0]) ;
% phase_pad = padarray(phase_pad, [0 0 100 0]) ;
phase_pad = padarray(phase_pad, [0 0 mz 0], 'symmetric', 'pre') ;

mask_pad = padarray(mask_nii.img, [mx/2 my/2 0 0]) ;
% mask_pad = padarray(mask_pad, [0 0 100 0]) ;
mask_pad = padarray(mask_pad, [0 0 mz 0], 'symmetric', 'pre') ;

mx_orig = mx ;
my_orig = my ;
mz_orig = mz ;

[mx,my,mz,~] = size(phase_pad) ;

% computing the weighted wrapped phase Laplacian (r0, Eq. 5.71)
u = zeros(size(phase_pad)) ;
v = zeros(size(phase_pad)) ;
w = zeros(size(phase_pad)) ;

for x = 1:mx-1
    u(x,:,:,:) = min(mask_pad(x+1,:,:,:), mask_pad(x,:,:,:)) ;
end

for y = 1:my-1
    v(:,y,:,:) = min(mask_pad(:,y+1,:,:), mask_pad(:,y,:,:)) ;
end

for z = 1:mz-1
    w(:,:,z,:) = min(mask_pad(:,:,z+1,:), mask_pad(:,:,z,:)) ;
end


phase_diff_x = padarray(angle(exp(1i*diff(phase_pad,1,1))),[1 0 0 0], 0 , 'pre') ;
phase_diff_y = padarray(angle(exp(1i*diff(phase_pad,1,2))),[0 1 0 0], 0 , 'pre') ;
phase_diff_z = padarray(angle(exp(1i*diff(phase_pad,1,3))),[0 0 1 0], 0 , 'pre') ;

u_pdiff_x = u.*phase_diff_x ;
v_pdiff_y = v.*phase_diff_y ;
w_pdiff_z = w.*phase_diff_z ;

u_pdiff_x_diff = padarray(diff(u_pdiff_x,1,1),[1 0 0 0], 0 , 'post') ;
v_pdiff_y_diff = padarray(diff(v_pdiff_y,1,2),[0 1 0 0], 0 , 'post') ;
w_pdiff_z_diff = padarray(diff(w_pdiff_z,1,3),[0 0 1 0], 0 , 'post') ;

lap_w_wr = u_pdiff_x_diff + v_pdiff_y_diff + w_pdiff_z_diff ;

ind3d_x = (1:mx)' ;
ind3d_x = repmat(ind3d_x, [1 my mz]) ;

ind3d_y = 1:my ;
ind3d_y = repmat(ind3d_y, [mx 1 mz]) ;

ind3d_z = (1:mz)' ;
ind3d_z = repmat(ind3d_z, [1 mx my]) ;
ind3d_z = shiftdim(ind3d_z,1) ;


%%% main PCG algorithm
ph_pcguw = zeros(size(phase_pad)) ;
for t = 1:TP

    % defining weighted wrapped phase laplacian for one volume (r0)
    lap3d_w_wr = squeeze(lap_w_wr(:,:,:,t)) ;
    
    % initializing the unwrapped phase solution to zero
    ph2d_pcguw = zeros(mx,my,mz) ;
    
    % needed for epsi calculation
    rmse0 = sqrt(mean(vector(lap3d_w_wr.^2))) ;
    
    % initialization of the iteration steps
    j = 0 ;
    epsi = 10*data.epsi_con ;
    
    % pcg iterations
    while (j<data.max_iter && epsi > data.epsi_con)
        
        % solving Pz_k=r_k equation using unweighted discrete cosine transform algorithm
        ph_lapuw = mirt_idctn(mirt_dctn(lap3d_w_wr)./(2*cos(ind3d_x*pi/mx) + 2*cos(ind3d_y*pi/my) + 2*cos(ind3d_z*pi/mz) - 6)) ;
        
        
        % "b" taken from the c++ code
        b = sum(vector(lap3d_w_wr.*ph_lapuw)) ;

        % calculate p
        if j == 0
            p = ph_lapuw ;
        else
            beta =  b/b_old ;
            p = ph_lapuw + beta.*p_old ;
        end
        
        % remove constant bias from p
        p_mean = mean(vector(p)) ;
        p = p - p_mean ;
        
        % calculate Qp
        p_diff_x = padarray(diff(p,1,1),[1 0 0 0], 0 , 'pre') ;
        p_diff_y = padarray(diff(p,1,2),[0 1 0 0], 0 , 'pre') ;
        p_diff_z = padarray(diff(p,1,3),[0 0 1 0], 0 , 'pre') ;
        
        u_pdiff_x = u(:,:,:,t).*p_diff_x ;
        v_pdiff_y = v(:,:,:,t).*p_diff_y ;
        w_pdiff_z = w(:,:,:,t).*p_diff_z ;
        
        u_pdiff_x_diff = padarray(diff(u_pdiff_x,1,1),[1 0 0 0], 0 , 'post') ;
        v_pdiff_y_diff = padarray(diff(v_pdiff_y,1,2),[0 1 0 0], 0 , 'post') ;
        w_pdiff_z_diff = padarray(diff(w_pdiff_z,1,3),[0 0 1 0], 0 , 'post') ;
        
        Qp = u_pdiff_x_diff + v_pdiff_y_diff + w_pdiff_z_diff ;
        
        % "alpha" as understood from c++ code
        alpha = b/sum(vector(p.*Qp)) ;

        ph2d_pcguw = ph2d_pcguw + alpha*p ;
        
        % removing global bias from unwrapped solution
        ph2d_pcguw_mean = mean(vector(ph2d_pcguw)) ;
        ph2d_pcguw =  ph2d_pcguw - ph2d_pcguw_mean ;
        
        % calculating residual of the laplacian of the weighted wrapped phase
        lap3d_w_wr = lap3d_w_wr - alpha*Qp ;
        
        
        ph_lapuw_old = ph_lapuw ;
        p_old = p ;
        b_old = b ;
                
        epsi =  sqrt(mean(vector(lap3d_w_wr.^2)))/rmse0 ;
        j = j + 1 ;
        
    end
    
    ph_pcguw(:,:,:,t) = ph2d_pcguw ;
    disp(['Volume ' num2str(t) ' finished after iteration nr: ' num2str(j) ' with epsi ' num2str(epsi)])
end

ph_pcguw_rd = ph_pcguw(mx_orig/2+1 : mx-mx_orig/2, my_orig/2+1 : my-my_orig/2, mz_orig+1 : mz ,:) ;
    
% centre_and_save_nii(make_nii(ph_pcguw_rd, pixdim), data.fn_phase_unwrapped_lap, data.pixdim) ;
save_nii(make_nii(ph_pcguw_rd), data.fn_phase_unwrapped_lap) ;

switch data.unwraptype
    case 'laplacian_pc_cong'
        
        % congurence operation
        ph_uw_cong = zeros(mx_orig,my_orig,mz_orig,TP,data.N) ;
        d_x = zeros(mx_orig,my_orig,mz_orig,TP,data.N) ;
        d_y = zeros(mx_orig,my_orig,mz_orig,TP,data.N) ;
        d_z = zeros(mx_orig,my_orig,mz_orig,TP,data.N) ;
        
        for k = 1:data.N
            h = 2*pi*k/data.N ;
            ph_uw_cong(:,:,:,:,k) = ph_pcguw_rd(:,:,:,:) + h + angle(exp(1i*(ph_nii.img(:,:,:,:)-ph_pcguw_rd(:,:,:,:)-h))) ;
            
%             centre_and_save_nii(make_nii(ph_uw_cong(:,:,:,:,k), pixdim), data.fn_phase_unwrapped_pcg, data.pixdim)
            save_nii(make_nii(ph_uw_cong(:,:,:,:,k)), data.fn_phase_unwrapped_pcg)
            
            d_x(1:mx_orig-1,:,:,:,k) = abs(diff(ph_uw_cong(:,:,:,:,k),1,1)) ;
            d_y(:,1:my_orig-1,:,:,k) = abs(diff(ph_uw_cong(:,:,:,:,k),1,2)) ;
            
        end
        
        mask_diff_x = zeros(size(mask_nii.img)) ;
        mask_diff_x(2:mx_orig,:,:,:) = diff(mask_nii.img, 1,1) ;
        mask_nii.img(mask_diff_x == 1) = 0 ;
        
        mask_diff_x = zeros(size(mask_nii.img)) ;
        mask_diff_x(1:mx_orig-1,:,:,:) = diff(mask_nii.img, 1,1) ;
        mask_nii.img(mask_diff_x == -1) = 0 ;
        
        mask_diff_y = zeros(size(mask_nii.img)) ;
        mask_diff_y(2:mx_orig,:,:,:) = diff(mask_nii.img, 1,1) ;
        mask_nii.img(mask_diff_y == 1) = 0 ;
        
        mask_diff_y = zeros(size(mask_nii.img)) ;
        mask_diff_y(1:mx_orig-1,:,:,:) = diff(mask_nii.img, 1,1) ;
        mask_nii.img(mask_diff_y == -1) = 0 ;
        
        d_x(mask_nii.img == 0) = 0 ;
        d_y(mask_nii.img == 0) = 0 ;
        
        d_x(d_x<2*pi) = 0 ;
        d_x(d_x>2*pi) = 1 ;
        
        d_y(d_y<2*pi) = 0 ;
        d_y(d_y>2*pi) = 1 ;
        
        D = zeros(mz_orig,TP,data.N, 'single') ;
        
        %centre_and_save_nii(make_nii(ph_uw_final), data.fn_t2star_map, data.pixdim)
        save_nii(make_nii(ph_uw_final), data.fn_t2star_map)

        % %   SR: this is the original but crashes
        %ph_uw_final = zeros(size(ph_nii.img), 'single') ;
        % for t = 1 : TP
        %     for z = 1 : mz_orig
        %         for k = 1:data.N
        %             D(z,t,k) = sum(vector(d_x(:,:,z,t,k))) + sum(vector(d_y(:,:,z,t,k))) ;
        %         end
        %         [Dval, Kmin] =  min(vector(D(z,t,2:data.N))) ;
        %         % ! Assignment has more non-singleton rhs dimensions than non-singleton subscripts
        %         ph_uw_final(:,:,z,t) = ph_uw_cong(:,:,z,t,Kmin) ;
        %     end
        % end
        
end