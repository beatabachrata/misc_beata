%ima_root_dir = '/net/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/'; 
ima_root_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/';

%ima_name = 'Image.nii' ;
%output_name ='log.nii';

output_root_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/'; 

for imaj=5:6  
    switch imaj
         case 1 
           imaj_dir = 'p_bb_20180507_2/nifti/2'; 
           outputj_dir = 'p_bb_20180507_2/analyze_pulse_shape/LS';
         case 2 
           imaj_dir = 'p_bb_20180507_2/nifti/3';
           outputj_dir = 'p_bb_20180507_2/analyze_pulse_shape/SLR';
         case 3 
           imaj_dir = 'p_bb_20180507_2/nifti/4';
           outputj_dir = 'p_bb_20180507_2/analyze_pulse_shape/SINC';
         case 4 
           imaj_dir = 'p_bb_20180507_2/nifti/6';
           outputj_dir = 'p_bb_20180507_2/analyze_pulse_shape/GAUSS';
           output_name ='log10.nii';
           ima_name = 'Image.nii' ;
           
         case 5 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/centerACSlines/'; 
           ima_name = 'fat_unaliased_center_lines.nii';
           outputj_dir = 'p_bb_20180308/bigFOV_logIma';
           output_name ='fat_unaliased_center_lines_log.nii';  
         case 6 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/centerACSlines/'; 
           ima_name = 'water_unaliased_center_lines.nii';
           outputj_dir = 'p_bb_20180308/bigFOV_logIma';
           output_name ='water_unaliased_center_lines_log.nii'; 
         case 7 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/'; 
           ima_name = 'fat_unaliased_2xF.nii';
           outputj_dir = 'p_bb_201800308/caipirinha_optimization/bigFOV_logIma';
           output_name ='fat_unaliased_2xF_log.nii';
         case 8 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/';
           ima_name = 'fat_unaliased_2xWandF.nii';
           outputj_dir = 'p_bb_201800308/caipirinha_optimization/bigFOV_logIma';
           output_name ='fat_unaliased_2xWandF_log.nii';
         case 9 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/';
           ima_name = 'fat_unaliased_2xW.nii';
           outputj_dir = 'p_bb_201800308/caipirinha_optimization/bigFOV_logIma';
           output_name ='fat_unaliased_2xW_log.nii';
         case 10 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/';
           ima_name = 'fat_unaliased.nii';
           outputj_dir = 'p_bb_201800308/caipirinha_optimization/bigFOV_logIma';
           output_name ='fat_unaliased_log.nii';
         case 11 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/'; 
           ima_name = 'water_unaliased_2xF.nii';
           outputj_dir = 'p_bb_20180308/caipirinha_optimization/bigFOV_logIma';
           output_name ='water_unaliased_2xF_log.nii';
         case 12
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/';
           ima_name = 'water_unaliased_2xWandF.nii';
           outputj_dir = 'p_bb_20180308/caipirinha_optimization/bigFOV_logIma';
           output_name ='water_unaliased_2xWandF_log.nii';
         case 13 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/';
           ima_name = 'water_unaliased_2xW.nii';
           outputj_dir = 'p_bb_20180308/caipirinha_optimization/bigFOV_logIma';
           output_name ='water_unaliased_2xW_log.nii';
         case 14 
           imaj_dir = 'p_bb_20180308/bigFOV_iter2/';
           ima_name = 'water_unaliased.nii';
           outputj_dir = 'p_bb_20180308/caipirinha_optimization/bigFOV_logIma'; 
           output_name ='water_unaliased_log.nii';
         
          case 15 
           imaj_dir = '19701007SMRB_20180511/caipirinha_smallFOV/chemical_shift_correction/';
           ima_name = 'fat_unaliased_cropped.nii';
           outputj_dir = '19701007SMRB_20180511/caipirinha_smallFOV/chemical_shift_correction/'; 
           output_name ='fat_unaliased_cropped_log.nii';  
    end
 
output_dir = fullfile(output_root_dir,outputj_dir);
[s,mess] = mkdir(output_dir);
fprintf('Writing results to %s\n', output_dir);
if s == 0
    error('No permission to make directory %s/m', output_dir);
end
    
ima_file = fullfile(ima_root_dir,imaj_dir,ima_name) ;
ima_nii = load_nii(ima_file) ;

log_img = log(double(ima_nii.img));

log_nii = make_nii(log_img, ima_nii.hdr.dime.pixdim(2:4)) ;
log_file = fullfile(output_dir, output_name) ;
save_nii(log_nii, log_file) ;


% double_log_img = log(log_img);
% 
% double_log_nii = make_nii(double_log_img, ima_nii.hdr.dime.pixdim(2:4)) ;
% double_log_file = fullfile(output_dir, 'double_log.nii') ;
% save_nii(double_log_nii, double_log_file) ;


end