function SNR_array = calc_iSNR_func_bb(readfile_image, readfile_noise, roi_filename_background, roi_filename_image, cf_background, corr);

%needs to loop over images, calculating iSNR in each ROI

no_rois=size(roi_filename_image,2);

%load the image - the first in the series if there are more than one in the
%directory

% flist = dir(fullfile(readfile_dir,'*.img'));
% readfile_image=sprintf('%s%s',readfile_dir,flist(1).name);
image_nii = load_nii(readfile_image);
noise_nii = load_nii(readfile_noise);

ndims = image_nii.hdr.dime.dim(1);

switch ndims
    case 3
        %image = image_nii.img(:,:,round(image_nii.hdr.dime.dim(4)/2)); 
        image = image_nii.img(:,:,:); %%BB changed
        noise = noise_nii.img(:,:,:); %%BB changed
    case 4
        image = image_nii.img(:,:,round(image_nii.hdr.dime.dim(4)/2),1);
        noise = noise_nii.img(:,:,round(image_nii.hdr.dime.dim(4)/2),1);
end

% switch cf_background
%     case 'yes'
%         %read in background ROI
%         try
%             bg_ROI_nii = load_nii(roi_filename_background);
%             bg_ROI = bg_ROI_nii.img;
%         catch
%             error(sprintf('!Error!: Did not find %s \nSet "draw_rois" to "yes" in SNR_calc_main.m ', roi_filename_background));
%         end
%         %and get values out of image
%         image_in_bgROI=image(find(bg_ROI~=0));
%         noise_in_bgROI=noise(find(bg_ROI~=0));
% 
%     case 'no'
% end

%now read in the image ROIs and calculate the SNR

SNR_array = zeros(length(no_rois));

for i=1:no_rois
    try
        one_ROI_nii = load_nii(char(roi_filename_image(i)));
        all_ROIs(:,:,:,i) = one_ROI_nii.img;
    catch
        error(sprintf('!Error!: Did not find %s \nSet "draw_rois" to "yes" in SNR_calc_main.m ', char(roi_filename_image(i))));
    end

    image_in_iROI = image(find(all_ROIs(:,:,:,i)~=0));
    noise_in_iROI = noise(find(all_ROIs(:,:,:,i)~=0));

    switch cf_background
        case 'yes'
            if (corr == 1)
                SNR = mean(image_in_iROI)/std(noise_in_iROI,1);
            else
                SNR = mean(image_in_iROI)/std(noise_in_iROI,1);
            end
        case 'no'
            SNR = mean(image_in_iROI);
    end
    
    SNR_array(i)=SNR;
    
end
