% Images displayed in MRIcro are flipped compared to IceSimulation display, 
% as they have differently oriented axis (x-axis has the same orientation,
% but y-axis has opposite orientation). 
% In MRIcro, channels are increasing from bottom to top.



%% user definable part
col = 128; % are col and lin here the same in the name of phaseOffset or exchanged
lin = 128;
%chan = 32;
slices = 30;

data_dir='/data/bbachrata/virtualShare/vrcPhaseOffsets';


%% no need for user modifications

datatype ='float32'; %ICE_FORMAT_CXFL

realAndIma = zeros(2*lin, col, length(slices));
real = zeros(lin, col, length(slices));
imaginary = zeros(lin, col, length(slices));
magnitude = zeros(lin, col, length(slices));
phase = zeros(lin, col, length(slices));

for i = 1:slices
   %storeInd = storeInd + 1; 
   name=fullfile(data_dir,sprintf('poSaved_0_col128_lin128_chan32_slc%i.ima',i));
    
   realAndIma(:,:,i) = fread(fopen(name), [lin*2 , col], datatype);
   real(:,:,i) = realAndIma(1:2:end,:,i);
   imaginary(:,:,i) = realAndIma(2:2:end,:,i);
   magnitude(:,:,i) = sqrt(real(:,:,i).*real(:,:,i) + imaginary(:,:,i).*imaginary(:,:,i));
   phase(:,:,i) = atan2(imaginary(:,:,i), real(:,:,i));
end

save_nii(make_nii(magnitude), fullfile(data_dir,'poSaved_getCorrSliceNumb_mag.nii')); 
save_nii(make_nii(phase), fullfile(data_dir,'poSaved_getCorrSliceNumb_phase.nii'))
%save_nii(make_nii(real), fullfile(data_dir,'vrcPhaseOffset_real.nii')); 
%save_nii(make_nii(imaginary), fullfile(data_dir,'vrcPhaseOffset_ima.nii')); 


