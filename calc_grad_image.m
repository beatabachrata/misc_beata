clc; clearvars; close all;

root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre';
sub_dir = 'p_bb_20200107_waterExcForReview';

for file = 2
    
switch file    
    case 1
        input_name = 'SMURF_DB.nii'; 
    case    2
        input_name = 'waterExc.nii';      
end
    
output_name = sprintf('grad_%s',input_name);
ima_nii = load_nii(fullfile(root_dir,sub_dir, input_name));
ima = double(ima_nii.img);

% Calculate & Save Image Gradient
[~,Gx,~] = gradient(ima/2/pi) ; % in gradient function x is swaped with y, i.e. the gradient in the PE-direction is in x-direction
Gx_nii = make_nii(Gx) ;
save_nii(Gx_nii, fullfile(root_dir,sub_dir, output_name));


end
