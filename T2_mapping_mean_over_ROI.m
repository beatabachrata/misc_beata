clear, clc

nCase = 0;
%%
% for lTETR = 1:nTETR
for lFile = 26
    
switch lFile
    case 1
        TEs = 3:4:39;
        image_subdir = '2';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 2
        TEs = 3.2:4:39.2;
        image_subdir = '3';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 3
        TEs = 3.4:4:39.4;
        image_subdir = '4';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 4
        TEs = 3.6:4:39.6;
        image_subdir = '5';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 5
        TEs = 3.8:4:39.8;
        image_subdir = '6';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 6
        TEs = 4:4:40;
        image_subdir = '7';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 7
        TEs = 4.2:4:40.2;
        image_subdir = '8';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 8
        TEs = 4.4:4:40.4;
        image_subdir = '9';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 9
        TEs = 4.6:4:40.6;
        image_subdir = '10';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 10
        TEs = 4.8:4:40.8;
        image_subdir = '11';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 11
        TEs = 5:4:41;
        image_subdir = '12';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 12
        TEs = 5.2:4:41.2;
        image_subdir = '13';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 13
        TEs = 5.4:4:41.4;
        image_subdir = '14';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 14
        TEs = 5.6:4:41.6;
        image_subdir = '15';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 15
        TEs = 5.8:4:41.8;
        image_subdir = '16';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 16
        TEs = 6:4:42;
        image_subdir = '17';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 17
        TEs = 6.2:4:42.2;
        image_subdir = '18';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 18
        TEs = 6.4:4:42.4;
        image_subdir = '19';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 19
        TEs = 6.6:4:42.6;
        image_subdir = '20';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
    case 20
        TEs = 6.8:4:42.8;
        image_subdir = '21';
        subj_dir = 'p_bb_20181030_';
        output_subdir = 'p_bb_20181030_1/T2_mapping/conc20'; 
 
    case 21
        TEs = 4.65:4.65:55.8;
        image_subdir = '12';
        subj_dir = 'p_bb_20181030_conc5';
        output_subdir = 'p_bb_20181030/T2_mapping/conc5'; 
    case 22
        TEs = 4.65:4.65:55.8;
        image_subdir = '14';
        subj_dir = 'p_bb_20181030_conc10';
        output_subdir = 'p_bb_20181030/T2_mapping/conc10'; 
    case 23
        TEs = 4.65:4.65:55.8;
        image_subdir = '31';
        subj_dir = 'p_bb_20181030_conc20';
        output_subdir = 'p_bb_20181030/T2_mapping/conc20'; 

    case 24
        TEs = 12.8:0.2:16.0;
        image_subdir = {'26','24','22','20','18','16','2','6','8','10','12','14','28','30','32','34','36'};
        subj_dir = 'p_bb_20190410_conc30';
        output_subdir = 'p_bb_20190410/WFphaseRel/0Pi0Pi'; 

    case 25
        TEs = 12.8:0.2:16.0;
        image_subdir = {'27','25','23','21','19','17','3','7','9','11','13','15','29','31','33','35','37'};
        subj_dir = 'p_bb_20190410_conc30';
        output_subdir = 'p_bb_20190410/WFphaseRel/1Pi1Pi'; 
        
    case 26
        TEs = 12.3:0.2:16.7;
        image_subdir = {'3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25'};
        subj_dir = 'p_bb_20190411';
        output_subdir = 'p_bb_20190411/WFphaseRel/0Pi0Pi';     

end
output_dir = fullfile('/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/',output_subdir);

[s,mess] = mkdir(output_dir);
fprintf('Writing results to %s\n', output_dir);
if s == 0
    ff_measured('No permission to make directory %s/m', output_dir);
end

nCase = nCase + 1;

%nSlc = size(info.ONLINE.mdhInfo{1,1},4);


%image_nii = load_nii(fullfile('/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/',subj_dir,'nifti/', image_subdir, 'reform/Image.nii'));

for nTE = 1:size(TEs,2)
    image_nii = load_nii(fullfile('/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/',subj_dir,'nifti/', image_subdir{nTE}, 'Image.nii'));
    %image = double(image_nii.img(:,:,:,nTE));
    image = double(image_nii.img);
    image_cropped = image(end:-1:1,:,:);
    echo_nii = make_nii(image_cropped);
    save_nii(echo_nii,fullfile(output_dir,sprintf('TE%.*f.nii',2,TEs(nTE))));

    %% create and save ROI as nii
    % fslchfiletype NIFTI lroi.img roi.nii

    %% load ROI
    roi_nii = load_nii(fullfile(output_dir,'roi.nii'));
    roi = double(roi_nii.img);
    meanOverROI(nCase,nTE) = sum(sum(sum(image_cropped.*roi))) / sum(sum(sum(roi)));
    TETR_array(nCase,nTE) = TEs(nTE);
    
end
end

digits(5);
vpa(TETR_array)
vpa(meanOverROI)

T2_mapping.meanOverROI=meanOverROI;
T2_mapping.TETR=TETR_array;
save(fullfile(output_dir,'T2_mapping'));


figure;
hold on;
for lCase = 1:nCase
      plot(TETR_array(lCase,:),meanOverROI(lCase,:),'or')
end
%legend('conc5','conc10','conc20')

