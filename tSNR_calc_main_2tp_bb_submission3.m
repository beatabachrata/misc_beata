% SNR_calc_main.m
% Simon Robinson. 31.10.2006
% Calculating Image SNR on the basis of ROIs, which can be defined and saved here
% and/or Time-Series SNR, which is done on a pixel-by-pixel basis
% Calculation done in calc_tSNR_func

clear, clc
warning off MATLAB:divideByZero


main_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19790301PDCR_20200123_abdomen/';

slices = 2:6;

for iSlc = 1:length(slices)
    slc = slices(iSlc);
    
    for variant = 1:24
    switch variant
            case 1
                data_subdir = {'caipirinha/scan1/',
                               'caipirinha/scan2/'};
                write_subdir = 'caipirinha/scans12/';
                image_name = 'water.nii';
            case 2
                data_subdir = {'caipirinha/scan3/',
                               'caipirinha/scan4/'};
                write_subdir = 'caipirinha/scans34/';
                image_name = 'water.nii';
            case 3
                data_subdir = {'caipirinha/scan5/',
                               'caipirinha/scan6/'};
                write_subdir = 'caipirinha/scans56/';
                image_name = 'water.nii';
            case 4
                data_subdir = {'caipirinha/scan7/',
                               'caipirinha/scan8/'};
                write_subdir = 'caipirinha/scans78/';
                image_name = 'water.nii';

            case 5
                data_subdir = {'dixon_2D/longTR/scan1/',
                               'dixon_2D/longTR/scan2/'};
                write_subdir = 'dixon_2D/longTR/scan12/';
                image_name = sprintf('water_Hernando_slc%i.nii',slc);
            case 6
                data_subdir = {'dixon_2D/longTR/scan3/',
                               'dixon_2D/longTR/scan4/'};
                write_subdir = 'dixon_2D/longTR/scans34/';                
                image_name = sprintf('water_Hernando_slc%i.nii',slc);
            case 7
                data_subdir = {'dixon_2D/longTR/scan5/',
                               'dixon_2D/longTR/scan6/'};
                write_subdir = 'dixon_2D/longTR/scans56/';
                image_name = sprintf('water_Hernando_slc%i.nii',slc);
            case 8
                data_subdir = {'dixon_2D/longTR/scan7/',
                               'dixon_2D/longTR/scan8/'};
                write_subdir = 'dixon_2D/longTR/scans78/';
                image_name = sprintf('water_Hernando_slc%i.nii',slc);

            case 9
                data_subdir = {'dixon_2D/shortTR/scan1/',
                               'dixon_2D/shortTR/scan2/'};
                write_subdir = 'dixon_2D/shortTR/scan12/';
                image_name = sprintf('water_Hernando_slc%i.nii',slc);
            case 10
                data_subdir = {'dixon_2D/shortTR/scan3/',
                               'dixon_2D/shortTR/scan4/'};
                write_subdir = 'dixon_2D/shortTR/scans34/';                
                image_name = sprintf('water_Hernando_slc%i.nii',slc);
            case 11
                data_subdir = {'dixon_2D/shortTR/scan5/',
                               'dixon_2D/shortTR/scan6/'};
                write_subdir = 'dixon_2D/shortTR/scans56/';
                image_name = sprintf('water_Hernando_slc%i.nii',slc);
            case 12
                data_subdir = {'dixon_2D/shortTR/scan7/',
                               'dixon_2D/shortTR/scan8/'};
                write_subdir = 'dixon_2D/shortTR/scans78/';
                image_name = sprintf('water_Hernando_slc%i.nii',slc);


            case 13
                data_subdir = {'caipirinha/scan1/',
                               'caipirinha/scan2/'};
                write_subdir = 'caipirinha/scans12/';
                image_name = 'fat_chemShiftCorr.nii';
            case 14
                data_subdir = {'caipirinha/scan3/',
                               'caipirinha/scan4/'};
                write_subdir = 'caipirinha/scans34/';
                image_name = 'fat_chemShiftCorr.nii';
            case 15
                data_subdir = {'caipirinha/scan5/',
                               'caipirinha/scan6/'};
                write_subdir = 'caipirinha/scans56/';
                image_name = 'fat_chemShiftCorr.nii';
            case 16
                data_subdir = {'caipirinha/scan7/',
                               'caipirinha/scan8/'};
                write_subdir = 'caipirinha/scans78/';
                image_name = 'fat_chemShiftCorr.nii';

            case 17
                data_subdir = {'dixon_2D/longTR/scan1/',
                               'dixon_2D/longTR/scan2/'};
                write_subdir = 'dixon_2D/longTR/scans12/';
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);
            case 18
                data_subdir = {'dixon_2D/longTR/scan3/',
                               'dixon_2D/longTR/scan4/'};
                write_subdir = 'dixon_2D/longTR/scans34/';                
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);
            case 19
                data_subdir = {'dixon_2D/longTR/scan5/',
                               'dixon_2D/longTR/scan6/'};
                write_subdir = 'dixon_2D/longTR/scans56/';
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);
            case 20
                data_subdir = {'dixon_2D/longTR/scan7/',
                               'dixon_2D/longTR/scan8/'};
                write_subdir = 'dixon_2D/longTR/scans78/';
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);

            case 21
                data_subdir = {'dixon_2D/shortTR/scan1/',
                               'dixon_2D/shortTR/scan2/'};
                write_subdir = 'dixon_2D/shortTR/scans12/';
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);
            case 22
                data_subdir = {'dixon_2D/shortTR/scan3/',
                               'dixon_2D/shortTR/scan4/'};
                write_subdir = 'dixon_2D/shortTR/scans34/';                
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);
            case 23
                data_subdir = {'dixon_2D/shortTR/scan5/',
                               'dixon_2D/shortTR/scan6/'};
                write_subdir = 'dixon_2D/shortTR/scans56/';
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);
            case 24
                data_subdir = {'dixon_2D/shortTR/scan7/',
                               'dixon_2D/shortTR/scan8/'};
                write_subdir = 'dixon_2D/shortTR/scans78/';
                image_name = sprintf('fat_Hernando_slc%i.nii',slc);

    end

        if (variant <= 12) 
             roi_names = {sprintf('water_roi1_slc%i.nii',slc),sprintf('water_roi2_slc%i.nii',slc),sprintf('water_roi3_slc%i.nii',slc),sprintf('water_roi4_slc%i.nii',slc)};
        else
             roi_names = {sprintf('fat_roi1_slc%i.nii',slc),sprintf('fat_roi2_slc%i.nii',slc),sprintf('fat_roi3_slc%i.nii',slc),sprintf('fat_roi4_slc%i.nii',slc)};
        end
        
        %%
        write_dir = fullfile(main_dir,write_subdir);

        %% make results directory
        s = warning ('query', 'MATLAB:MKDIR:DirectoryExists') ;	    % get current state
        warning ('off', 'MATLAB:MKDIR:DirectoryExists') ;
        mkdir(write_dir);
        warning(s) ;						    % restore state


        %%
        clear image image_4D inROI_mean inROI_diff

        for scan = 1:length(data_subdir)
            readfile = fullfile(main_dir, data_subdir{scan}, image_name);
            image_nii = load_nii(readfile);
            image = image_nii.img;
            if (size(image,3) ~= 1)
                image_4D(:,:,:,scan) = image(:,:,slc);
            else
                image_4D(:,:,:,scan) = image;
            end
        end


        %calculate mean and difference image over the 2 acquisitions
        mean_ima = sum(image_4D, 4)./2;
        diff_ima = squeeze(image_4D(:,:,:,1) - image_4D(:,:,:,2));

%         save_nii(make_nii(mean_ima),fullfile(write_dir,sprintf('mean_slc%i_%s',slc,image_name)))
%         save_nii(make_nii(diff_ima),fullfile(write_dir,sprintf('diff_slc%i_%s',slc,image_name)))


        for lroi = 1:length(roi_names)
            roifile = fullfile(main_dir,'rois/',roi_names{lroi});
            roi_nii = load_nii(roifile);
            roi = roi_nii.img;

            j = 1;
            for i = 1:length(roi(:))
                if (roi(i) ~= 0)
                    inROI_mean(j) = mean_ima(i);
                    inROI_diff(j) = diff_ima(i);
                    j = j+1;
                end
            end
            
            signal(variant,lroi) = mean(inROI_mean);
            noise(variant,lroi) = std(inROI_diff)/sqrt(2);
            ROIsize(lroi) = j;
        end
    diff_ima_all(:,:,variant) = diff_ima;

    end
    
SNR((length(signal)*(iSlc-1)+1):(length(signal)*iSlc),:) = signal./noise;
ROIsize_all(:,iSlc) = ROIsize;

% diff_ima_SMURF_water = mean(abs(diff_ima_all(:,:,1:4)),3)*6*10^8;
% diff_ima_longTR_water = mean(abs(diff_ima_all(:,:,5:8)),3);
% diff_ima_shortTR_water = mean(abs(diff_ima_all(:,:,9:12)),3);
% diff_ima_SMURF_fat = mean(abs(diff_ima_all(:,:,13:16)),3)*6*10^8;
% diff_ima_longTR_fat = mean(abs(diff_ima_all(:,:,17:20)),3);
% diff_ima_shortTR_fat = mean(abs(diff_ima_all(:,:,21:24)),3);
% 
% save_nii(make_nii(diff_ima_SMURF_water),fullfile(main_dir,sprintf('meanDiff_SMURF_water_slc%i.nii',slc)))
% save_nii(make_nii(diff_ima_longTR_water),fullfile(main_dir,sprintf('meanDiff_Dixon_longTR_water_slc%i.nii',slc)))
% save_nii(make_nii(diff_ima_shortTR_water),fullfile(main_dir,sprintf('meanDiff_Dixon_shortTR_water_slc%i.nii',slc)))
% save_nii(make_nii(diff_ima_SMURF_fat),fullfile(main_dir,sprintf('meanDiff_SMURF_fat_slc%i.nii',slc)))
% save_nii(make_nii(diff_ima_longTR_fat),fullfile(main_dir,sprintf('meanDiff_Dixon_longTR_fat_slc%i.nii',slc)))
% save_nii(make_nii(diff_ima_shortTR_fat),fullfile(main_dir,sprintf('meanDiff_Dixon_shortTR_fat_slc%i.nii',slc)))
% 
% diff_ima_water = (diff_ima_SMURF_water+diff_ima_longTR_water+diff_ima_shortTR_water)/3;
% diff_ima_fat = (diff_ima_SMURF_fat+diff_ima_longTR_fat+diff_ima_shortTR_fat)/3;
% 
% save_nii(make_nii(diff_ima_water),fullfile(main_dir,sprintf('meanDiff_all_water_slc%i.nii',slc)))
% save_nii(make_nii(diff_ima_fat),fullfile(main_dir,sprintf('meanDiff_all_fat_slc%i.nii',slc)))

end


%dlmwrite(sprintf(fullfile(main_dir,sprintf('SNR_table_fromMatlab_20200203'))),SNR,'\t')

%% Calculate SNR and SNR efficiency
% SNR of water
order_array = [1:4,25:28,49:52,73:76,97:100];
median_SNR_SMURF_water = median(SNR(order_array,:),1);
median_SNR_longTR_water = median(SNR(order_array+4,:),1);
median_SNR_shortTR_water = median(SNR(order_array+8,:),1);
median_SNR_water = [median_SNR_SMURF_water.',median_SNR_longTR_water.',median_SNR_shortTR_water.'];
MAD_SNR_SMURF_water = mad(SNR(order_array,:),0,1);
MAD_SNR_longTR_water = mad(SNR(order_array+4,:),0,1);
MAD_SNR_shortTR_water = mad(SNR(order_array+8,:),0,1);
MAD_SNR_water = [MAD_SNR_SMURF_water.',MAD_SNR_longTR_water.',MAD_SNR_shortTR_water.'];

median_SNR_SMURF_fat = median(SNR(order_array+12,:),1);
median_SNR_longTR_fat = median(SNR(order_array+16,:),1);
median_SNR_shortTR_fat = median(SNR(order_array+20,:),1);
median_SNR_fat = [median_SNR_SMURF_fat.',median_SNR_longTR_fat.',median_SNR_shortTR_fat.'];
MAD_SNR_SMURF_fat = mad(SNR(order_array+12,:),0,1);
MAD_SNR_longTR_fat = mad(SNR(order_array+16,:),0,1);
MAD_SNR_shortTR_fat = mad(SNR(order_array+20,:),0,1);
MAD_SNR_fat = [MAD_SNR_SMURF_fat.',MAD_SNR_longTR_fat.',MAD_SNR_shortTR_fat.'];

% SNR efficiency in water
median_SNR_eff_SMURF_water = median(SNR(order_array,:)/sqrt(34.6),1);
median_SNR_eff_longTR_water = median(SNR(order_array+4,:)/sqrt(34.6),1);
median_SNR_eff_shortTR_water = median(SNR(order_array+8,:)/sqrt(15.56),1);
median_SNR_eff_water = [median_SNR_eff_SMURF_water.',median_SNR_eff_longTR_water.',median_SNR_eff_shortTR_water.'];
MAD_SNR_eff_SMURF_water = mad(SNR(order_array,:)/sqrt(34.6),0,1);
MAD_SNR_eff_longTR_water = mad(SNR(order_array+4,:)/sqrt(34.6),0,1);
MAD_SNR_eff_shortTR_water = mad(SNR(order_array+8,:)/sqrt(15.56),0,1);
MAD_SNR_eff_water = [MAD_SNR_eff_SMURF_water.',MAD_SNR_eff_longTR_water.',MAD_SNR_eff_shortTR_water.'];


% SNR efficiency in fat
median_SNR_eff_SMURF_fat = median(SNR(order_array+12,:)/sqrt(34.6),1);
median_SNR_eff_longTR_fat = median(SNR(order_array+16,:)/sqrt(34.6),1);
median_SNR_eff_shortTR_fat = median(SNR(order_array+20,:)/sqrt(15.56),1);
median_SNR_eff_fat = [median_SNR_eff_SMURF_fat.',median_SNR_eff_longTR_fat.',median_SNR_eff_shortTR_fat.'];
MAD_SNR_eff_SMURF_fat = mad(SNR(order_array+12,:)/sqrt(34.6),0,1);
MAD_SNR_eff_longTR_fat = mad(SNR(order_array+16,:)/sqrt(34.6),0,1);
MAD_SNR_eff_shortTR_fat = mad(SNR(order_array+20,:)/sqrt(15.56),0,1);
MAD_SNR_eff_fat = [MAD_SNR_eff_SMURF_fat.',MAD_SNR_eff_longTR_fat.',MAD_SNR_eff_shortTR_fat.'];


%% Plot
names_water = {'ROIs 1-5','ROIs 6-10','ROIs 11-15','ROIs 16-20'};
names_fat = {'ROIs 21-25','ROIs 26-30','ROIs 31-35','ROIs 36-40'};

figure;
s11 = subplot(2,2,1);
b11 = bar(median_SNR_water);
ylabel('a.u.','FontSize',12);
s11.Position = s11.Position + [-0.09 0.04 0.0 0.0];
set(gca,'XTickLabel',names_water,'FontSize',12);
title('SNR of ROIs in water tissues','FontSize',14)
ay11 = gca;
ay11.YTick = 0:20:120;
set(ay11);
xBar=cell2mat(get(b11,'XData')).' + [b11.XOffset];  % compute bar centers
hold on
errorbar(xBar,median_SNR_water,MAD_SNR_water,'k.');
legend('SMURF','long-TR Dixon','short-TR Dixon')


s12 = subplot(2,2,2);
b12 = bar(median_SNR_fat);
ylabel('a.u.','FontSize',12);
s12.Position = s12.Position + [-0.14 0.04 0.0 0.0];
set(gca,'XTickLabel',names_fat,'FontSize',12);
title('SNR of ROIs in fatty tissues','FontSize',14);
ay12 = gca;
ay12.YTick = 0:30:180;
set(ay12);
xBar12=cell2mat(get(b12,'XData')).' + [b12.XOffset];  % compute bar centers
hold on
errorbar(xBar12,median_SNR_fat,MAD_SNR_fat,'k.');
legend('SMURF','long-TR Dixon','short-TR Dixon')

s21 = subplot(2,2,3);
b21 = bar(median_SNR_eff_water);
ylabel('a.u.','FontSize',12);
ylim([0,21]);
s21.Position = s21.Position + [-0.09 0.06 0.0 0.0];
set(gca,'XTickLabel',names_water,'FontSize',12)
title('SNR efficiency of ROIs in water tissues','FontSize',14)
ay21 = gca;
ay21.YTick = 0:3:21;
set(ay21)
xBar21=cell2mat(get(b21,'XData')).' + [b21.XOffset];  % compute bar centers
hold on
errorbar(xBar21,median_SNR_eff_water,MAD_SNR_eff_water,'k.');
legend('SMURF','long-TR Dixon','short-TR Dixon')

s22 = subplot(2,2,4);
b22 = bar(median_SNR_eff_fat);
ylabel('a.u.','FontSize',12);
s22.Position = s22.Position + [-0.14 0.06 0.0 0.0];
set(gca,'XTickLabel',names_fat,'FontSize',12)
title('SNR efficiency of ROIs in fatty tissues','FontSize',14)
ay22 = gca;
ay22.YTick = 0:5:30;
set(ay22)
xBar22=cell2mat(get(b22,'XData')).' + [b22.XOffset];  % compute bar centers
hold on
errorbar(xBar22,median_SNR_eff_fat,MAD_SNR_eff_fat,'k.');
legend('SMURF','long-TR Dixon','short-TR Dixon')


b22(1).FaceColor = [136/255,204/255,255/255];
b22(2).FaceColor = [255/255,205/255,151/255];
b22(3).FaceColor = [255/255,153/255,151/255];
b11(1).FaceColor = b22(1).FaceColor;
b11(2).FaceColor = b22(2).FaceColor;
b11(3).FaceColor = b22(3).FaceColor;
b21(1).FaceColor = b22(1).FaceColor;
b21(2).FaceColor = b22(2).FaceColor;
b21(3).FaceColor = b22(3).FaceColor;
b12(1).FaceColor = b22(1).FaceColor;
b12(2).FaceColor = b22(2).FaceColor;
b12(3).FaceColor = b22(3).FaceColor;


% %% Calculate SNR and SNR efficiency
% % SNR of water
% SNR_SMURF_water = SNR(1:4,:,:);
% SNR_longTR_water = SNR(5:8,:,:);
% SNR_shortTR_water = SNR(9:12,:,:);
% SNR_water = [SNR_SMURF_water(:), SNR_longTR_water(:), SNR_shortTR_water(:)];
% 
% % SNR of water
% SNR_SMURF_fat = SNR(13:16,:,:);
% SNR_longTR_fat = SNR(17:20,:,:);
% SNR_shortTR_fat = SNR(21:24,:,:);
% SNR_fat = [SNR_SMURF_fat(:), SNR_longTR_fat(:), SNR_shortTR_fat(:)];
% 
% % SNR efficiency in water
% SNR_eff_SMURF_water = SNR_SMURF_water/sqrt(34.6);
% SNR_eff_longTR_water = SNR_longTR_water/sqrt(34.6);
% SNR_eff_shortTR_water = SNR_shortTR_water/sqrt(15.56);
% SNR_eff_water = [SNR_eff_SMURF_water(:), SNR_eff_longTR_water(:), SNR_eff_shortTR_water(:)];
% 
% % SNR efficiency in fat
% SNR_eff_SMURF_fat = SNR_SMURF_fat/sqrt(34.6);
% SNR_eff_longTR_fat = SNR_longTR_fat/sqrt(34.6);
% SNR_eff_shortTR_fat = SNR_shortTR_fat/sqrt(15.56);
% SNR_eff_fat = [SNR_eff_SMURF_fat(:), SNR_eff_longTR_fat(:), SNR_eff_shortTR_fat(:)];


% %% Box plot
% names = {'SMURF','longTR Dixon','shortTR Dixon'};
% 
% figure;
% s11 = subplot(2,2,1);
% box11 = boxplot(SNR_water,names,'Widths',0.5,'Colors','k','Notch','on');
% ylabel('a.u.','FontSize',12);
% s11.Position = s11.Position + [-0.09 0.04 -0.08 0.0];
% set(gca,'XTick',1:3)%2 because only exists 2 boxplot
% set(gca,'XTickLabel',names,'FontSize',12);
% title('SNR over ROIs in water tissue','FontSize',14)
% ay11 = gca;
% ay11.YTick = 25:20:125;
% set(ay11);
% 
% s12 = subplot(2,2,2);
% box12 = boxplot(SNR_fat,names,'Widths',0.5, 'Colors','k','Notch','on');
% ylabel('a.u.','FontSize',12);
% s12.Position = s12.Position + [-0.22 0.04 -0.08 0.0];
% set(gca,'XTick',1:3)%2 because only exists 2 boxplot
% set(gca,'XTickLabel',names,'FontSize',12);
% title('SNR over ROIs in fatty tissue','FontSize',14);
% ay12 = gca;
% ay12.YTick = 30:30:180;
% set(ay12);
% 
% s21 = subplot(2,2,3);
% box21 = boxplot(SNR_eff_water,names,'Widths',0.5, 'Colors','k','Notch','on');
% ylabel('a.u.','FontSize',12);
% s21.Position = s21.Position + [-0.09 0.1 -0.08 0.0];
% set(gca,'XTick',1:3)%2 because only exists 2 boxplot
% set(gca,'XTickLabel',names,'FontSize',12)
% title('SNR efficiency over ROIs in water tissue','FontSize',14)
% ay21 = gca;
% ay21.YTick = 4:3:23;
% set(ay21)
% 
% s22 = subplot(2,2,4);
% box22 = boxplot(SNR_eff_fat,names,'Widths',0.5, 'Colors','k','Notch','on');
% ylabel('a.u.','FontSize',12);
% s22.Position = s22.Position + [-0.22 0.1 -0.08 0.0];
% set(gca,'XTick',1:3)%2 because only exists 2 boxplot
% set(gca,'XTickLabel',names,'FontSize',12)
% title('SNR efficiency over ROIs in fatty tissue','FontSize',14)
% ay22 = gca;
% ay22.YTick = 5:5:40;
% set(ay22)
% 
% lines = findobj(gcf, 'type', 'line', 'Tag', 'Median');
% set(lines, 'Color', 'r');
% 

% %% Calculate medians and median absolute deviations
% % SNR in water
% median_SMURF_water = median(SNR_SMURF_water(:));
% median_longTR_water = median(SNR_longTR_water(:));
% median_shortTR_water = median(SNR_shortTR_water(:));
% mad_SMURF_water = mad(SNR_SMURF_water(:));
% mad_longTR_water = mad(SNR_longTR_water(:));
% mad_shortTR_water = mad(SNR_shortTR_water(:));
% 
% % SNR in fat
% median_SMURF_fat = median(SNR_SMURF_fat(:));
% median_longTR_fat = median(SNR_longTR_fat(:));
% median_shortTR_fat = median(SNR_shortTR_fat(:));
% mad_SMURF_fat = mad(SNR_SMURF_fat(:));
% mad_longTR_fat = mad(SNR_longTR_fat(:));
% mad_shortTR_fat = mad(SNR_shortTR_fat(:));
% 
% % SNR efficiency in water
% median_eff_SMURF_water = median(SNR_eff_SMURF_water(:));
% median_eff_longTR_water = median(SNR_eff_longTR_water(:));
% median_eff_shortTR_water = median(SNR_eff_shortTR_water(:));
% mad_eff_SMURF_water = mad(SNR_eff_SMURF_water(:));
% mad_eff_longTR_water = mad(SNR_eff_longTR_water(:));
% mad_eff_shortTR_water = mad(SNR_eff_shortTR_water(:));
% 
% % SNR efficiency in fat
% median_eff_SMURF_fat = median(SNR_eff_SMURF_fat(:));
% median_eff_longTR_fat = median(SNR_eff_longTR_fat(:));
% median_eff_shortTR_fat = median(SNR_eff_shortTR_fat(:));
% mad_eff_SMURF_fat = mad(SNR_eff_SMURF_fat(:));
% mad_eff_longTR_fat = mad(SNR_eff_longTR_fat(:));
% mad_eff_shortTR_fat = mad(SNR_eff_shortTR_fat(:));
% 
%%
disp('Finished');

warning on MATLAB:divideByZero
