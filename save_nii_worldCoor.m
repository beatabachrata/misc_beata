function save_nii_worldCoor(nii, name, orient)

    if (contains(orient,'sagittal_PA') || contains(orient,'sagittal_AP'))
        nii.img = flip(flip(nii.img,1),3);
        nii.img = permute(nii.img,[3,1,2,4]);        
        nii.hdr.dime.dim(2:4) = [nii.hdr.dime.dim(4),nii.hdr.dime.dim(2),nii.hdr.dime.dim(3)];
        nii.hdr.dime.pixdim(2:4) = [nii.hdr.dime.pixdim(4),nii.hdr.dime.pixdim(2),nii.hdr.dime.pixdim(3)];

    elseif (contains(orient,'coronal_HF') || contains(orient,'coronal_FH'))
        nii.img = flip(flip(nii.img,1),3);
        nii.img = permute(nii.img,[1,3,2,4]);
        nii.hdr.dime.dim(2:4) = [nii.hdr.dime.dim(2),nii.hdr.dime.dim(4),nii.hdr.dime.dim(3)];
        nii.hdr.dime.pixdim(2:4) = [nii.hdr.dime.pixdim(2),nii.hdr.dime.pixdim(4),nii.hdr.dime.pixdim(3)];

    elseif (contains(orient,'transversal_PA') || contains(orient,'transversal_AP'))
        nii.img = flip(nii.img,1);

    elseif (contains(orient,'coronal_RL') || contains(orient,'coronal_LR'))
        nii.img = flip(flip(nii.img,1),3);
        nii.img = permute(nii.img,[1,3,2,4]);        
        nii.hdr.dime.dim(2:4) = [nii.hdr.dime.dim(2),nii.hdr.dime.dim(4),nii.hdr.dime.dim(3)];
        nii.hdr.dime.pixdim(2:4) = [nii.hdr.dime.pixdim(2),nii.hdr.dime.pixdim(4),nii.hdr.dime.pixdim(3)];

    end
    
    nii.hdr.hist.originator(1:3) = [nii.hdr.dime.dim(2)/2,nii.hdr.dime.dim(3)/2,nii.hdr.dime.dim(4)/2];
    save_nii(nii, name);

end

