function [noise_median,signal_median, mask] = get_noise_median(img)
%% BB: estimate median noise in the image by separating the image into noise and signal using GMM

    gmm = fitgmdist(img(:),2);

    cluster1 = cluster(gmm,img(:))-1;
    cluster2 = abs(cluster(gmm,img(:))-2);
    
    j = 1; k = 1;
    for i = 1:length(cluster1)
        if cluster1(i) == 1
            img1(j) = cluster1(i).*img(i);
            j = j + 1;
        else
            img2(k) = cluster2(i).*img(i);
            k = k + 1;
        end
    end

    median1 = median(img1(:));
    median2 = median(img2(:));
    mask = img;

    if (median1 < median2)
        noise_median = median1;
        signal_median = median2;
        for i = 1:size(img(:))
            mask(i) = cluster2(i);
        end
    else
        noise_median = median2;
        signal_median = median1;
        for i = 1:size(img(:))
            mask(i) = cluster1(i);
        end
    end
    
    
    
end