function SNR = calc_SNR_func_bb(read_file, roi_name, cf_background, roi_bkg_name)

% load the image
image_nii = load_nii(read_file);
image = image_nii.img(:,:,:); 

if (size(image,4) > 1)
    image = squeeze(image(:,:,:,1));
end

% load the ROI
ROI_nii = load_nii(char(roi_name));
ROI = ROI_nii.img;

% get the image values within ROI
image_in_iROI = image(find(ROI(:,:,:)~=0));
    
if (~cf_background)
    % get the noise within image ROI 
    noise_signal = std(image_in_iROI,1); 
    
else   
    % load the background ROI
    bkg_ROI_nii = load_nii(roi_bkg_name);
    bkg_ROI = bkg_ROI_nii.img;

    % get the image values within background ROI
    image_in_bkgROI = image(find(bkg_ROI~=0));
    
    % get the noise within background ROI 
    noise_signal = std(image_in_bkgROI,1); 
        
end

% calculate the SNR relative to mean signal
SNR = median(image_in_iROI)/noise_signal;  

end
