function [SNR, mean_signal, std_noise] = calc_iSNR_func_noiseScan_bb(readfile_image, readfile_noise, roi_filename_image);

image_nii = load_nii(readfile_image);
noise_nii = load_nii(readfile_noise);
ROI_nii = load_nii(char(roi_filename_image));

image = image_nii.img; 
noise = noise_nii.img; 
ROI = ROI_nii.img;

image_in_ROI = image(find(ROI(:,:,:)~=0));
noise_in_iROI = noise(find(ROI(:,:,:)~=0));

mean_signal = mean(image_in_ROI);
std_noise = std(noise_in_iROI)/sqrt(2);

SNR = mean_signal/std_noise;

end
