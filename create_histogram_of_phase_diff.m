
clear, clc, close all;

mask_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/fm_diff_to_GE_5_4_separate_TP/mask_FM_comb_GE_cmrr_1TP.nii';
readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/';


readfile = {
    'fm_diff_to_GE_5_4_separate_TP/diff_cmrr_2_1_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_2_1_to_GE_5_4.nii',
};

mask_nii=load_nii(mask_dir);
cmrr_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
mean_cmrr_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));

%% GRAPH
%
mask = mask_nii.img(:,:,:);
mask_vect = mask(:);
 
for tp = 1:size(cmrr_2_1_GE_PA_5_4_nii.img,4)
    cmrr_2_1_GE_PA_5_4_singleTP = cmrr_2_1_GE_PA_5_4_nii.img(:,:,:,tp);

    j=1;
    for i=1:size(mask_vect,1)
        if mask_vect(i) == 1
            cmrr_2_1_GE_PA_5_4_vect(tp,j) = cmrr_2_1_GE_PA_5_4_singleTP(i);
            j=j+1;
        end  
    end
end

j=1;
for i=1:size(mask_vect,1)
    if mask_vect(i) == 1
        mean_cmrr_2_1_GE_PA_5_4_vect(1,j) = mean_cmrr_2_1_GE_PA_5_4_nii.img(i);
        j=j+1;
    end  
end

figure;
[H_cmrr_2_1_GE_PA_5_4_tp1, range_cmrr_2_1_GE_PA_5_4_tp1]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(1,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp2, range_cmrr_2_1_GE_PA_5_4_tp2]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(2,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp3, range_cmrr_2_1_GE_PA_5_4_tp3]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(3,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp4, range_cmrr_2_1_GE_PA_5_4_tp4]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(4,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp5, range_cmrr_2_1_GE_PA_5_4_tp5]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(5,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp6, range_cmrr_2_1_GE_PA_5_4_tp6]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(6,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp7, range_cmrr_2_1_GE_PA_5_4_tp7]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(7,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp8, range_cmrr_2_1_GE_PA_5_4_tp8]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(8,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp9, range_cmrr_2_1_GE_PA_5_4_tp9]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(9,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_tp10, range_cmrr_2_1_GE_PA_5_4_tp10]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(10,:),[-460,360],170);
[H_cmrr_2_1_GE_PA_5_4_mean, range_cmrr_2_1_GE_PA_5_4_mean]=imHistogram(mean_cmrr_2_1_GE_PA_5_4_vect(1,:),[-460,360],170);
plot((range_cmrr_2_1_GE_PA_5_4_tp1./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp1,'b','LineWidth',1.5)
hold
plot((range_cmrr_2_1_GE_PA_5_4_tp2./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp2,'r','LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp3./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp3,'g','LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp4./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp4,'k','LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp5./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp5,'c','LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp6./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp6,'y','LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp7./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp7,'m','LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp8./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp8,'color',[1 0.4 0],'LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp9./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp9,'color',[0 0.4 0.4],'LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_tp10./(2*pi)),H_cmrr_2_1_GE_PA_5_4_tp10,'color',[0.6 0.6 0.6],'LineWidth',1.5)
plot((range_cmrr_2_1_GE_PA_5_4_mean./(2*pi)),H_cmrr_2_1_GE_PA_5_4_mean,'--k','LineWidth',2)    

%title('VOLUNTEER - cmrr 2 1 - difference to GE PA 5 4');
legend('TP 1','TP 2','TP 3','TP 4','TP 5','TP 6','TP7','TP 8','TP 9','TP 10','mean');
axis([-450/(2*pi) 350/(2*pi) 0 3800])
xlabel('[Hz]');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc;

mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/eroded_GE_mask.nii';
mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/fm_diff_to_GE_5_4_separate_TP/mask_FM_comb_GE_cmrr_1TP.nii';

readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/';



readfile = {
    'fm_diff_to_GE_5_4/diff_GE_2_1_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_GE_3_2_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_GE_4_3_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_GE_6_5_to_GE_5_4.nii',
 
    'fm_diff_to_GE_5_4/diff_cmrr_2_1_to_GE_5_4.nii',    
    'fm_diff_to_GE_5_4/diff_cmrr_3_2_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_4_3_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_5_4_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_6_5_to_GE_5_4.nii',
    
    'fm_diff_to_GE_5_4/reference_fm_GE_5_4.nii',
};

mask_GE_nii=load_nii(mask_dir_GE);
mask_epi_nii=load_nii(mask_dir_epi);

GE_PA_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
GE_PA_3_2_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
GE_PA_4_3_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
GE_PA_6_5_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));

cmrr_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
cmrr_3_2_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
cmrr_4_3_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(7))));
cmrr_5_4_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(8))));
cmrr_6_5_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(9))));

reference_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(10))));

%% GRAPH

mask_GE = mask_GE_nii.img(:,:,:);
mask_GE_vect = mask_GE(:);

mask_epi = mask_epi_nii.img(:,:,:);
mask_epi_vect = mask_epi(:);

j=1;
for i=1:size(mask_GE_vect,1)
    if mask_GE_vect(i) == 1
        GE_PA_2_1_GE_PA_5_4_vect(1,j) = GE_PA_2_1_GE_PA_5_4_nii.img(i);
        GE_PA_3_2_GE_PA_5_4_vect(1,j) = GE_PA_3_2_GE_PA_5_4_nii.img(i);
        GE_PA_4_3_GE_PA_5_4_vect(1,j) = GE_PA_4_3_GE_PA_5_4_nii.img(i);
        GE_PA_6_5_GE_PA_5_4_vect(1,j) = GE_PA_6_5_GE_PA_5_4_nii.img(i);

        reference_GE_PA_5_4_vect(1,j) = reference_GE_PA_5_4_nii.img(i);

        j=j+1;
    end  
end

j=1;
for i=1:size(mask_epi_vect,1)
    if mask_epi_vect(i) ~= 0
        cmrr_2_1_GE_PA_5_4_vect(1,j) = cmrr_2_1_GE_PA_5_4_nii.img(i);
        cmrr_3_2_GE_PA_5_4_vect(1,j) = cmrr_3_2_GE_PA_5_4_nii.img(i);
        cmrr_4_3_GE_PA_5_4_vect(1,j) = cmrr_4_3_GE_PA_5_4_nii.img(i);
        cmrr_5_4_GE_PA_5_4_vect(1,j) = cmrr_5_4_GE_PA_5_4_nii.img(i);
        cmrr_6_5_GE_PA_5_4_vect(1,j) = cmrr_6_5_GE_PA_5_4_nii.img(i);
 
        j=j+1;
    end  
end


% differences with respect to GE PA 5_4
% figure;
% [H_reference_GE_PA_5_4, range_reference_GE_PA_5_4]=imHistogram(reference_GE_PA_5_4_vect);
% plot(range_reference_GE_PA_5_4,H_reference_GE_PA_5_4,'k','LineWidth',1)
% title('VOLUNTEER - reference GE PA 5 4');


figure
[H_GE_PA_2_1_GE_PA_5_4, range_GE_PA_2_1_GE_PA_5_4]=imHistogram(GE_PA_2_1_GE_PA_5_4_vect,[-70,70],50);
[H_GE_PA_3_2_GE_PA_5_4, range_GE_PA_3_2_GE_PA_5_4]=imHistogram(GE_PA_3_2_GE_PA_5_4_vect,[-70,70],50);
[H_GE_PA_4_3_GE_PA_5_4, range_GE_PA_4_3_GE_PA_5_4]=imHistogram(GE_PA_4_3_GE_PA_5_4_vect,[-70,70],50);
[H_GE_PA_6_5_GE_PA_5_4, range_GE_PA_6_5_GE_PA_5_4]=imHistogram(GE_PA_6_5_GE_PA_5_4_vect,[-70,70],50);
plot((range_GE_PA_2_1_GE_PA_5_4./(2*pi)),H_GE_PA_2_1_GE_PA_5_4,'g','LineWidth',1.5)
hold
plot((range_GE_PA_3_2_GE_PA_5_4./(2*pi)),H_GE_PA_3_2_GE_PA_5_4,'b','LineWidth',1.5)
plot((range_GE_PA_4_3_GE_PA_5_4./(2*pi)),H_GE_PA_4_3_GE_PA_5_4,'r','LineWidth',1.5)
plot((range_GE_PA_6_5_GE_PA_5_4./(2*pi)),H_GE_PA_6_5_GE_PA_5_4,'k','LineWidth',1.5)
legend('14 - 7 ms','21 - 14 ms','28 - 21 ms', '42 - 35 ms');
%title('VOLUNTEER - difference to GE PA 5 4');
axis([-65/(2*pi) 65/(2*pi) 0 13000])
xlabel('[Hz]');
s


 
figure
[H_cmrr_2_1_GE_PA_5_4, range_cmrr_2_1_GE_PA_5_4]=imHistogram(cmrr_2_1_GE_PA_5_4_vect,[-360,260],100);
[H_cmrr_3_2_GE_PA_5_4, range_cmrr_3_2_GE_PA_5_4]=imHistogram(cmrr_3_2_GE_PA_5_4_vect,[-360,260],100);
[H_cmrr_4_3_GE_PA_5_4, range_cmrr_4_3_GE_PA_5_4]=imHistogram(cmrr_4_3_GE_PA_5_4_vect,[-360,260],100);
[H_cmrr_5_4_GE_PA_5_4, range_cmrr_5_4_GE_PA_5_4]=imHistogram(cmrr_5_4_GE_PA_5_4_vect,[-360,260],100);
[H_cmrr_6_5_GE_PA_5_4, range_cmrr_6_5_GE_PA_5_4]=imHistogram(cmrr_6_5_GE_PA_5_4_vect,[-360,260],100);
plot((range_cmrr_2_1_GE_PA_5_4./(2*pi)),H_cmrr_2_1_GE_PA_5_4,'b','LineWidth',1.5)
hold 
plot((range_cmrr_3_2_GE_PA_5_4./(2*pi)),H_cmrr_3_2_GE_PA_5_4,'r','LineWidth',1.5)
plot((range_cmrr_4_3_GE_PA_5_4./(2*pi)),H_cmrr_4_3_GE_PA_5_4,'g','LineWidth',1.5)
plot((range_cmrr_5_4_GE_PA_5_4./(2*pi)),H_cmrr_5_4_GE_PA_5_4,'k','LineWidth',1.5)
plot((range_cmrr_6_5_GE_PA_5_4./(2*pi)),H_cmrr_6_5_GE_PA_5_4,'m','LineWidth',1.5)
legend('31 -28 ms','34 - 31 ms','37 - 34 ms','40 - 37 ms','43 - 40 ms');
%title('VOLUNTEER - difference to GE PA 5 4');
axis([-350/(2*pi) 250/(2*pi) 0 9000])
xlabel('[Hz]');





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 
% clear, clc;
% 
% mask_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/fm_diff_to_GE_5_4_separate_TP/mask_FM_comb_GE_cmrr_1TP.nii';
% readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/';
% 
% 
% readfile = {
%     'fm_diff_to_GE_5_4_separate_TP/diff_to_means/cmrr_2_1.nii',    
% };
% 
% mask_nii=load_nii(mask_dir);
% cmrr_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));

% %% GRAPH
% %
% mask = mask_nii.img(:,:,:);
% mask_vect = mask(:);
% 
% % start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
%  
% for tp = 1:size(cmrr_2_1_GE_PA_5_4_nii.img,4)
%     cmrr_2_1_GE_PA_5_4_singleTP = cmrr_2_1_GE_PA_5_4_nii.img(:,:,:,tp);
% 
%     j=1;
%         for i=1:size(mask_vect,1)
%             if mask_vect(i) == 1
%                 cmrr_2_1_GE_PA_5_4_vect(tp,j) = cmrr_2_1_GE_PA_5_4_singleTP(i);
%                 j=j+1;
%             end  
%         end
% end
% 
% figure;
% [H_cmrr_2_1_GE_PA_5_4_tp1, range_cmrr_2_1_GE_PA_5_4_tp1]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(1,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp2, range_cmrr_2_1_GE_PA_5_4_tp2]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(2,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp3, range_cmrr_2_1_GE_PA_5_4_tp3]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(3,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp4, range_cmrr_2_1_GE_PA_5_4_tp4]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(4,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp5, range_cmrr_2_1_GE_PA_5_4_tp5]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(5,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp6, range_cmrr_2_1_GE_PA_5_4_tp6]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(6,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp7, range_cmrr_2_1_GE_PA_5_4_tp7]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(7,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp8, range_cmrr_2_1_GE_PA_5_4_tp8]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(8,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp9, range_cmrr_2_1_GE_PA_5_4_tp9]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(9,:),[-400,400],200);
% [H_cmrr_2_1_GE_PA_5_4_tp10, range_cmrr_2_1_GE_PA_5_4_tp10]=imHistogram(cmrr_2_1_GE_PA_5_4_vect(10,:),[-400,400],200);
% plot(range_cmrr_2_1_GE_PA_5_4_tp1,H_cmrr_2_1_GE_PA_5_4_tp1,'b','LineWidth',1.5)
% hold
% plot(range_cmrr_2_1_GE_PA_5_4_tp2,H_cmrr_2_1_GE_PA_5_4_tp2,'r','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp3,H_cmrr_2_1_GE_PA_5_4_tp3,'g','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp4,H_cmrr_2_1_GE_PA_5_4_tp4,'k','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp5,H_cmrr_2_1_GE_PA_5_4_tp5,'c','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp6,H_cmrr_2_1_GE_PA_5_4_tp6,'--b','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp7,H_cmrr_2_1_GE_PA_5_4_tp7,'--r','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp8,H_cmrr_2_1_GE_PA_5_4_tp8,'--g','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp9,H_cmrr_2_1_GE_PA_5_4_tp9,'--k','LineWidth',1.5)
% plot(range_cmrr_2_1_GE_PA_5_4_tp10,H_cmrr_2_1_GE_PA_5_4_tp10,'--c','LineWidth',1.5)
%     
% title('difference of individual TP and their mean of diff_cmrr_2_1_and_GE_5_4');
% legend('TP 1','TP2','TP 3','TP 4','TP 5','TP 6','TP7','TP 8','TP 9','TP 10');
% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% clear, clc, close all;
% 
% mask_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/fm_diff_to_GE_5_4_separate_TP/mask_FM_comb_GE_cmrr_1TP.nii';
% % mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171022/phase_evolution_cmrr_separate_TP/GE_masked_fieldmap_2_1_noTEcorr.nii';
% 
% readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/';
% 
% 
% readfile = {
%     'fm_diff_to_GE_5_4_separate_TP/diff_cmrr_2_1_to_GE_5_4.nii',    
%     'fm_diff_to_GE_5_4_separate_TP/diff_cmrr_3_2_to_GE_5_4.nii',
%     'fm_diff_to_GE_5_4_separate_TP/diff_cmrr_4_3_to_GE_5_4.nii',
%     'fm_diff_to_GE_5_4_separate_TP/diff_cmrr_5_4_to_GE_5_4.nii',
%     'fm_diff_to_GE_5_4_separate_TP/diff_cmrr_6_5_to_GE_5_4.nii',
%     
%     'fm_diff_to_GE_5_4_separate_TP/reference_fm_GE_5_4.nii',
% };
% 
% mask_nii=load_nii(mask_dir);
% % mask_epi_nii=load_nii(mask_dir_epi);
% 
% cmrr_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
% cmrr_3_2_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
% cmrr_4_3_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
% cmrr_5_4_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));
% cmrr_6_5_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
% 
% reference_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
% 
% %% GRAPH
% %
% mask = mask_nii.img(:,:,:);
% mask_vect = mask(:);
% % start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
% 
% for tp = 1:size(cmrr_2_1_GE_PA_5_4_nii.img,4)
%     cmrr_2_1_GE_PA_5_4_singleTP = cmrr_2_1_GE_PA_5_4_nii.img(:,:,:,tp);
%     cmrr_3_2_GE_PA_5_4_singleTP = cmrr_3_2_GE_PA_5_4_nii.img(:,:,:,tp);
%     cmrr_4_3_GE_PA_5_4_singleTP = cmrr_4_3_GE_PA_5_4_nii.img(:,:,:,tp);
%     cmrr_5_4_GE_PA_5_4_singleTP = cmrr_5_4_GE_PA_5_4_nii.img(:,:,:,tp);
%     cmrr_6_5_GE_PA_5_4_singleTP = cmrr_6_5_GE_PA_5_4_nii.img(:,:,:,tp);
%     
%     
%     j=1;
%         for i=1:size(mask_vect,1)
%             if mask_vect(i) == 1
%                 cmrr_2_1_GE_PA_5_4_vect(1,j) = cmrr_2_1_GE_PA_5_4_singleTP(i);
%                 cmrr_3_2_GE_PA_5_4_vect(1,j) = cmrr_3_2_GE_PA_5_4_singleTP(i);
%                 cmrr_4_3_GE_PA_5_4_vect(1,j) = cmrr_4_3_GE_PA_5_4_singleTP(i);
%                 cmrr_5_4_GE_PA_5_4_vect(1,j) = cmrr_5_4_GE_PA_5_4_singleTP(i);
%                 cmrr_6_5_GE_PA_5_4_vect(1,j) = cmrr_6_5_GE_PA_5_4_singleTP(i);
%                 
% %                 reference_GE_PA_5_4_vect(1,j) = reference_GE_PA_5_4_nii.img(i);
% 
%                 j=j+1;
%             end  
%          end
%   
% 
%     % differences with respect to GE PA 5_4
%     % [H_reference_GE_PA_5_4, range_reference_GE_PA_5_4]=imHistogram(reference_GE_PA_5_4_vect);
%     % plot(range_reference_GE_PA_5_4,H_reference_GE_PA_5_4,'k','LineWidth',1)
%     % title('reference GE PA 5 4');
% 
%     % 
%     figure
%     [H_cmrr_2_1_GE_PA_5_4, range_cmrr_2_1_GE_PA_5_4]=imHistogram(cmrr_2_1_GE_PA_5_4_vect,[-400,400],200);
%     [H_cmrr_3_2_GE_PA_5_4, range_cmrr_3_2_GE_PA_5_4]=imHistogram(cmrr_3_2_GE_PA_5_4_vect,[-400,400],200);
%     [H_cmrr_4_3_GE_PA_5_4, range_cmrr_4_3_GE_PA_5_4]=imHistogram(cmrr_4_3_GE_PA_5_4_vect,[-400,400],200);
%     [H_cmrr_5_4_GE_PA_5_4, range_cmrr_5_4_GE_PA_5_4]=imHistogram(cmrr_5_4_GE_PA_5_4_vect,[-400,400],200);
%     [H_cmrr_6_5_GE_PA_5_4, range_cmrr_6_5_GE_PA_5_4]=imHistogram(cmrr_6_5_GE_PA_5_4_vect,[-400,400],200);
%     plot(range_cmrr_2_1_GE_PA_5_4,H_cmrr_2_1_GE_PA_5_4,'b','LineWidth',1.5)
%     hold 
%     plot(range_cmrr_3_2_GE_PA_5_4,H_cmrr_3_2_GE_PA_5_4,'r','LineWidth',1.5)
%     plot(range_cmrr_4_3_GE_PA_5_4,H_cmrr_4_3_GE_PA_5_4,'g','LineWidth',1.5)
%     plot(range_cmrr_5_4_GE_PA_5_4,H_cmrr_5_4_GE_PA_5_4,'k','LineWidth',1.5)
%     plot(range_cmrr_6_5_GE_PA_5_4,H_cmrr_6_5_GE_PA_5_4,'m','LineWidth',1.5)
%     legend('cmrr 2 1','cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5');
%     title(sprintf('difference to GE PA 5 4 for TP %i',tp));
% 
% end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



clear, clc;

mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171022/eroded_GE_mask.nii';
mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171022/phase_evolution_cmrr/GE_masked_fieldmap_2_1_noTEcorr.nii';

readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171022/';


readfile = {
    'fm_diff_to_GE_5_4/diff_GE_2_1_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_GE_3_2_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_GE_4_3_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_GE_6_5_to_GE_5_4.nii',
 
    'fm_diff_to_GE_5_4/diff_cmrr_2_1_to_GE_5_4.nii',    
    'fm_diff_to_GE_5_4/diff_cmrr_3_2_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_4_3_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_5_4_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_6_5_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_7_6_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_8_7_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_9_8_to_GE_5_4.nii',
    'fm_diff_to_GE_5_4/diff_cmrr_10_9_to_GE_5_4.nii',
    
    'fm_diff_to_GE_5_4/reference_fm_GE_5_4.nii',
};

mask_GE_nii=load_nii(mask_dir_GE);
mask_epi_nii=load_nii(mask_dir_epi);

GE_PA_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
GE_PA_3_2_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
GE_PA_4_3_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
GE_PA_6_5_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));

cmrr_2_1_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
cmrr_3_2_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
cmrr_4_3_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(7))));
cmrr_5_4_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(8))));
cmrr_6_5_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(9))));
cmrr_7_6_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(10))));
cmrr_8_7_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(11))));
cmrr_9_8_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(12))));
cmrr_10_9_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(13))));

reference_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(14))));

%% GRAPH

mask_GE = mask_GE_nii.img(:,:,:);
mask_GE_vect = mask_GE(:);

mask_epi = mask_epi_nii.img(:,:,:);
mask_epi_vect = mask_epi(:);

start_GE = 0;
start_epi = 0;

j=1;
for i=1:size(mask_GE_vect,1)
    if mask_GE_vect(i) == 1
        GE_PA_2_1_GE_PA_5_4_vect(1,j) = GE_PA_2_1_GE_PA_5_4_nii.img(i);
        GE_PA_3_2_GE_PA_5_4_vect(1,j) = GE_PA_3_2_GE_PA_5_4_nii.img(i);
        GE_PA_4_3_GE_PA_5_4_vect(1,j) = GE_PA_4_3_GE_PA_5_4_nii.img(i);
        GE_PA_6_5_GE_PA_5_4_vect(1,j) = GE_PA_6_5_GE_PA_5_4_nii.img(i);

        reference_GE_PA_5_4_vect(1,j) = reference_GE_PA_5_4_nii.img(i);

        j=j+1;
    end  
end

j=1;
for i=1:size(mask_epi_vect,1)
    if mask_epi_vect(i) ~= 0
        cmrr_2_1_GE_PA_5_4_vect(1,j) = cmrr_2_1_GE_PA_5_4_nii.img(i);
        cmrr_3_2_GE_PA_5_4_vect(1,j) = cmrr_3_2_GE_PA_5_4_nii.img(i);
        cmrr_4_3_GE_PA_5_4_vect(1,j) = cmrr_4_3_GE_PA_5_4_nii.img(i);
        cmrr_5_4_GE_PA_5_4_vect(1,j) = cmrr_5_4_GE_PA_5_4_nii.img(i);
        cmrr_6_5_GE_PA_5_4_vect(1,j) = cmrr_6_5_GE_PA_5_4_nii.img(i);
        cmrr_7_6_GE_PA_5_4_vect(1,j) = cmrr_7_6_GE_PA_5_4_nii.img(i);
        cmrr_8_7_GE_PA_5_4_vect(1,j) = cmrr_8_7_GE_PA_5_4_nii.img(i);
        cmrr_9_8_GE_PA_5_4_vect(1,j) = cmrr_9_8_GE_PA_5_4_nii.img(i);
        cmrr_10_9_GE_PA_5_4_vect(1,j) = cmrr_10_9_GE_PA_5_4_nii.img(i);
 
        j=j+1;
    end  
end


% % differences with respect to GE PA 5_4
% [H_reference_GE_PA_5_4, range_reference_GE_PA_5_4]=imHistogram(reference_GE_PA_5_4_vect);
% plot(range_reference_GE_PA_5_4,H_reference_GE_PA_5_4,'k','LineWidth',1)
% title('reference GE PA 5 4');


figure
[H_GE_PA_2_1_GE_PA_5_4, range_GE_PA_2_1_GE_PA_5_4]=imHistogram(GE_PA_2_1_GE_PA_5_4_vect,[-35,35],40);
[H_GE_PA_3_2_GE_PA_5_4, range_GE_PA_3_2_GE_PA_5_4]=imHistogram(GE_PA_3_2_GE_PA_5_4_vect,[-35,35],40);
[H_GE_PA_4_3_GE_PA_5_4, range_GE_PA_4_3_GE_PA_5_4]=imHistogram(GE_PA_4_3_GE_PA_5_4_vect,[-35,35],40);
[H_GE_PA_6_5_GE_PA_5_4, range_GE_PA_6_5_GE_PA_5_4]=imHistogram(GE_PA_6_5_GE_PA_5_4_vect,[-35,35],40);
plot((range_GE_PA_2_1_GE_PA_5_4./(2*pi)),H_GE_PA_2_1_GE_PA_5_4,'g','LineWidth',1.5)
hold
plot((range_GE_PA_3_2_GE_PA_5_4./(2*pi)),H_GE_PA_3_2_GE_PA_5_4,'b','LineWidth',1.5)
plot((range_GE_PA_4_3_GE_PA_5_4./(2*pi)),H_GE_PA_4_3_GE_PA_5_4,'r','LineWidth',1.5)
plot((range_GE_PA_6_5_GE_PA_5_4./(2*pi)),H_GE_PA_6_5_GE_PA_5_4,'k','LineWidth',1.5)
legend('14 - 7 ms','21 - 14 ms','28 - 21 ms', '42 - 35 ms');
%title('PHANTOM - difference to GE PA 5 4');
axis([-30/(2*pi) 30/(2*pi) 0 38000])
xlabel('[Hz]');

% 
figure
[H_cmrr_2_1_GE_PA_5_4, range_cmrr_2_1_GE_PA_5_4]=imHistogram(cmrr_2_1_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_3_2_GE_PA_5_4, range_cmrr_3_2_GE_PA_5_4]=imHistogram(cmrr_3_2_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_4_3_GE_PA_5_4, range_cmrr_4_3_GE_PA_5_4]=imHistogram(cmrr_4_3_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_5_4_GE_PA_5_4, range_cmrr_5_4_GE_PA_5_4]=imHistogram(cmrr_5_4_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_6_5_GE_PA_5_4, range_cmrr_6_5_GE_PA_5_4]=imHistogram(cmrr_6_5_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_7_6_GE_PA_5_4, range_cmrr_7_6_GE_PA_5_4]=imHistogram(cmrr_7_6_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_8_7_GE_PA_5_4, range_cmrr_8_7_GE_PA_5_4]=imHistogram(cmrr_8_7_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_9_8_GE_PA_5_4, range_cmrr_9_8_GE_PA_5_4]=imHistogram(cmrr_9_8_GE_PA_5_4_vect,[-35,165],100);
[H_cmrr_10_9_GE_PA_5_4, range_cmrr_10_9_GE_PA_5_4]=imHistogram(cmrr_10_9_GE_PA_5_4_vect,[-35,165],100);
plot((range_cmrr_2_1_GE_PA_5_4./(2*pi)),H_cmrr_2_1_GE_PA_5_4,'b','LineWidth',1.5)
hold 
plot((range_cmrr_3_2_GE_PA_5_4./(2*pi)),H_cmrr_3_2_GE_PA_5_4,'r','LineWidth',1.5)
plot((range_cmrr_4_3_GE_PA_5_4./(2*pi)),H_cmrr_4_3_GE_PA_5_4,'g','LineWidth',1.5)
plot((range_cmrr_5_4_GE_PA_5_4./(2*pi)),H_cmrr_5_4_GE_PA_5_4,'k','LineWidth',1.5)
plot((range_cmrr_6_5_GE_PA_5_4./(2*pi)),H_cmrr_6_5_GE_PA_5_4,'m','LineWidth',1.5)
plot((range_cmrr_7_6_GE_PA_5_4./(2*pi)),H_cmrr_7_6_GE_PA_5_4,'c','LineWidth',1.5)
plot((range_cmrr_8_7_GE_PA_5_4./(2*pi)),H_cmrr_8_7_GE_PA_5_4,'y','LineWidth',1.5)
plot((range_cmrr_9_8_GE_PA_5_4./(2*pi)),H_cmrr_9_8_GE_PA_5_4,'color',[0.6 0.6 0.6],'LineWidth',1.5)
plot((range_cmrr_10_9_GE_PA_5_4./(2*pi)),H_cmrr_10_9_GE_PA_5_4,'color',[1 0.4 0],'LineWidth',1.5)

legend('30 - 28 ms','32 - 30 ms','34 - 32 ms','36 - 34 ms','38 - 36 ms','40 - 38 ms','42 - 40 ms','44 - 42 ms','46 - 44 ms');
%title('PHANTOM - mean difference to GE PA 5 4');
axis([-30/(2*pi) 155/(2*pi) 0 18000])
xlabel('[Hz]');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% clear, clc, close all;
% 
% mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171022/eroded_GE_mask.nii';
% mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171022/phase_evolution_cmrr/GE_masked_fieldmap_2_1_noTEcorr.nii';
% 
% readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171022/';
% 
% 
% readfile = {
%     'fm_diff_to_GE_2_1/diff_GE_PA_3_2_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_GE_PA_4_3_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_GE_PA_5_4_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_GE_PA_6_5_to_GE_PA_2_1.nii',
%  
%     'fm_diff_to_GE_2_1/diff_cmrr_2_1_to_GE_PA_2_1.nii',    
%     'fm_diff_to_GE_2_1/diff_cmrr_3_2_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_4_3_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_5_4_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_6_5_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_7_6_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_8_7_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_9_8_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_10_9_to_GE_PA_2_1.nii',
%       
%     
%     'fm_diff_to_mean_GE/diff_GE_PA_2_1_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_GE_PA_3_2_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_GE_PA_4_3_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_GE_PA_5_4_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_GE_PA_6_5_to_mean_GE_PA.nii',
%  
%     'fm_diff_to_mean_GE/diff_cmrr_2_1_to_mean_GE_PA.nii',    
%     'fm_diff_to_mean_GE/diff_cmrr_3_2_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_4_3_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_5_4_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_6_5_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_7_6_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_8_7_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_9_8_to_mean_GE_PA.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_10_9_to_mean_GE_PA.nii',
%     
%     
%     'fm_diff_to_cmrr_2_1/diff_cmrr_3_2_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_4_3_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_5_4_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_6_5_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_7_6_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_8_7_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_9_8_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_10_9_to_cmrr_2_1.nii',
%     
%     
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_2_1_to_mean_cmrr.nii',    
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_3_2_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_4_3_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_5_4_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_6_5_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_7_6_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_8_7_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_9_8_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr_first6/diff_cmrr_10_9_to_mean_cmrr.nii',
%     
%     
%     'fm_diff_to_GE_2_1/reference_fm_GE_PA_2_1.nii',
%     'fm_diff_to_mean_GE/reference_fm_mean_GE.nii',
%     'fm_diff_to_cmrr_2_1/reference_fm_cmrr_2_1.nii',
%     'fm_diff_to_mean_cmrr_first6/reference_fm_mean_cmrr.nii',
% };
% 
% mask_GE_nii=load_nii(mask_dir_GE);
% mask_epi_nii=load_nii(mask_dir_epi);
% 
% 
% GE_PA_3_2_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
% GE_PA_4_3_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
% GE_PA_5_4_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
% GE_PA_6_5_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));
% 
% cmrr_2_1_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
% cmrr_3_2_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
% cmrr_4_3_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(7))));
% cmrr_5_4_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(8))));
% cmrr_6_5_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(9))));
% cmrr_7_6_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(10))));
% cmrr_8_7_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(11))));
% cmrr_9_8_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(12))));
% cmrr_10_9_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(13))));
% 
% 
% 
% GE_PA_2_1_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(14))));
% GE_PA_3_2_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(15))));
% GE_PA_4_3_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(16))));
% GE_PA_5_4_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(17))));
% GE_PA_6_5_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(18))));
% 
% cmrr_2_1_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(19))));
% cmrr_3_2_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(20))));
% cmrr_4_3_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(21))));
% cmrr_5_4_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(22))));
% cmrr_6_5_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(23))));
% cmrr_7_6_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(24))));
% cmrr_8_7_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(25))));
% cmrr_9_8_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(26))));
% cmrr_10_9_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(27))));
% 
% 
% 
% cmrr_3_2_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(28))));
% cmrr_4_3_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(29))));
% cmrr_5_4_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(30))));
% cmrr_6_5_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(31))));
% cmrr_7_6_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(32))));
% cmrr_8_7_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(33))));
% cmrr_9_8_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(34))));
% cmrr_10_9_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(35))));
% 
% 
% cmrr_2_1_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(36))));
% cmrr_3_2_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(37))));
% cmrr_4_3_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(38))));
% cmrr_5_4_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(39))));
% cmrr_6_5_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(40))));
% cmrr_7_6_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(41))));
% cmrr_8_7_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(42))));
% cmrr_9_8_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(43))));
% cmrr_10_9_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(44))));
% 
% 
% reference_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(45))));
% reference_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(46))));
% reference_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(47))));
% reference_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(48))));
% 
% 
% %% GRAPH
% 
% mask_GE_1_40 = mask_GE_nii.img(:,:,8:end);
% mask_GE_vect = mask_GE_1_40(:);
% 
% mask_epi_1_36 = mask_epi_nii.img(:,:,12:end);
% mask_epi_vect = mask_epi_1_36(:);
% 
% start_GE = size(mask_GE_nii.img(:,:,1:7),1)*size(mask_GE_nii.img(:,:,1:7),2)*size(mask_GE_nii.img(:,:,1:7),3);
% start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
% 
% %start =1;
% 
% j=1;
% for i=1:size(mask_GE_vect,1)
%     if mask_GE_vect(i) == 1
%         GE_PA_3_2_GE_PA_2_1_vect(1,j) = GE_PA_3_2_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_4_3_GE_PA_2_1_vect(1,j) = GE_PA_4_3_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_5_4_GE_PA_2_1_vect(1,j) = GE_PA_5_4_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_6_5_GE_PA_2_1_vect(1,j) = GE_PA_6_5_GE_PA_2_1_nii.img(start_GE+i);
%         
%         GE_PA_2_1_mean_GE_PA_vect(1,j) = GE_PA_2_1_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_3_2_mean_GE_PA_vect(1,j) = GE_PA_3_2_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_4_3_mean_GE_PA_vect(1,j) = GE_PA_4_3_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_5_4_mean_GE_PA_vect(1,j) = GE_PA_5_4_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_6_5_mean_GE_PA_vect(1,j) = GE_PA_6_5_mean_GE_PA_nii.img(start_GE+i);
% 
%         reference_GE_PA_2_1_vect(1,j) = reference_GE_PA_2_1_nii.img(start_GE+i);
%         reference_mean_GE_PA_vect(1,j) = reference_mean_GE_PA_nii.img(start_GE+i);
% 
%         j=j+1;
%     end  
% end
% 
% j=1;
% for i=1:size(mask_epi_vect,1)
%     if mask_epi_vect(i) ~= 0
%         cmrr_2_1_GE_PA_2_1_vect(1,j) = cmrr_2_1_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_3_2_GE_PA_2_1_vect(1,j) = cmrr_3_2_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_4_3_GE_PA_2_1_vect(1,j) = cmrr_4_3_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_5_4_GE_PA_2_1_vect(1,j) = cmrr_5_4_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_6_5_GE_PA_2_1_vect(1,j) = cmrr_6_5_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_7_6_GE_PA_2_1_vect(1,j) = cmrr_7_6_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_8_7_GE_PA_2_1_vect(1,j) = cmrr_8_7_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_9_8_GE_PA_2_1_vect(1,j) = cmrr_9_8_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_10_9_GE_PA_2_1_vect(1,j) = cmrr_10_9_GE_PA_2_1_nii.img(start_epi+i); 
%         
%         cmrr_2_1_mean_GE_PA_vect(1,j) = cmrr_2_1_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_3_2_mean_GE_PA_vect(1,j) = cmrr_3_2_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_4_3_mean_GE_PA_vect(1,j) = cmrr_4_3_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_5_4_mean_GE_PA_vect(1,j) = cmrr_5_4_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_6_5_mean_GE_PA_vect(1,j) = cmrr_6_5_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_7_6_mean_GE_PA_vect(1,j) = cmrr_7_6_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_8_7_mean_GE_PA_vect(1,j) = cmrr_8_7_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_9_8_mean_GE_PA_vect(1,j) = cmrr_9_8_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_10_9_mean_GE_PA_vect(1,j) = cmrr_10_9_mean_GE_PA_nii.img(start_epi+i); 
%         
%         cmrr_3_2_cmrr_2_1_vect(1,j) = cmrr_3_2_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_4_3_cmrr_2_1_vect(1,j) = cmrr_4_3_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_5_4_cmrr_2_1_vect(1,j) = cmrr_5_4_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_6_5_cmrr_2_1_vect(1,j) = cmrr_6_5_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_7_6_cmrr_2_1_vect(1,j) = cmrr_7_6_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_8_7_cmrr_2_1_vect(1,j) = cmrr_8_7_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_9_8_cmrr_2_1_vect(1,j) = cmrr_9_8_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_10_9_cmrr_2_1_vect(1,j) = cmrr_10_9_cmrr_2_1_nii.img(start_epi+i); 
%         
%         cmrr_2_1_mean_cmrr_vect(1,j) = cmrr_2_1_mean_cmrr_nii.img(start_epi+i);
%         cmrr_3_2_mean_cmrr_vect(1,j) = cmrr_3_2_mean_cmrr_nii.img(start_epi+i);
%         cmrr_4_3_mean_cmrr_vect(1,j) = cmrr_4_3_mean_cmrr_nii.img(start_epi+i);
%         cmrr_5_4_mean_cmrr_vect(1,j) = cmrr_5_4_mean_cmrr_nii.img(start_epi+i);
%         cmrr_6_5_mean_cmrr_vect(1,j) = cmrr_6_5_mean_cmrr_nii.img(start_epi+i);
%         cmrr_7_6_mean_cmrr_vect(1,j) = cmrr_7_6_mean_cmrr_nii.img(start_epi+i);
%         cmrr_8_7_mean_cmrr_vect(1,j) = cmrr_8_7_mean_cmrr_nii.img(start_epi+i);
%         cmrr_9_8_mean_cmrr_vect(1,j) = cmrr_9_8_mean_cmrr_nii.img(start_epi+i);
%         cmrr_10_9_mean_cmrr_vect(1,j) = cmrr_10_9_mean_cmrr_nii.img(start_epi+i); 
%         
%         reference_cmrr_2_1_vect(1,j) = reference_cmrr_2_1_nii.img(start_epi+i);
%         reference_mean_cmrr_vect(1,j) = reference_mean_cmrr_nii.img(start_epi+i);
% 
%         j=j+1;
%     end  
% end


%% differences with respect to GE PA 2 1
% [H_reference_GE_PA_2_1, range_reference_GE_PA_2_1]=imHistogram(reference_GE_PA_2_1_vect);
% plot(range_reference_GE_PA_2_1,H_reference_GE_PA_2_1,'k','LineWidth',1)
% title('reference GE PA 2 1');
% 
% 
% figure
% [H_GE_PA_3_2_GE_PA_2_1, range_GE_PA_3_2_GE_PA_2_1]=imHistogram(GE_PA_3_2_GE_PA_2_1_vect,[-20,30],50);
% [H_GE_PA_4_3_GE_PA_2_1, range_GE_PA_4_3_GE_PA_2_1]=imHistogram(GE_PA_4_3_GE_PA_2_1_vect,[-20,30],50);
% [H_GE_PA_5_4_GE_PA_2_1, range_GE_PA_5_4_GE_PA_2_1]=imHistogram(GE_PA_5_4_GE_PA_2_1_vect,[-20,30],50);
% [H_GE_PA_6_5_GE_PA_2_1, range_GE_PA_6_5_GE_PA_2_1]=imHistogram(GE_PA_6_5_GE_PA_2_1_vect,[-20,30],50);
% plot(range_GE_PA_3_2_GE_PA_2_1,H_GE_PA_3_2_GE_PA_2_1,'b','LineWidth',1.5)
% hold
% plot(range_GE_PA_4_3_GE_PA_2_1,H_GE_PA_4_3_GE_PA_2_1,'r','LineWidth',1.5)
% plot(range_GE_PA_5_4_GE_PA_2_1,H_GE_PA_5_4_GE_PA_2_1,'g','LineWidth',1.5)
% plot(range_GE_PA_6_5_GE_PA_2_1,H_GE_PA_6_5_GE_PA_2_1,'k','LineWidth',1.5)
% legend('GE PA 3 2','GE PA 4 3','GE PA 5 4','GE PA 6 5');
% title('difference to GE PA 2 1');
% 
% 
% figure
% [H_cmrr_2_1_GE_PA_2_1, range_cmrr_2_1_GE_PA_2_1]=imHistogram(cmrr_2_1_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_3_2_GE_PA_2_1, range_cmrr_3_2_GE_PA_2_1]=imHistogram(cmrr_3_2_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_4_3_GE_PA_2_1, range_cmrr_4_3_GE_PA_2_1]=imHistogram(cmrr_4_3_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_5_4_GE_PA_2_1, range_cmrr_5_4_GE_PA_2_1]=imHistogram(cmrr_5_4_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_6_5_GE_PA_2_1, range_cmrr_6_5_GE_PA_2_1]=imHistogram(cmrr_6_5_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_7_6_GE_PA_2_1, range_cmrr_7_6_GE_PA_2_1]=imHistogram(cmrr_7_6_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_8_7_GE_PA_2_1, range_cmrr_8_7_GE_PA_2_1]=imHistogram(cmrr_8_7_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_9_8_GE_PA_2_1, range_cmrr_9_8_GE_PA_2_1]=imHistogram(cmrr_9_8_GE_PA_2_1_vect,[-20,150],170);
% [H_cmrr_10_9_GE_PA_2_1, range_cmrr_10_9_GE_PA_2_1]=imHistogram(cmrr_10_9_GE_PA_2_1_vect,[-20,150],170);
% plot(range_cmrr_2_1_GE_PA_2_1,H_cmrr_2_1_GE_PA_2_1,'b','LineWidth',1.5)
% hold 
% plot(range_cmrr_3_2_GE_PA_2_1,H_cmrr_3_2_GE_PA_2_1,'r','LineWidth',1.5)
% plot(range_cmrr_4_3_GE_PA_2_1,H_cmrr_4_3_GE_PA_2_1,'g','LineWidth',1.5)
% plot(range_cmrr_5_4_GE_PA_2_1,H_cmrr_5_4_GE_PA_2_1,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_GE_PA_2_1,H_cmrr_6_5_GE_PA_2_1,'m','LineWidth',1.5)
% plot(range_cmrr_7_6_GE_PA_2_1,H_cmrr_7_6_GE_PA_2_1,'c','LineWidth',1.5)
% plot(range_cmrr_8_7_GE_PA_2_1,H_cmrr_8_7_GE_PA_2_1,'y','LineWidth',1.5)
% plot(range_cmrr_9_8_GE_PA_2_1,H_cmrr_9_8_GE_PA_2_1,'--b','LineWidth',1.5)
% plot(range_cmrr_10_9_GE_PA_2_1,H_cmrr_10_9_GE_PA_2_1,'--r','LineWidth',1.5)
% legend('cmrr 2 1','cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5','cmrr 7 6','cmrr 8 7','cmrr 9 8','cmrr 10 9');
% title('difference to GE PA 2 1');


% %% differences with respect to mean GE PA
% figure
% [H_reference_mean_GE_PA, range_reference_mean_GE_PA]=imHistogram(reference_mean_GE_PA_vect);
% plot(range_reference_mean_GE_PA,H_reference_mean_GE_PA,'k','LineWidth',1)
% title('reference mean GE PA');
% 
% 
% figure
% [H_GE_PA_2_1_mean_GE_PA, range_GE_PA_2_1_mean_GE_PA]=imHistogram(GE_PA_2_1_mean_GE_PA_vect,[-20,30],50);
% [H_GE_PA_3_2_mean_GE_PA, range_GE_PA_3_2_mean_GE_PA]=imHistogram(GE_PA_3_2_mean_GE_PA_vect,[-20,30],50);
% [H_GE_PA_4_3_mean_GE_PA, range_GE_PA_4_3_mean_GE_PA]=imHistogram(GE_PA_4_3_mean_GE_PA_vect,[-20,30],50);
% [H_GE_PA_5_4_mean_GE_PA, range_GE_PA_5_4_mean_GE_PA]=imHistogram(GE_PA_5_4_mean_GE_PA_vect,[-20,30],50);
% [H_GE_PA_6_5_mean_GE_PA, range_GE_PA_6_5_mean_GE_PA]=imHistogram(GE_PA_6_5_mean_GE_PA_vect,[-20,30],50);
% plot(range_GE_PA_2_1_mean_GE_PA,H_GE_PA_2_1_mean_GE_PA,'b','LineWidth',1)
% hold
% plot(range_GE_PA_3_2_mean_GE_PA,H_GE_PA_3_2_mean_GE_PA,'r','LineWidth',1.5)
% plot(range_GE_PA_4_3_mean_GE_PA,H_GE_PA_4_3_mean_GE_PA,'g','LineWidth',1.5)
% plot(range_GE_PA_5_4_mean_GE_PA,H_GE_PA_5_4_mean_GE_PA,'k','LineWidth',1.5)
% plot(range_GE_PA_6_5_mean_GE_PA,H_GE_PA_6_5_mean_GE_PA,'c','LineWidth',1.5)
% legend('GE PA 2 1','GE PA 3 2','GE PA 4 3','GE PA 5 4','GE PA 6 5');
% title('difference to mean GE PA');
% 
% 
% figure
% [H_cmrr_2_1_mean_GE_PA, range_cmrr_2_1_mean_GE_PA]=imHistogram(cmrr_2_1_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_3_2_mean_GE_PA, range_cmrr_3_2_mean_GE_PA]=imHistogram(cmrr_3_2_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_4_3_mean_GE_PA, range_cmrr_4_3_mean_GE_PA]=imHistogram(cmrr_4_3_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_5_4_mean_GE_PA, range_cmrr_5_4_mean_GE_PA]=imHistogram(cmrr_5_4_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_6_5_mean_GE_PA, range_cmrr_6_5_mean_GE_PA]=imHistogram(cmrr_6_5_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_7_6_mean_GE_PA, range_cmrr_7_6_mean_GE_PA]=imHistogram(cmrr_7_6_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_8_7_mean_GE_PA, range_cmrr_8_7_mean_GE_PA]=imHistogram(cmrr_8_7_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_9_8_mean_GE_PA, range_cmrr_9_8_mean_GE_PA]=imHistogram(cmrr_9_8_mean_GE_PA_vect,[-20,150],170);
% [H_cmrr_10_9_mean_GE_PA, range_cmrr_10_9_mean_GE_PA]=imHistogram(cmrr_10_9_mean_GE_PA_vect,[-20,150],170);
% 
% plot(range_cmrr_2_1_mean_GE_PA,H_cmrr_2_1_mean_GE_PA,'b','LineWidth',1.5)
% hold 
% plot(range_cmrr_3_2_mean_GE_PA,H_cmrr_3_2_mean_GE_PA,'r','LineWidth',1.5)
% plot(range_cmrr_4_3_mean_GE_PA,H_cmrr_4_3_mean_GE_PA,'g','LineWidth',1.5)
% plot(range_cmrr_5_4_mean_GE_PA,H_cmrr_5_4_mean_GE_PA,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_mean_GE_PA,H_cmrr_6_5_mean_GE_PA,'m','LineWidth',1.5)
% plot(range_cmrr_7_6_mean_GE_PA,H_cmrr_7_6_mean_GE_PA,'c','LineWidth',1.5)
% plot(range_cmrr_8_7_mean_GE_PA,H_cmrr_8_7_mean_GE_PA,'y','LineWidth',1.5)
% plot(range_cmrr_9_8_mean_GE_PA,H_cmrr_9_8_mean_GE_PA,'--b','LineWidth',1.5)
% plot(range_cmrr_10_9_mean_GE_PA,H_cmrr_10_9_mean_GE_PA,'--r','LineWidth',1.5)
% legend('cmrr 2 1','cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5','cmrr 7 6','cmrr 8 7','cmrr 9 8','cmrr 10 9');
% title('difference to mean GE PA');
% 
% 
% 
% %% differences with respect to cmrr 2 1
% % figure
% % [H_reference_cmrr_2_1, range_reference_cmrr_2_1]=imHistogram(reference_cmrr_2_1_vect);
% % plot(range_reference_cmrr_2_1,H_reference_cmrr_2_1,'k','LineWidth',1)
% % title('reference cmrr 2 1');
% % 
% % 
% % figure
% % [H_cmrr_3_2_cmrr_2_1, range_cmrr_3_2_cmrr_2_1]=imHistogram(cmrr_3_2_cmrr_2_1_vect,[-20,150],170);
% % [H_cmrr_4_3_cmrr_2_1, range_cmrr_4_3_cmrr_2_1]=imHistogram(cmrr_4_3_cmrr_2_1_vect,[-20,150],170);
% % [H_cmrr_5_4_cmrr_2_1, range_cmrr_5_4_cmrr_2_1]=imHistogram(cmrr_5_4_cmrr_2_1_vect,[-20,150],170);
% % [H_cmrr_6_5_cmrr_2_1, range_cmrr_6_5_cmrr_2_1]=imHistogram(cmrr_6_5_cmrr_2_1_vect,[-20,150],170);
% % [H_cmrr_7_6_cmrr_2_1, range_cmrr_7_6_cmrr_2_1]=imHistogram(cmrr_7_6_cmrr_2_1_vect,[-20,150],170);
% % [H_cmrr_8_7_cmrr_2_1, range_cmrr_8_7_cmrr_2_1]=imHistogram(cmrr_8_7_cmrr_2_1_vect,[-20,150],170);
% % [H_cmrr_9_8_cmrr_2_1, range_cmrr_9_8_cmrr_2_1]=imHistogram(cmrr_9_8_cmrr_2_1_vect,[-20,150],170);
% % [H_cmrr_10_9_cmrr_2_1, range_cmrr_10_9_cmrr_2_1]=imHistogram(cmrr_10_9_cmrr_2_1_vect,[-20,150],170);
% % plot(range_cmrr_3_2_cmrr_2_1,H_cmrr_3_2_cmrr_2_1,'b','LineWidth',1.5)
% % hold
% % plot(range_cmrr_4_3_cmrr_2_1,H_cmrr_4_3_cmrr_2_1,'r','LineWidth',1.5)
% % plot(range_cmrr_5_4_cmrr_2_1,H_cmrr_5_4_cmrr_2_1,'g','LineWidth',1.5)
% % plot(range_cmrr_6_5_cmrr_2_1,H_cmrr_6_5_cmrr_2_1,'k','LineWidth',1.5)
% % plot(range_cmrr_7_6_cmrr_2_1,H_cmrr_7_6_cmrr_2_1,'m','LineWidth',1.5)
% % plot(range_cmrr_8_7_cmrr_2_1,H_cmrr_8_7_cmrr_2_1,'c','LineWidth',1.5)
% % plot(range_cmrr_9_8_cmrr_2_1,H_cmrr_9_8_cmrr_2_1,'y','LineWidth',1.5)
% % plot(range_cmrr_10_9_cmrr_2_1,H_cmrr_10_9_cmrr_2_1,'--r','LineWidth',1.5)
% % legend('cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5','cmrr 7 6','cmrr 8 7','cmrr 9 8','cmrr 10 9');
% % title('difference to cmrr 2 1');
% 
% 
% 
% %% differences with respect to mean cmrr
% figure
% [H_reference_mean_cmrr,range_reference_mean_cmrr]=imHistogram(reference_mean_cmrr_vect);
% plot(range_reference_mean_cmrr,H_reference_mean_cmrr,'k','LineWidth',1)
% title('reference mean cmrr');
% 
% 
% figure
% [H_cmrr_2_1_mean_cmrr, range_cmrr_2_1_mean_cmrr]=imHistogram(cmrr_2_1_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_3_2_mean_cmrr, range_cmrr_3_2_mean_cmrr]=imHistogram(cmrr_3_2_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_4_3_mean_cmrr, range_cmrr_4_3_mean_cmrr]=imHistogram(cmrr_4_3_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_5_4_mean_cmrr, range_cmrr_5_4_mean_cmrr]=imHistogram(cmrr_5_4_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_6_5_mean_cmrr, range_cmrr_6_5_mean_cmrr]=imHistogram(cmrr_6_5_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_7_6_mean_cmrr, range_cmrr_7_6_mean_cmrr]=imHistogram(cmrr_7_6_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_8_7_mean_cmrr, range_cmrr_8_7_mean_cmrr]=imHistogram(cmrr_8_7_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_9_8_mean_cmrr, range_cmrr_9_8_mean_cmrr]=imHistogram(cmrr_9_8_mean_cmrr_vect,[-50,100],150);
% [H_cmrr_10_9_mean_cmrr, range_cmrr_10_9_mean_cmrr]=imHistogram(cmrr_10_9_mean_cmrr_vect,[-50,100],150);
% plot(range_cmrr_2_1_mean_cmrr,H_cmrr_2_1_mean_cmrr,'b','LineWidth',1.5)
% hold
% plot(range_cmrr_3_2_mean_cmrr,H_cmrr_3_2_mean_cmrr,'r','LineWidth',1.5)
% plot(range_cmrr_4_3_mean_cmrr,H_cmrr_4_3_mean_cmrr,'g','LineWidth',1.5)
% plot(range_cmrr_5_4_mean_cmrr,H_cmrr_5_4_mean_cmrr,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_mean_cmrr,H_cmrr_6_5_mean_cmrr,'m','LineWidth',1.5)
% plot(range_cmrr_7_6_mean_cmrr,H_cmrr_7_6_mean_cmrr,'c','LineWidth',1.5)
% plot(range_cmrr_8_7_mean_cmrr,H_cmrr_8_7_mean_cmrr,'y','LineWidth',1.5)
% plot(range_cmrr_9_8_mean_cmrr,H_cmrr_9_8_mean_cmrr,'--b','LineWidth',1.5)
% plot(range_cmrr_10_9_mean_cmrr,H_cmrr_10_9_mean_cmrr,'--r','LineWidth',1.5)
% legend('cmrr 2 1','cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5','cmrr 7 6','cmrr 8 7','cmrr 9 8','cmrr 10 9');
% title('difference to mean cmrr');
% 
% 
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
% clear, clc;
% 
% mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/eroded_GE_mask.nii';
% mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/phase_evolution_cmrr/GE_masked_fieldmap_2_1_noTEcorr.nii';
% 
% readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/';
% 
% 
% readfile = {
%     'fm_diff_to_mean_GE/diff_GE_2_1_to_mean_GE.nii',
%     'fm_diff_to_mean_GE/diff_GE_3_2_to_mean_GE.nii',
%     'fm_diff_to_mean_GE/diff_GE_4_3_to_mean_GE.nii',
%     'fm_diff_to_mean_GE/diff_GE_5_4_to_mean_GE.nii',
%     'fm_diff_to_mean_GE/diff_GE_6_5_to_mean_GE.nii',
%  
%     'fm_diff_to_mean_GE/diff_cmrr_2_1_to_mean_GE.nii',    
%     'fm_diff_to_mean_GE/diff_cmrr_3_2_to_mean_GE.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_4_3_to_mean_GE.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_5_4_to_mean_GE.nii',
%     'fm_diff_to_mean_GE/diff_cmrr_6_5_to_mean_GE.nii',
% 
%     'fm_diff_to_mean_cmrr/diff_cmrr_2_1_to_mean_cmrr.nii',    
%     'fm_diff_to_mean_cmrr/diff_cmrr_3_2_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr/diff_cmrr_4_3_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr/diff_cmrr_5_4_to_mean_cmrr.nii',
%     'fm_diff_to_mean_cmrr/diff_cmrr_6_5_to_mean_cmrr.nii',
%    
%     'fm_diff_to_mean_GE/reference_fm_mean_GE.nii',
%     'fm_diff_to_mean_cmrr/reference_fm_mean_cmrr.nii',
%     
%     
%     
%     'fm_diff_to_GE_2_1/diff_GE_3_2_to_GE_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_GE_4_3_to_GE_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_GE_5_4_to_GE_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_GE_6_5_to_GE_2_1.nii',
%  
%     'fm_diff_to_GE_2_1/diff_cmrr_2_1_to_GE_2_1.nii',    
%     'fm_diff_to_GE_2_1/diff_cmrr_3_2_to_GE_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_4_3_to_GE_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_5_4_to_GE_2_1.nii',
%     'fm_diff_to_GE_2_1/diff_cmrr_6_5_to_GE_2_1.nii',
% 
%     'fm_diff_to_cmrr_2_1/diff_cmrr_3_2_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_4_3_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_5_4_to_cmrr_2_1.nii',
%     'fm_diff_to_cmrr_2_1/diff_cmrr_6_5_to_cmrr_2_1.nii',
%     
%     'fm_diff_to_GE_2_1/reference_fm_GE_2_1.nii',
%     'fm_diff_to_cmrr_2_1/reference_fm_cmrr_2_1.nii',
% 
% };
% 
% mask_GE_nii=load_nii(mask_dir_GE);
% mask_epi_nii=load_nii(mask_dir_epi);
% 
% GE_PA_2_1_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
% GE_PA_3_2_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
% GE_PA_4_3_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
% GE_PA_5_4_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));
% GE_PA_6_5_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
% 
% cmrr_2_1_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
% cmrr_3_2_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(7))));
% cmrr_4_3_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(8))));
% cmrr_5_4_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(9))));
% cmrr_6_5_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(10))));
% 
% 
% cmrr_2_1_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(11))));
% cmrr_3_2_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(12))));
% cmrr_4_3_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(13))));
% cmrr_5_4_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(14))));
% cmrr_6_5_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(15))));
% 
% reference_mean_GE_PA_nii=load_nii(fullfile(readfile_dir,char(readfile(16))));
% reference_mean_cmrr_nii=load_nii(fullfile(readfile_dir,char(readfile(17))));
% 
% 
% 
% 
% GE_PA_3_2_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(18))));
% GE_PA_4_3_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(19))));
% GE_PA_5_4_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(20))));
% GE_PA_6_5_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(21))));
% 
% cmrr_2_1_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(22))));
% cmrr_3_2_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(23))));
% cmrr_4_3_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(24))));
% cmrr_5_4_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(25))));
% cmrr_6_5_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(26))));
% 
% cmrr_3_2_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(27))));
% cmrr_4_3_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(28))));
% cmrr_5_4_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(29))));
% cmrr_6_5_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(30))));
% 
% reference_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(31))));
% reference_cmrr_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(32))));
% 
% 
% 
% %% GRAPH
% 
% mask_GE_1_40 = mask_GE_nii.img(:,:,8:end);
% mask_GE_vect = mask_GE_1_40(:);
% 
% mask_epi_1_36 = mask_epi_nii.img(:,:,12:end);
% mask_epi_vect = mask_epi_1_36(:);
% 
% start_GE = size(mask_GE_nii.img(:,:,1:7),1)*size(mask_GE_nii.img(:,:,1:7),2)*size(mask_GE_nii.img(:,:,1:7),3);
% start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
% 
% %start =1;
% 
% j=1;
% for i=1:size(mask_GE_vect,1)
%     if mask_GE_vect(i) == 1
%         GE_PA_3_2_GE_PA_2_1_vect(1,j) = GE_PA_3_2_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_4_3_GE_PA_2_1_vect(1,j) = GE_PA_4_3_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_5_4_GE_PA_2_1_vect(1,j) = GE_PA_5_4_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_6_5_GE_PA_2_1_vect(1,j) = GE_PA_6_5_GE_PA_2_1_nii.img(start_GE+i);
%         
%         GE_PA_2_1_mean_GE_PA_vect(1,j) = GE_PA_2_1_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_3_2_mean_GE_PA_vect(1,j) = GE_PA_3_2_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_4_3_mean_GE_PA_vect(1,j) = GE_PA_4_3_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_5_4_mean_GE_PA_vect(1,j) = GE_PA_5_4_mean_GE_PA_nii.img(start_GE+i);
%         GE_PA_6_5_mean_GE_PA_vect(1,j) = GE_PA_6_5_mean_GE_PA_nii.img(start_GE+i);
% 
%         reference_mean_GE_PA_vect(1,j) = reference_mean_GE_PA_nii.img(start_GE+i);
%         reference_GE_PA_2_1_vect(1,j) = reference_GE_PA_2_1_nii.img(start_GE+i);
% 
%         j=j+1;
%     end  
% end
% 
% j=1;
% for i=1:size(mask_epi_vect,1)
%     if mask_epi_vect(i) ~= 0
%         cmrr_2_1_GE_PA_2_1_vect(1,j) = cmrr_2_1_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_3_2_GE_PA_2_1_vect(1,j) = cmrr_3_2_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_4_3_GE_PA_2_1_vect(1,j) = cmrr_4_3_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_5_4_GE_PA_2_1_vect(1,j) = cmrr_5_4_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_6_5_GE_PA_2_1_vect(1,j) = cmrr_6_5_GE_PA_2_1_nii.img(start_epi+i);
%         
%         cmrr_2_1_mean_GE_PA_vect(1,j) = cmrr_2_1_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_3_2_mean_GE_PA_vect(1,j) = cmrr_3_2_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_4_3_mean_GE_PA_vect(1,j) = cmrr_4_3_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_5_4_mean_GE_PA_vect(1,j) = cmrr_5_4_mean_GE_PA_nii.img(start_epi+i);
%         cmrr_6_5_mean_GE_PA_vect(1,j) = cmrr_6_5_mean_GE_PA_nii.img(start_epi+i);
%         
%         cmrr_3_2_cmrr_2_1_vect(1,j) = cmrr_3_2_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_4_3_cmrr_2_1_vect(1,j) = cmrr_4_3_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_5_4_cmrr_2_1_vect(1,j) = cmrr_5_4_cmrr_2_1_nii.img(start_epi+i);
%         cmrr_6_5_cmrr_2_1_vect(1,j) = cmrr_6_5_cmrr_2_1_nii.img(start_epi+i); 
%         
%         cmrr_2_1_mean_cmrr_vect(1,j) = cmrr_2_1_mean_cmrr_nii.img(start_epi+i);
%         cmrr_3_2_mean_cmrr_vect(1,j) = cmrr_3_2_mean_cmrr_nii.img(start_epi+i);
%         cmrr_4_3_mean_cmrr_vect(1,j) = cmrr_4_3_mean_cmrr_nii.img(start_epi+i);
%         cmrr_5_4_mean_cmrr_vect(1,j) = cmrr_5_4_mean_cmrr_nii.img(start_epi+i);
%         cmrr_6_5_mean_cmrr_vect(1,j) = cmrr_6_5_mean_cmrr_nii.img(start_epi+i); 
%         
%         reference_mean_cmrr_vect(1,j) = reference_mean_cmrr_nii.img(start_epi+i);
%         reference_cmrr_2_1_vect(1,j) = reference_cmrr_2_1_nii.img(start_epi+i);
% 
%         j=j+1;
%     end  
% end
% 
% 
% 
% %% differences with respect to mean GE PA
% figure
% [H_reference_mean_GE_PA, range_reference_mean_GE_PA]=imHistogram(reference_mean_GE_PA_vect);
% plot(range_reference_mean_GE_PA,H_reference_mean_GE_PA,'k','LineWidth',1)
% title('reference mean GE PA');
% 
% 
% figure
% [H_GE_PA_2_1_mean_GE_PA, range_GE_PA_2_1_mean_GE_PA]=imHistogram(GE_PA_2_1_mean_GE_PA_vect,[-50,50],100);
% [H_GE_PA_3_2_mean_GE_PA, range_GE_PA_3_2_mean_GE_PA]=imHistogram(GE_PA_3_2_mean_GE_PA_vect,[-50,50],100);
% [H_GE_PA_4_3_mean_GE_PA, range_GE_PA_4_3_mean_GE_PA]=imHistogram(GE_PA_4_3_mean_GE_PA_vect,[-50,50],100);
% [H_GE_PA_5_4_mean_GE_PA, range_GE_PA_5_4_mean_GE_PA]=imHistogram(GE_PA_5_4_mean_GE_PA_vect,[-50,50],100);
% [H_GE_PA_6_5_mean_GE_PA, range_GE_PA_6_5_mean_GE_PA]=imHistogram(GE_PA_6_5_mean_GE_PA_vect,[-50,50],100);
% plot(range_GE_PA_2_1_mean_GE_PA,H_GE_PA_2_1_mean_GE_PA,'b','LineWidth',1)
% hold
% plot(range_GE_PA_3_2_mean_GE_PA,H_GE_PA_3_2_mean_GE_PA,'r','LineWidth',1.5)
% plot(range_GE_PA_4_3_mean_GE_PA,H_GE_PA_4_3_mean_GE_PA,'g','LineWidth',1.5)
% plot(range_GE_PA_5_4_mean_GE_PA,H_GE_PA_5_4_mean_GE_PA,'k','LineWidth',1.5)
% plot(range_GE_PA_6_5_mean_GE_PA,H_GE_PA_6_5_mean_GE_PA,'c','LineWidth',1.5)
% legend('GE PA 2 1','GE PA 3 2','GE PA 4 3','GE PA 5 4','GE PA 6 5');
% title('difference to mean GE PA');
% 
% 
% figure
% [H_cmrr_2_1_mean_GE_PA, range_cmrr_2_1_mean_GE_PA]=imHistogram(cmrr_2_1_mean_GE_PA_vect,[-350,250],150);
% [H_cmrr_3_2_mean_GE_PA, range_cmrr_3_2_mean_GE_PA]=imHistogram(cmrr_3_2_mean_GE_PA_vect,[-350,250],150);
% [H_cmrr_4_3_mean_GE_PA, range_cmrr_4_3_mean_GE_PA]=imHistogram(cmrr_4_3_mean_GE_PA_vect,[-350,250],150);
% [H_cmrr_5_4_mean_GE_PA, range_cmrr_5_4_mean_GE_PA]=imHistogram(cmrr_5_4_mean_GE_PA_vect,[-350,250],150);
% [H_cmrr_6_5_mean_GE_PA, range_cmrr_6_5_mean_GE_PA]=imHistogram(cmrr_6_5_mean_GE_PA_vect,[-350,250],150);
% 
% plot(range_cmrr_2_1_mean_GE_PA,H_cmrr_2_1_mean_GE_PA,'b','LineWidth',1.5)
% hold 
% plot(range_cmrr_3_2_mean_GE_PA,H_cmrr_3_2_mean_GE_PA,'r','LineWidth',1.5)
% plot(range_cmrr_4_3_mean_GE_PA,H_cmrr_4_3_mean_GE_PA,'g','LineWidth',1.5)
% plot(range_cmrr_5_4_mean_GE_PA,H_cmrr_5_4_mean_GE_PA,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_mean_GE_PA,H_cmrr_6_5_mean_GE_PA,'m','LineWidth',1.5)
% legend('cmrr 2 1','cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5');
% title('difference to mean GE PA');
% 
% 
% 
% %% differences with respect to GE PA 2 1
% figure
% [H_reference_GE_PA_2_1, range_reference_GE_PA_2_1]=imHistogram(reference_GE_PA_2_1_vect);
% plot(range_reference_GE_PA_2_1,H_reference_GE_PA_2_1,'k','LineWidth',1)
% title('reference GE PA 2 1');
% 
% 
% figure
% [H_GE_PA_3_2_GE_PA_2_1, range_GE_PA_3_2_GE_PA_2_1]=imHistogram(GE_PA_3_2_GE_PA_2_1_vect,[-50,50],50);
% [H_GE_PA_4_3_GE_PA_2_1, range_GE_PA_4_3_GE_PA_2_1]=imHistogram(GE_PA_4_3_GE_PA_2_1_vect,[-50,50],50);
% [H_GE_PA_5_4_GE_PA_2_1, range_GE_PA_5_4_GE_PA_2_1]=imHistogram(GE_PA_5_4_GE_PA_2_1_vect,[-50,50],50);
% [H_GE_PA_6_5_GE_PA_2_1, range_GE_PA_6_5_GE_PA_2_1]=imHistogram(GE_PA_6_5_GE_PA_2_1_vect,[-50,50],50);
% plot(range_GE_PA_3_2_GE_PA_2_1,H_GE_PA_3_2_GE_PA_2_1,'b','LineWidth',1.5)
% hold
% plot(range_GE_PA_4_3_GE_PA_2_1,H_GE_PA_4_3_GE_PA_2_1,'r','LineWidth',1.5)
% plot(range_GE_PA_5_4_GE_PA_2_1,H_GE_PA_5_4_GE_PA_2_1,'g','LineWidth',1.5)
% plot(range_GE_PA_6_5_GE_PA_2_1,H_GE_PA_6_5_GE_PA_2_1,'k','LineWidth',1.5)
% legend('GE PA 3 2','GE PA 4 3','GE PA 5 4','GE PA 6 5');
% title('difference to GE PA 2 1');
% 
% 
% figure
% [H_cmrr_2_1_GE_PA_2_1, range_cmrr_2_1_GE_PA_2_1]=imHistogram(cmrr_2_1_GE_PA_2_1_vect,[-300,200],100);
% [H_cmrr_3_2_GE_PA_2_1, range_cmrr_3_2_GE_PA_2_1]=imHistogram(cmrr_3_2_GE_PA_2_1_vect,[-300,200],100);
% [H_cmrr_4_3_GE_PA_2_1, range_cmrr_4_3_GE_PA_2_1]=imHistogram(cmrr_4_3_GE_PA_2_1_vect,[-300,200],100);
% [H_cmrr_5_4_GE_PA_2_1, range_cmrr_5_4_GE_PA_2_1]=imHistogram(cmrr_5_4_GE_PA_2_1_vect,[-300,200],100);
% [H_cmrr_6_5_GE_PA_2_1, range_cmrr_6_5_GE_PA_2_1]=imHistogram(cmrr_6_5_GE_PA_2_1_vect,[-300,200],100);
% plot(range_cmrr_2_1_GE_PA_2_1,H_cmrr_2_1_GE_PA_2_1,'b','LineWidth',1.5)
% hold 
% plot(range_cmrr_3_2_GE_PA_2_1,H_cmrr_3_2_GE_PA_2_1,'r','LineWidth',1.5)
% plot(range_cmrr_4_3_GE_PA_2_1,H_cmrr_4_3_GE_PA_2_1,'g','LineWidth',1.5)
% plot(range_cmrr_5_4_GE_PA_2_1,H_cmrr_5_4_GE_PA_2_1,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_GE_PA_2_1,H_cmrr_6_5_GE_PA_2_1,'m','LineWidth',1.5)
% 
% legend('cmrr 2 1','cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5');
% title('difference to GE PA 2 1');
% 
% 
% %% differences with respect to mean cmrr
% figure
% [H_reference_mean_cmrr,range_reference_mean_cmrr]=imHistogram(reference_mean_cmrr_vect);
% plot(range_reference_mean_cmrr,H_reference_mean_cmrr,'k','LineWidth',1)
% title('reference mean cmrr');
% 
% 
% figure
% [H_cmrr_2_1_mean_cmrr, range_cmrr_2_1_mean_cmrr]=imHistogram(cmrr_2_1_mean_cmrr_vect,[-250,200],150);
% [H_cmrr_3_2_mean_cmrr, range_cmrr_3_2_mean_cmrr]=imHistogram(cmrr_3_2_mean_cmrr_vect,[-250,200],150);
% [H_cmrr_4_3_mean_cmrr, range_cmrr_4_3_mean_cmrr]=imHistogram(cmrr_4_3_mean_cmrr_vect,[-250,200],150);
% [H_cmrr_5_4_mean_cmrr, range_cmrr_5_4_mean_cmrr]=imHistogram(cmrr_5_4_mean_cmrr_vect,[-250,200],150);
% [H_cmrr_6_5_mean_cmrr, range_cmrr_6_5_mean_cmrr]=imHistogram(cmrr_6_5_mean_cmrr_vect,[-250,200],150);
% plot(range_cmrr_2_1_mean_cmrr,H_cmrr_2_1_mean_cmrr,'b','LineWidth',1.5)
% hold
% plot(range_cmrr_3_2_mean_cmrr,H_cmrr_3_2_mean_cmrr,'r','LineWidth',1.5)
% plot(range_cmrr_4_3_mean_cmrr,H_cmrr_4_3_mean_cmrr,'g','LineWidth',1.5)
% plot(range_cmrr_5_4_mean_cmrr,H_cmrr_5_4_mean_cmrr,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_mean_cmrr,H_cmrr_6_5_mean_cmrr,'m','LineWidth',1.5)
% legend('cmrr 2 1','cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5');
% title('difference to mean cmrr');
% 
% 
% 
% %% differences with respect to cmrr 2 1
% figure
% [H_reference_cmrr_2_1, range_reference_cmrr_2_1]=imHistogram(reference_cmrr_2_1_vect);
% plot(range_reference_cmrr_2_1,H_reference_cmrr_2_1,'k','LineWidth',1)
% title('reference cmrr 2 1');
% 
% 
% figure
% [H_cmrr_3_2_cmrr_2_1, range_cmrr_3_2_cmrr_2_1]=imHistogram(cmrr_3_2_cmrr_2_1_vect,[-200,300],100);
% [H_cmrr_4_3_cmrr_2_1, range_cmrr_4_3_cmrr_2_1]=imHistogram(cmrr_4_3_cmrr_2_1_vect,[-200,300],100);
% [H_cmrr_5_4_cmrr_2_1, range_cmrr_5_4_cmrr_2_1]=imHistogram(cmrr_5_4_cmrr_2_1_vect,[-200,300],100);
% [H_cmrr_6_5_cmrr_2_1, range_cmrr_6_5_cmrr_2_1]=imHistogram(cmrr_6_5_cmrr_2_1_vect,[-200,300],100);
% plot(range_cmrr_3_2_cmrr_2_1,H_cmrr_3_2_cmrr_2_1,'b','LineWidth',1.5)
% hold
% plot(range_cmrr_4_3_cmrr_2_1,H_cmrr_4_3_cmrr_2_1,'r','LineWidth',1.5)
% plot(range_cmrr_5_4_cmrr_2_1,H_cmrr_5_4_cmrr_2_1,'g','LineWidth',1.5)
% plot(range_cmrr_6_5_cmrr_2_1,H_cmrr_6_5_cmrr_2_1,'k','LineWidth',1.5)
% legend('cmrr 3 2','cmrr 4 3','cmrr 5 4','cmrr 6 5');
% title('difference to cmrr 2 1');
% 
% 
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % 
% % clear, clc;
% % 
% % mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/eroded_GE_PA_mask.nii';
% % mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/phase_evolution_over_echoes/cmrr_PA/GE_masked_fieldmap_5_4_noTEcorr.nii';
% % 
% % readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/';
% % 
% % 
% % readfile = {
% %     'fm_diff_to_GE_PA_2_1/diff_GE_PA_3_2_to_GE_PA_2_1.nii',
% %     'fm_diff_to_GE_PA_2_1/diff_GE_PA_4_3_to_GE_PA_2_1.nii',
% %     'fm_diff_to_GE_PA_2_1/diff_GE_PA_5_4_to_GE_PA_2_1.nii',
% %     'fm_diff_to_GE_PA_2_1/diff_GE_PA_6_5_to_GE_PA_2_1.nii',
% % 
% %     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_5_4_to_GE_PA_2_1.nii',
% %     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_6_5_to_GE_PA_2_1.nii',
% % 
% %     'fm_diff_to_GE_PA_2_1/reference_fm_GE_PA_2_1.nii',
% % };
% % 
% % mask_GE_nii=load_nii(mask_dir_GE);
% % mask_epi_nii=load_nii(mask_dir_epi);
% % 
% % 
% % GE_PA_3_2_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
% GE_PA_4_3_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
% GE_PA_5_4_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
% GE_PA_6_5_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));
% 
% cmrr_5_4_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
% cmrr_6_5_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
% 
% reference_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(7))));
% 
% 
% %% GRAPH
% 
% mask_GE_1_40 = mask_GE_nii.img(:,:,8:end);
% mask_GE_vect = mask_GE_1_40(:);
% 
% mask_epi_1_36 = mask_epi_nii.img(:,:,12:end);
% mask_epi_vect = mask_epi_1_36(:);
% 
% start_GE = size(mask_GE_nii.img(:,:,1:7),1)*size(mask_GE_nii.img(:,:,1:7),2)*size(mask_GE_nii.img(:,:,1:7),3);
% start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
% 
% %start =1;
% 
% j=1;
% for i=1:size(mask_GE_vect,1)
%     if mask_GE_vect(i) == 1
%         GE_PA_3_2_GE_PA_2_1_vect(1,j) = GE_PA_3_2_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_4_3_GE_PA_2_1_vect(1,j) = GE_PA_4_3_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_5_4_GE_PA_2_1_vect(1,j) = GE_PA_5_4_GE_PA_2_1_nii.img(start_GE+i);
%         GE_PA_6_5_GE_PA_2_1_vect(1,j) = GE_PA_6_5_GE_PA_2_1_nii.img(start_GE+i);
%         
%         reference_GE_PA_2_1_vect(1,j) = reference_GE_PA_2_1_nii.img(start_GE+i);
% 
%         j=j+1;
%     end  
% end
% 
% j=1;
% for i=1:size(mask_epi_vect,1)
%     if mask_epi_vect(i) ~= 0
%         cmrr_5_4_GE_PA_2_1_vect(1,j) = cmrr_5_4_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_6_5_GE_PA_2_1_vect(1,j) = cmrr_6_5_GE_PA_2_1_nii.img(start_epi+i);
% 
%         j=j+1;
%     end  
% end
% 
% 
% 

% %% differences with respect to GE PA 2 1
% figure
% [H_reference_GE_PA_2_1, range_reference_GE_PA_2_1]=imHistogram(reference_GE_PA_2_1_vect);
% plot(range_reference_GE_PA_2_1,H_reference_GE_PA_2_1,'k','LineWidth',1)
% title('reference GE PA 2 1');
% 
% 
% figure
% [H_GE_PA_3_2_GE_PA_2_1, range_GE_PA_3_2_GE_PA_2_1]=imHistogram(GE_PA_3_2_GE_PA_2_1_vect,[-10,25],25);
% [H_GE_PA_4_3_GE_PA_2_1, range_GE_PA_4_3_GE_PA_2_1]=imHistogram(GE_PA_4_3_GE_PA_2_1_vect,[-10,25],25);
% [H_GE_PA_5_4_GE_PA_2_1, range_GE_PA_5_4_GE_PA_2_1]=imHistogram(GE_PA_5_4_GE_PA_2_1_vect,[-10,25],25);
% [H_GE_PA_6_5_GE_PA_2_1, range_GE_PA_6_5_GE_PA_2_1]=imHistogram(GE_PA_6_5_GE_PA_2_1_vect,[-10,25],25);
% plot(range_GE_PA_3_2_GE_PA_2_1,H_GE_PA_3_2_GE_PA_2_1,'b','LineWidth',1.5)
% hold
% plot(range_GE_PA_4_3_GE_PA_2_1,H_GE_PA_4_3_GE_PA_2_1,'r','LineWidth',1.5)
% plot(range_GE_PA_5_4_GE_PA_2_1,H_GE_PA_5_4_GE_PA_2_1,'g','LineWidth',1.5)
% plot(range_GE_PA_6_5_GE_PA_2_1,H_GE_PA_6_5_GE_PA_2_1,'k','LineWidth',1.5)
% legend('GE PA 3 2','GE PA 4 3','GE PA 5 4','GE PA 6 5');
% title('difference to GE PA 2 1');
% 
% 
% figure
% [H_cmrr_5_4_GE_PA_2_1, range_cmrr_5_4_GE_PA_2_1]=imHistogram(cmrr_5_4_GE_PA_2_1_vect,[-5,45],50);
% [H_cmrr_6_5_GE_PA_2_1, range_cmrr_6_5_GE_PA_2_1]=imHistogram(cmrr_6_5_GE_PA_2_1_vect,[-5,45],50);
% plot(range_cmrr_5_4_GE_PA_2_1,H_cmrr_5_4_GE_PA_2_1,'k','LineWidth',1.5)
% hold
% plot(range_cmrr_6_5_GE_PA_2_1,H_cmrr_6_5_GE_PA_2_1,'m','LineWidth',1.5)
% 
% legend('cmrr 5 4','cmrr 6 5');
% title('difference to GE PA 2 1');
% 
% 
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
% mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/eroded_GE_AP_mask.nii';
% mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/phase_evolution_over_echoes/cmrr_AP/GE_masked_fieldmap_5_4_noTEcorr.nii';
% 
% readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/';
% 
% 
% readfile = {
%     'fm_diff_to_GE_AP_2_1/diff_GE_AP_3_2_to_GE_AP_2_1.nii',
%     'fm_diff_to_GE_AP_2_1/diff_GE_AP_4_3_to_GE_AP_2_1.nii',
%     'fm_diff_to_GE_AP_2_1/diff_GE_AP_5_4_to_GE_AP_2_1.nii',
%     'fm_diff_to_GE_AP_2_1/diff_GE_AP_6_5_to_GE_AP_2_1.nii',
% 
%     'fm_diff_to_GE_AP_2_1/diff_cmrr_AP_5_4_to_GE_AP_2_1.nii',
%     'fm_diff_to_GE_AP_2_1/diff_cmrr_AP_6_5_to_GE_AP_2_1.nii',
% 
%     'fm_diff_to_GE_AP_2_1/reference_fm_GE_AP_2_1.nii',
% };
% 
% mask_GE_nii=load_nii(mask_dir_GE);
% mask_epi_nii=load_nii(mask_dir_epi);
% 
% 
% GE_AP_3_2_GE_AP_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
% GE_AP_4_3_GE_AP_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
% GE_AP_5_4_GE_AP_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
% GE_AP_6_5_GE_AP_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));
% 
% cmrr_5_4_GE_AP_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
% cmrr_6_5_GE_AP_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
% 
% reference_GE_AP_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(7))));
% 
% 
% %% GRAPH
% 
% mask_GE_1_40 = mask_GE_nii.img(:,:,8:end);
% mask_GE_vect = mask_GE_1_40(:);
% 
% mask_epi_1_36 = mask_epi_nii.img(:,:,12:end);
% mask_epi_vect = mask_epi_1_36(:);
% 
% start_GE = size(mask_GE_nii.img(:,:,1:7),1)*size(mask_GE_nii.img(:,:,1:7),2)*size(mask_GE_nii.img(:,:,1:7),3);
% start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
% 
% %start =1;
% 
% j=1;
% for i=1:size(mask_GE_vect,1)
%     if mask_GE_vect(i) == 1
%         GE_AP_3_2_GE_AP_2_1_vect(1,j) = GE_AP_3_2_GE_AP_2_1_nii.img(start_GE+i);
%         GE_AP_4_3_GE_AP_2_1_vect(1,j) = GE_AP_4_3_GE_AP_2_1_nii.img(start_GE+i);
%         GE_AP_5_4_GE_AP_2_1_vect(1,j) = GE_AP_5_4_GE_AP_2_1_nii.img(start_GE+i);
%         GE_AP_6_5_GE_AP_2_1_vect(1,j) = GE_AP_6_5_GE_AP_2_1_nii.img(start_GE+i);
%         
%         reference_GE_AP_2_1_vect(1,j) = reference_GE_AP_2_1_nii.img(start_GE+i);
% 
%         j=j+1;
%     end  
% end
% 
% j=1;
% for i=1:size(mask_epi_vect,1)
%     if mask_epi_vect(i) ~= 0
%         cmrr_5_4_GE_AP_2_1_vect(1,j) = cmrr_5_4_GE_AP_2_1_nii.img(start_epi+i);
%         cmrr_6_5_GE_AP_2_1_vect(1,j) = cmrr_6_5_GE_AP_2_1_nii.img(start_epi+i);
% 
%         j=j+1;
%     end  
% end
% 
% 
% 
% 
% %% differences with respect to GE AP 2 1
% figure
% [H_reference_GE_AP_2_1, range_reference_GE_AP_2_1]=imHistogram(reference_GE_AP_2_1_vect);
% plot(range_reference_GE_AP_2_1,H_reference_GE_AP_2_1,'k','LineWidth',1)
% title('reference GE AP 2 1');
% 
% 
% figure
% [H_GE_AP_3_2_GE_AP_2_1, range_GE_AP_3_2_GE_AP_2_1]=imHistogram(GE_AP_3_2_GE_AP_2_1_vect,[-10,25],25);
% [H_GE_AP_4_3_GE_AP_2_1, range_GE_AP_4_3_GE_AP_2_1]=imHistogram(GE_AP_4_3_GE_AP_2_1_vect,[-10,25],25);
% [H_GE_AP_5_4_GE_AP_2_1, range_GE_AP_5_4_GE_AP_2_1]=imHistogram(GE_AP_5_4_GE_AP_2_1_vect,[-10,25],25);
% [H_GE_AP_6_5_GE_AP_2_1, range_GE_AP_6_5_GE_AP_2_1]=imHistogram(GE_AP_6_5_GE_AP_2_1_vect,[-10,25],25);
% plot(range_GE_AP_3_2_GE_AP_2_1,H_GE_AP_3_2_GE_AP_2_1,'b','LineWidth',1.5)
% hold
% plot(range_GE_AP_4_3_GE_AP_2_1,H_GE_AP_4_3_GE_AP_2_1,'r','LineWidth',1.5)
% plot(range_GE_AP_5_4_GE_AP_2_1,H_GE_AP_5_4_GE_AP_2_1,'g','LineWidth',1.5)
% plot(range_GE_AP_6_5_GE_AP_2_1,H_GE_AP_6_5_GE_AP_2_1,'k','LineWidth',1.5)
% legend('GE AP 3 2','GE AP 4 3','GE AP 5 4','GE AP 6 5');
% title('difference to GE AP 2 1');
% 
% 
% figure
% [H_cmrr_5_4_GE_AP_2_1, range_cmrr_5_4_GE_AP_2_1]=imHistogram(cmrr_5_4_GE_AP_2_1_vect,[0,45],30);
% [H_cmrr_6_5_GE_AP_2_1, range_cmrr_6_5_GE_AP_2_1]=imHistogram(cmrr_6_5_GE_AP_2_1_vect,[0,45],30);
% plot(range_cmrr_5_4_GE_AP_2_1,H_cmrr_5_4_GE_AP_2_1,'k','LineWidth',1.5)
% hold
% plot(range_cmrr_6_5_GE_AP_2_1,H_cmrr_6_5_GE_AP_2_1,'m','LineWidth',1.5)
% 
% legend('cmrr 5 4','cmrr 6 5');
% title('difference to GE AP 2 1');
% 
% 
% 
% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 
% mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/eroded_GE_PA_mask.nii';
% mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/phase_evolution_over_echoes/cmrr_PA/GE_masked_fieldmap_5_4_noTEcorr.nii';
% 
% readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/';
% 
% 
% readfile = {
%     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_5_4_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_6_5_to_GE_PA_2_1.nii',
% 
%     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_5_4_TEcor_orig_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_6_5_TEcor_orig_to_GE_PA_2_1.nii',
% 
%     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_5_4_TEcor_inverted_to_GE_PA_2_1.nii',
%     'fm_diff_to_GE_PA_2_1/diff_cmrr_PA_6_5_TEcor_inverted_to_GE_PA_2_1.nii',
% };
% 
% mask_epi_nii=load_nii(mask_dir_epi);
% 
% cmrr_5_4_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
% cmrr_6_5_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
% 
% cmrr_5_4_TEcorr_orig_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
% cmrr_6_5_TEcorr_orig_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));
% 
% cmrr_5_4_TEcorr_inverted_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
% cmrr_6_5_TEcorr_inverted_GE_PA_2_1_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
% 
% %% GRAPH
% 
% mask_epi_1_36 = mask_epi_nii.img(:,:,12:end);
% mask_epi_vect = mask_epi_1_36(:);
% 
% start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
% 
% %start =1;
% 
% j=1;
% for i=1:size(mask_epi_vect,1)
%     if mask_epi_vect(i) ~= 0
%         cmrr_5_4_GE_PA_2_1_vect(1,j) = cmrr_5_4_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_6_5_GE_PA_2_1_vect(1,j) = cmrr_6_5_GE_PA_2_1_nii.img(start_epi+i);
% 
%         cmrr_5_4_TEcorr_orig_GE_PA_2_1_vect(1,j) = cmrr_5_4_TEcorr_orig_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_6_5_TEcorr_orig_GE_PA_2_1_vect(1,j) = cmrr_6_5_TEcorr_orig_GE_PA_2_1_nii.img(start_epi+i);
%         
%         cmrr_5_4_TEcorr_inverted_GE_PA_2_1_vect(1,j) = cmrr_5_4_TEcorr_inverted_GE_PA_2_1_nii.img(start_epi+i);
%         cmrr_6_5_TEcorr_inverted_GE_PA_2_1_vect(1,j) = cmrr_6_5_TEcorr_inverted_GE_PA_2_1_nii.img(start_epi+i);
%         j=j+1;
%     end  
% end
% 
% 
% 
% 
% %% differences with respect to GE PA 2 1
% 
% figure
% [H_cmrr_5_4_GE_PA_2_1, range_cmrr_5_4_GE_PA_2_1]=imHistogram(cmrr_5_4_GE_PA_2_1_vect,[-10,50],60);
% [H_cmrr_6_5_GE_PA_2_1, range_cmrr_6_5_GE_PA_2_1]=imHistogram(cmrr_6_5_GE_PA_2_1_vect,[-10,50],60);
% [H_cmrr_5_4_TEcorr_orig_GE_PA_2_1, range_cmrr_5_4_TEcorr_orig_GE_PA_2_1]=imHistogram(cmrr_5_4_TEcorr_orig_GE_PA_2_1_vect,[-10,50],60);
% [H_cmrr_6_5_TEcorr_orig_GE_PA_2_1, range_cmrr_6_5_TEcorr_orig_GE_PA_2_1]=imHistogram(cmrr_6_5_TEcorr_orig_GE_PA_2_1_vect,[-10,50],60);
% [H_cmrr_5_4_TEcorr_inverted_GE_PA_2_1, range_cmrr_5_4_TEcorr_inverted_GE_PA_2_1]=imHistogram(cmrr_5_4_TEcorr_inverted_GE_PA_2_1_vect,[-10,50],60);
% [H_cmrr_6_5_TEcorr_inverted_GE_PA_2_1, range_cmrr_6_5_TEcorr_inverted_GE_PA_2_1]=imHistogram(cmrr_6_5_TEcorr_inverted_GE_PA_2_1_vect,[-10,50],60);
% 
% plot(range_cmrr_5_4_GE_PA_2_1,H_cmrr_5_4_GE_PA_2_1,'b','LineWidth',1.5)
% hold
% plot(range_cmrr_6_5_GE_PA_2_1,H_cmrr_6_5_GE_PA_2_1,'r','LineWidth',1.5)
% plot(range_cmrr_5_4_TEcorr_orig_GE_PA_2_1,H_cmrr_5_4_TEcorr_orig_GE_PA_2_1,'g','LineWidth',1.5)
% plot(range_cmrr_6_5_TEcorr_orig_GE_PA_2_1,H_cmrr_6_5_TEcorr_orig_GE_PA_2_1,'c','LineWidth',1.5)
% plot(range_cmrr_5_4_TEcorr_inverted_GE_PA_2_1,H_cmrr_5_4_TEcorr_inverted_GE_PA_2_1,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_TEcorr_inverted_GE_PA_2_1,H_cmrr_6_5_TEcorr_inverted_GE_PA_2_1,'m','LineWidth',1.5)
% 
% legend('cmrr 5 4 no corr','cmrr 6 5 no corr','cmrr 5 4 TEorig corr','cmrr 6 5 TEorig corr', 'cmrr 5 4 TEinv corr','cmrr 6 5 TEinv corr');
% title('difference to GE PA 2 1');
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
% mask_dir_GE = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/eroded_GE_PA_mask.nii';
% mask_dir_epi = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/phase_evolution_over_echoes/cmrr_PA/GE_masked_fieldmap_5_4_noTEcorr.nii';
% 
% readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/p_bb_20171029/';
% 
% 
% readfile = {
%     'fm_diff_to_GE_PA_5_4/diff_cmrr_5_4_to_GE_PA_5_4.nii',
%     'fm_diff_to_GE_PA_5_4/diff_cmrr_6_5_to_GE_PA_5_4.nii',
% 
%     'fm_diff_to_GE_PA_5_4/diff_cmrr_5_4_TEcor_orig_to_GE_PA_5_4.nii',
%     'fm_diff_to_GE_PA_5_4/diff_cmrr_6_5_TEcor_orig_to_GE_PA_5_4.nii',
% 
%     'fm_diff_to_GE_PA_5_4/diff_cmrr_5_4_TEcor_inverted_to_GE_PA_5_4.nii',
%     'fm_diff_to_GE_PA_5_4/diff_cmrr_6_5_TEcor_inverted_to_GE_PA_5_4.nii',
% };
% 
% mask_epi_nii=load_nii(mask_dir_epi);
% 
% cmrr_5_4_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
% cmrr_6_5_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
% 
% cmrr_5_4_TEcorr_orig_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));
% cmrr_6_5_TEcorr_orig_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(4))));
% 
% cmrr_5_4_TEcorr_inverted_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(5))));
% cmrr_6_5_TEcorr_inverted_GE_PA_5_4_nii=load_nii(fullfile(readfile_dir,char(readfile(6))));
% 
% %% GRAPH
% 
% mask_epi_1_36 = mask_epi_nii.img(:,:,12:end);
% mask_epi_vect = mask_epi_1_36(:);
% 
% start_epi = size(mask_epi_nii.img(:,:,1:11),1)*size(mask_epi_nii.img(:,:,1:11),2)*size(mask_epi_nii.img(:,:,1:11),3);
% 
% %start =1;
% 
% j=1;
% for i=1:size(mask_epi_vect,1)
%     if mask_epi_vect(i) ~= 0
%         cmrr_5_4_GE_PA_5_4_vect(1,j) = cmrr_5_4_GE_PA_5_4_nii.img(start_epi+i);
%         cmrr_6_5_GE_PA_5_4_vect(1,j) = cmrr_6_5_GE_PA_5_4_nii.img(start_epi+i);
% 
%         cmrr_5_4_TEcorr_orig_GE_PA_5_4_vect(1,j) = cmrr_5_4_TEcorr_orig_GE_PA_5_4_nii.img(start_epi+i);
%         cmrr_6_5_TEcorr_orig_GE_PA_5_4_vect(1,j) = cmrr_6_5_TEcorr_orig_GE_PA_5_4_nii.img(start_epi+i);
%         
%         cmrr_5_4_TEcorr_inverted_GE_PA_5_4_vect(1,j) = cmrr_5_4_TEcorr_inverted_GE_PA_5_4_nii.img(start_epi+i);
%         cmrr_6_5_TEcorr_inverted_GE_PA_5_4_vect(1,j) = cmrr_6_5_TEcorr_inverted_GE_PA_5_4_nii.img(start_epi+i);
%         j=j+1;
%     end  
% end
% 
% 
% 
% 
% %% differences with respect to GE PA 5 4
% 
% figure
% [H_cmrr_5_4_GE_PA_5_4, range_cmrr_5_4_GE_PA_5_4]=imHistogram(cmrr_5_4_GE_PA_5_4_vect,[-30,50],60);
% [H_cmrr_6_5_GE_PA_5_4, range_cmrr_6_5_GE_PA_5_4]=imHistogram(cmrr_6_5_GE_PA_5_4_vect,[-30,50],60);
% [H_cmrr_5_4_TEcorr_orig_GE_PA_5_4, range_cmrr_5_4_TEcorr_orig_GE_PA_5_4]=imHistogram(cmrr_5_4_TEcorr_orig_GE_PA_5_4_vect,[-30,50],60);
% [H_cmrr_6_5_TEcorr_orig_GE_PA_5_4, range_cmrr_6_5_TEcorr_orig_GE_PA_5_4]=imHistogram(cmrr_6_5_TEcorr_orig_GE_PA_5_4_vect,[-30,50],60);
% [H_cmrr_5_4_TEcorr_inverted_GE_PA_5_4, range_cmrr_5_4_TEcorr_inverted_GE_PA_5_4]=imHistogram(cmrr_5_4_TEcorr_inverted_GE_PA_5_4_vect,[-30,50],60);
% [H_cmrr_6_5_TEcorr_inverted_GE_PA_5_4, range_cmrr_6_5_TEcorr_inverted_GE_PA_5_4]=imHistogram(cmrr_6_5_TEcorr_inverted_GE_PA_5_4_vect,[-30,50],60);
% 
% plot(range_cmrr_5_4_GE_PA_5_4,H_cmrr_5_4_GE_PA_5_4,'b','LineWidth',1.5)
% hold
% plot(range_cmrr_6_5_GE_PA_5_4,H_cmrr_6_5_GE_PA_5_4,'r','LineWidth',1.5)
% plot(range_cmrr_5_4_TEcorr_orig_GE_PA_5_4,H_cmrr_5_4_TEcorr_orig_GE_PA_5_4,'g','LineWidth',1.5)
% plot(range_cmrr_6_5_TEcorr_orig_GE_PA_5_4,H_cmrr_6_5_TEcorr_orig_GE_PA_5_4,'c','LineWidth',1.5)
% plot(range_cmrr_5_4_TEcorr_inverted_GE_PA_5_4,H_cmrr_5_4_TEcorr_inverted_GE_PA_5_4,'k','LineWidth',1.5)
% plot(range_cmrr_6_5_TEcorr_inverted_GE_PA_5_4,H_cmrr_6_5_TEcorr_inverted_GE_PA_5_4,'m','LineWidth',1.5)
% 
% legend('cmrr 5 4 no corr','cmrr 6 5 no corr','cmrr 5 4 TEorig corr','cmrr 6 5 TEorig corr', 'cmrr 5 4 TEinv corr','cmrr 6 5 TEinv corr');
% title('difference to GE PA 5 4');
% 
% 
% 







mask_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/GE_464x464x144/hip/mag/m_hip_2-1.nii';
readfile_dir = '/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/abstract_ismrm2018/19900813KREK_201710261855/GE_464x464x144';


readfile = {QSM_roemer_1_diff_to_hip_2_1.nii
    'diff_to_hip_2_1/QSM_aspire_1_diff_to_hip_2_1.nii',
    'diff_to_hip_2_1/QSM_vrc_online_1_diff_to_hip_2_1.nii',
    'diff_to_hip_2_1/QSM_roemer_1_diff_to_hip_2_1.nii',
};

mask_nii=load_nii(mask_dir);
aspire_nii=load_nii(fullfile(readfile_dir,char(readfile(1))));
vrc_nii=load_nii(fullfile(readfile_dir,char(readfile(2))));
roemer_nii=load_nii(fullfile(readfile_dir,char(readfile(3))));

%% GRAPH
%
mask = mask_nii.img(:,:,:);
mask_vect = mask(:);

j=1;
for i=1:size(mask_vect,1)
    if mask_vect(i) ~= 0
        aspire(1,j) = aspire_nii.img(i);
        vrc(1,j) = vrc_nii.img(i);
        roemer(1,j) = roemer_nii.img(i);
        j=j+1;
    end  
end

figure;
% [H_aspire, range_aspire]=imHistogram(aspire,[-0.01,0.01],100);
% [H_vrc, range_vrc]=imHistogram(vrc,[-0.01,0.01],100);
% [H_roemer, range_roemer]=imHistogram(roemer,[-0.01,0.01],100);
[H_aspire, range_aspire]=imHistogram(aspire,[-3.2,3.2],100);
[H_vrc, range_vrc]=imHistogram(vrc,[-3.2,3.2],100);
[H_roemer, range_roemer]=imHistogram(roemer,[-3.2,3.2],100);
plot(range_aspire,H_aspire,'b','LineWidth',1.5)
hold
plot(range_vrc,H_vrc,'r','LineWidth',1.5)
plot(range_roemer,H_roemer,'g','LineWidth',1.5)
title('QSM differences to HiP');
legend('aspire','vrc','roemer');

