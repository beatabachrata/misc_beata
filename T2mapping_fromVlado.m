% T2 mapping from nii files (from Vlado Juras)

folderFrom = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19950915AEL_20201029_tumorProtTest/caipirinha/sag_iPat3_pf78_5randomEchoes/';
uEchoTimes = [4 8 12 16 20]; % TEs, in ms
folderTo = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19950915AEL_20201029_tumorProtTest/t2star/sag_iPat3_5f78_5randomEchoes/';
fat = true;


%% 
if (~exist(folderTo,'dir'))
    mkdir(folderTo);
end

if (fat)
    loadfile = fullfile(folderFrom,'fat_type2Corr_type1Corr_T1Corr.nii');
    writefile = fullfile(folderTo,'t2Map_fat_0to30.nii');
    writefilesmooth = fullfile(folderTo,'t2MapSmooth_fat_0to30.nii'); 
    T2min = 0;
    T2max = 30;

else
    loadfile = fullfile(folderFrom,'water.nii');
    writefile = fullfile(folderTo,'t2Map_water_0to100.nii');
    writefilesmooth = fullfile(folderTo,'t2MapSmooth_water_0to100.nii');
    T2min = 0;
    T2max = 100;

end

nii2 = load_untouch_nii(loadfile);
data = nii2.img;

disp(['Loaded data of size ' int2str(size(data))]);

dimx = size(data,1);
dimy = size(data,2);
nrSlices = size(data,3);
nrEchoes = size(data,4);
uEchoTimes = uEchoTimes';

T2maps = zeros(dimx, dimy, nrSlices);
disp('Calculating T2 maps...');
 
%https://stackoverflow.com/questions/51289226/best-approach-to-speed-up-pixel-processing-in-matlab


data = smoothn(data,0.2);
save_nii(make_nii(data), writefilesmooth);


for S = 1:nrSlices

    slice = squeeze(data(:,:,S,:));
    sliceR = log(slice);
    sliceR = reshape(sliceR, [], size(uEchoTimes,1))'; % reshape and transpose to 16 by N
    X = uEchoTimes(:);
    X = X - mean(X);
    b_vectorized = [ones(size(uEchoTimes,1),1) X] \ sliceR; % solve all once       

    bv1 = b_vectorized(2,:);
    T2values = -1./b_vectorized(2,:);
    T2values(isnan(T2values)) = 0;
    T2values(isinf(T2values)) = 0;
    T2values(T2values<0) = T2max;
    T2mapVec = reshape(T2values,dimx,dimy); 
    T2maps(:,:,S) = T2mapVec; 

    %             for X=1:dimx
    %                 if (mod(X,10)==0)
    %                     disp(['Patient: ' int2str(F) ' of ' int2str(size(T2mfolders,1)) ' Slice ' int2str(C) ' , Line ' int2str(X) ' of ' int2str(dimx)]);
    %                 end
    %                 for Y=1:dimy
    %                     if (dicoms(X,Y,1)>20.0)  
    %                     x = uEchoTimes;
    %                     y = (squeeze(dicoms(X,Y,:)));
    %         %             f = @(b,x) b(1).*exp(-x./b(2)) + b(3);          % Objective Function (exponential)
    %                     f = @(b,x) b(1).*exp(-x./b(2));          % Objective Function (exponential)
    %         %             f = @(b,x) log(b(1)) -x./b(2);     % linear
    %                     options = optimset('MaxFunEvals',1000, 'display','off');
    %                     B = fminsearch(@(b) norm(y - f(b,x)), [x(1); 50.0], options)  ;                % Estimate Parameters
    %         %             B = fminsearch(@(b) norm(y - f(b,x)), [x(1); 50.0; 10.0], options)  ;                % Estimate Parameters
    % 
    %         %             fitobject = fit(x,y,'linearinterp');
    % 
    %                     T2maps(X,Y,C) = B(2);
    %         %             plot(x, y, 'pg')
    %         %             hold on
    %         %             plot(x, f(B,x), '-r')
    %         %             hold off
    %                     end
    %                 end
    %             end
            %     imshow(T2maps(:,:,C),[0 100]); title(['slice: ' int2str(C)])
end
    
T2maps(T2maps>T2max) = T2max;
T2maps(T2maps<T2min) = T2min;
save_nii(make_nii(T2maps), writefile);


T2maps_smooth = smoothn(T2maps,0.1);
save_nii(make_nii(T2maps_smooth), writefilesmooth);
% 






