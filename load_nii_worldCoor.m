function nii = load_nii_worldCoor(name, orient)

nii = load_nii(name);

switch orient
    case 'sagittal_PA'
        nii.img = permute(nii.img,[2,3,1,4]);        
        nii.img = flip(flip(nii.img,1),3);
        nii.hdr.dime.dim(2:4) = [nii.hdr.dime.dim(3),nii.hdr.dime.dim(4),nii.hdr.dime.dim(2)];
        nii.hdr.dime.pixdim(2:4) = [nii.hdr.dime.pixdim(3),nii.hdr.dime.pixdim(4),nii.hdr.dime.pixdim(2)];
        nii.hdr.hist.originator(1:3) = [nii.hdr.hist.originator(2),nii.hdr.hist.originator(3),nii.hdr.hist.originator(1)];

    case 'coronal_HF'
        nii.img = permute(nii.img,[1,3,2,4]);
        nii.img = flip(flip(nii.img,1),3);
        nii.hdr.dime.dim(2:4) = [nii.hdr.dime.dim(2),nii.hdr.dime.dim(4),nii.hdr.dime.dim(3)];
        nii.hdr.dime.pixdim(2:4) = [nii.hdr.dime.pixdim(2),nii.hdr.dime.pixdim(4),nii.hdr.dime.pixdim(3)];
        nii.hdr.hist.originator(1:3) = [nii.hdr.hist.originator(1),nii.hdr.hist.originator(3),nii.hdr.hist.originator(2)];

    case 'transversal_PA'
        nii.img = flip(nii.img,1);
        
    case 'transversal_AP'
        nii.img = flip(nii.img,1);
        
    case 'sagittal_AP'
        nii.img = permute(nii.img,[2,3,1,4]);        
        nii.img = flip(flip(nii.img,1),3);
        nii.hdr.dime.dim(2:4) = [nii.hdr.dime.dim(3),nii.hdr.dime.dim(4),nii.hdr.dime.dim(2)];
        nii.hdr.dime.pixdim(2:4) = [nii.hdr.dime.pixdim(3),nii.hdr.dime.pixdim(4),nii.hdr.dime.pixdim(2)];
        nii.hdr.hist.originator(1:3) = [nii.hdr.hist.originator(2),nii.hdr.hist.originator(3),nii.hdr.hist.originator(1)];

    case 'coronal_FH'
        nii.img = permute(nii.img,[1,3,2,4]);
        nii.img = flip(flip(nii.img,1),3);
        nii.hdr.dime.dim(2:4) = [nii.hdr.dime.dim(2),nii.hdr.dime.dim(4),nii.hdr.dime.dim(3)];
        nii.hdr.dime.pixdim(2:4) = [nii.hdr.dime.pixdim(2),nii.hdr.dime.pixdim(4),nii.hdr.dime.pixdim(3)];
        nii.hdr.hist.originator(1:3) = [nii.hdr.hist.originator(1),nii.hdr.hist.originator(3),nii.hdr.hist.originator(2)];


end

end
