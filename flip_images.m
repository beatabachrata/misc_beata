clear, clc


main_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/nifti_and_ima/SMURFImagesToShare';

for iImage = 15:16
    
    switch iImage
            case 1
                subdir = '';
                image_name = 'breasts_V1_SMURF_fat.nii';
                flip_dims = [3];
            case 2
                subdir = '';
                image_name = 'breasts_V1_SMURF_recombined.nii';
                flip_dims = [3];
            case 3
                subdir = '';
                image_name = 'breasts_V1_SMURF_water.nii';
                flip_dims = [3];
            case 4
                subdir = '';
                image_name = 'breasts_V8_SMURF_fat.nii';    
                flip_dims = [3];
            case 5
                subdir = '';
                image_name = 'breasts_V8_SMURF_recombined.nii';
                flip_dims = [3];
            case 6
                subdir = '';
                image_name = 'breasts_V8_SMURF_water.nii';
                flip_dims = [3];
            case 7
                subdir = '';
                image_name = 'knee_V1_SMURF_fat.nii';
                flip_dims = [3];
            case 8
                subdir = '';
                image_name = 'knee_V1_SMURF_recombined.nii'; 
                flip_dims = [3];
            case 9
                subdir = '';
                image_name = 'knee_V1_SMURF_water.nii'; 
                flip_dims = [3];
            case 10
                subdir = '';
                image_name = 'abdomen_V6_6ptDixon_mag.nii';
                flip_dims = [1,2];
            case 11
                subdir = '';
                image_name = 'abdomen_V6_6ptDixon_phase.nii'; 
                flip_dims = [1,2];
            case 12
                subdir = '';
                image_name = 'abdomen_V6_SMURF_aliased.nii'; 
                flip_dims = [1,2];    
            case 13
                subdir = '';
                image_name = 'abdomen_V6_SMURF_fat.nii'; 
                flip_dims = [1,2];
            case 14
                subdir = '';
                image_name = 'abdomen_V6_SMURF_water.nii'; 
                flip_dims = [1,2];    
            case 15
                subdir = '';
                image_name = 'abdomen_V6_6ptDixon_slc2_water.nii'; 
                flip_dims = [1,2];
            case 16
                subdir = '';
                image_name = 'abdomen_V6_6ptDixon_slc2_fat.nii'; 
                flip_dims = [1,2];    
    end
    
    filename = fullfile(main_dir, subdir, image_name);
    image_nii = load_nii(filename);
    image = image_nii.img;
    
    for i = 1:length(flip_dims)
        if (flip_dims(i) == 1)
            image = flipdim(image,1);
        elseif (flip_dims(i) == 2)
            image = flipdim(image,2);            
        elseif (flip_dims(i) == 3)
            image = flipdim(image,3);            
        elseif (flip_dims(i) == 4)
            image = flipdim(image,4);
        elseif (flip_dims(i) == 5)
            image = flipdim(image,5);            
        end
    end
    
    save_nii(make_nii(image),filename)
    
end