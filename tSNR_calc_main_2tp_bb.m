% SNR_calc_main.m
% Simon Robinson. 31.10.2006
% Calculating Image SNR on the basis of ROIs, which can be defined and saved here
% and/or Time-Series SNR, which is done on a pixel-by-pixel basis
% Calculation done in calc_tSNR_func

clear, clc
warning off MATLAB:divideByZero


main_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19790301PDCR_20200609_abdomenSNR/';

    
for variant = 1:24
switch variant
    case 1
        data_subdir = {'caipirinha/scan1/',
                       'caipirinha/scan2/'};
        write_subdir = 'caipirinha/scans12/';
        image_name = 'water.nii';
    case 2
        data_subdir = {'caipirinha/scan3/',
                       'caipirinha/scan4/'};
        write_subdir = 'caipirinha/scans34/';
        image_name = 'water.nii';
    case 3
        data_subdir = {'caipirinha/scan5/',
                       'caipirinha/scan6/'};
        write_subdir = 'caipirinha/scans56/';
        image_name = 'water.nii';
    case 4
        data_subdir = {'caipirinha/scan7/',
                       'caipirinha/scan8/'};
        write_subdir = 'caipirinha/scans78/';
        image_name = 'water.nii';

    case 5
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan1/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan2/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans12/';
        image_name = 'water_Hernando.nii';
    case 6
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan3/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan4/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans34/';                
        image_name = 'water_Hernando.nii';
    case 7
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan5/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan6/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans56/';
        image_name = 'water_Hernando.nii';
    case 8
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan7/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan8/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans78/';
        image_name = 'water_Hernando.nii';

    case 9
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan1/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan2/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans12/';
        image_name = 'water_Hernando.nii';
    case 10
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan3/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan4/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans34/';                
        image_name = 'water_Hernando.nii';
    case 11
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan5/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan6/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans56/';
        image_name = 'water_Hernando.nii';
    case 12
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan7/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan8/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans78/';
        image_name = 'water_Hernando.nii';


    case 13
        data_subdir = {'caipirinha/scan1/',
                       'caipirinha/scan2/'};
        write_subdir = 'caipirinha/scans12/';
        image_name = 'fat_chemShiftCorr.nii';
    case 14
        data_subdir = {'caipirinha/scan3/',
                       'caipirinha/scan4/'};
        write_subdir = 'caipirinha/scans34/';
        image_name = 'fat_chemShiftCorr.nii';
    case 15
        data_subdir = {'caipirinha/scan5/',
                       'caipirinha/scan6/'};
        write_subdir = 'caipirinha/scans56/';
        image_name = 'fat_chemShiftCorr.nii';
    case 16
        data_subdir = {'caipirinha/scan7/',
                       'caipirinha/scan8/'};
        write_subdir = 'caipirinha/scans78/';
        image_name = 'fat_chemShiftCorr.nii';

    case 17
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan1/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan2/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans12/';
        image_name = 'fat_Hernando.nii';
    case 18
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan3/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan4/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans34/';                
        image_name = 'fat_Hernando.nii';
    case 19
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan5/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan6/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans56/';
        image_name = 'fat_Hernando.nii';
    case 20
        data_subdir = {'dixon_longTR_new/3pt_2D_removeAspirePO/scan7/',
                       'dixon_longTR_new/3pt_2D_removeAspirePO/scan8/'};
        write_subdir = 'dixon_longTR_new/3pt_2D_removeAspirePO/scans78/';
        image_name = 'fat_Hernando.nii';

    case 21
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan1/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan2/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans12/';
        image_name = 'fat_Hernando.nii';
    case 22
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan3/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan4/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans34/';                
        image_name = 'fat_Hernando.nii';
    case 23
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan5/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan6/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans56/';
        image_name = 'fat_Hernando.nii';
    case 24
        data_subdir = {'dixon_shortTR_new/3pt_2D_removeAspirePO/scan7/',
                       'dixon_shortTR_new/3pt_2D_removeAspirePO/scan8/'};
        write_subdir = 'dixon_shortTR_new/3pt_2D_removeAspirePO/scans78/';
        image_name = 'fat_Hernando.nii';

end

    roi_names = {};
    for iROI = 1:20
        if (variant <= 12) 
            roi_names{iROI} = sprintf('water_roi%i.nii',iROI);
        else
            roi_names{iROI} = sprintf('fat_roi%i.nii',iROI);
        end
    end

    %%
    write_dir = fullfile(main_dir,write_subdir);

    %% make results directory
    s = warning ('query', 'MATLAB:MKDIR:DirectoryExists') ;	    % get current state
    warning ('off', 'MATLAB:MKDIR:DirectoryExists') ;
    mkdir(write_dir);
    warning(s) ;						    % restore state


    %%
    clear image image_4D inROI_mean inROI_diff

    for scan = 1:length(data_subdir)
        readfile = fullfile(main_dir, data_subdir{scan}, image_name);
        image_nii = load_nii(readfile);
        image = image_nii.img;
        image_4D(:,:,:,scan) = image;
    end

    %calculate mean and difference image over the 2 acquisitions
    mean_ima = sum(image_4D, 4)./2;
    diff_ima = squeeze(image_4D(:,:,:,1) - image_4D(:,:,:,2));

    save_nii(make_nii(mean_ima),fullfile(write_dir,sprintf('mean_%s',image_name)))
    save_nii(make_nii(diff_ima),fullfile(write_dir,sprintf('diff_%s',image_name)))
    
    for lroi = 1:length(roi_names)
        roifile = fullfile(main_dir,'SNR/rois/',roi_names{lroi});
        roi_nii = load_nii(roifile);
        roi = roi_nii.img;

        clear inROI_mean inROI_diff
        j = 1;
        for i = 1:length(roi(:))
            if (roi(i) ~= 0)
                inROI_mean(j) = mean_ima(i);
                inROI_diff(j) = diff_ima(i);
                j = j+1;
            end
        end

        signal(variant,lroi) = mean(inROI_mean);
        noise(variant,lroi) = std(inROI_diff)/sqrt(2);
    end
        
    diff_ima_all(:,:,:,variant) = diff_ima;
     
end
     

SNR_ratio = signal./noise;
dlmwrite(sprintf(fullfile(main_dir,sprintf('SNR_table_fromMatlab_20200701_subsample4'))),SNR_ratio,'\t');

SNR_ratio = reshape(SNR_ratio,[120,4]);

diff_ima_SMURF_water = mean(abs(diff_ima_all(:,:,:,1:4)),4)*6*1e8;
diff_ima_longTR_water = mean(abs(diff_ima_all(:,:,:,5:8)),4);
diff_ima_shortTR_water = mean(abs(diff_ima_all(:,:,:,9:12)),4);
diff_ima_SMURF_fat = mean(abs(diff_ima_all(:,:,:,13:16)),4)*6*1e8;
diff_ima_longTR_fat = mean(abs(diff_ima_all(:,:,:,17:20)),4);
diff_ima_shortTR_fat = mean(abs(diff_ima_all(:,:,:,21:24)),4);

save_nii(make_nii(diff_ima_SMURF_water),fullfile(main_dir,'SNR','meanDiff_SMURF_water.nii'))
save_nii(make_nii(diff_ima_longTR_water),fullfile(main_dir,'SNR','meanDiff_Dixon_longTR_water.nii'))
save_nii(make_nii(diff_ima_shortTR_water),fullfile(main_dir,'SNR','meanDiff_Dixon_shortTR_water.nii'))
save_nii(make_nii(diff_ima_SMURF_fat),fullfile(main_dir,'SNR','meanDiff_SMURF_fat.nii'))
save_nii(make_nii(diff_ima_longTR_fat),fullfile(main_dir,'SNR','meanDiff_Dixon_longTR_fat.nii'))
save_nii(make_nii(diff_ima_shortTR_fat),fullfile(main_dir,'SNR','meanDiff_Dixon_shortTR_fat.nii'))

diff_ima_water = (diff_ima_SMURF_water+diff_ima_longTR_water+diff_ima_shortTR_water)/3;
diff_ima_fat = (diff_ima_SMURF_fat+diff_ima_longTR_fat+diff_ima_shortTR_fat)/3;

save_nii(make_nii(diff_ima_water),fullfile(main_dir,'SNR','meanDiff_all_water.nii'))
save_nii(make_nii(diff_ima_fat),fullfile(main_dir,'SNR','meanDiff_all_fat.nii'))




%% Calculate SNR and SNR efficiency
order_array = [1:4,25:28,49:52,73:76,97:100];

% % check for nor normally distributed values
% for iRow = 1:4
%     for iOffset = 0:4:20
%         [is,p]=swtest(SNR_ratio(order_array+iOffset,iRow));
%         if is == 1
%             iRow
%             iOffset
%             p
%             figure
%             hist(SNR_ratio(order_array+iOffset,iRow))
%             SNR_ratio(order_array+iOffset,iRow)
%         end
%     end
% end
        
% SNR in water
median_SNR_SMURF_water = median(SNR_ratio(order_array,:),1);
median_SNR_longTR_water = median(SNR_ratio(order_array+4,:),1);
median_SNR_shortTR_water = median(SNR_ratio(order_array+8,:),1);
median_SNR_water = [median_SNR_SMURF_water.',median_SNR_longTR_water.',median_SNR_shortTR_water.'];
MAD_SNR_SMURF_water = mad(SNR_ratio(order_array,:),0,1);
MAD_SNR_longTR_water = mad(SNR_ratio(order_array+4,:),0,1);
MAD_SNR_shortTR_water = mad(SNR_ratio(order_array+8,:),0,1);
MAD_SNR_water = [MAD_SNR_SMURF_water.',MAD_SNR_longTR_water.',MAD_SNR_shortTR_water.'];

for i = 1:size(SNR_ratio(order_array,:),2)
%     h_ttest_SNR_water_SMURF_longTR(i) = ttest2(SNR_ratio(order_array,i),SNR_ratio(order_array+4,i));
%     h_ttest_SNR_water_SMURF_shortTR(i) = ttest2(SNR_ratio(order_array,i),SNR_ratio(order_array+8,i));
% %     h_ttest_SNR_water_longTR_shortTR(i) = ttest2(SNR_ratio(order_array+4,i),SNR_ratio(order_array+8,i));
    [p_SNR_water_SMURF_longTR(i),h_SNR_water_SMURF_longTR(i)] = ranksum(SNR_ratio(order_array,i),SNR_ratio(order_array+4,i));
    [p_SNR_water_SMURF_shortTR(i),h_SNR_water_SMURF_shortTR(i)] = ranksum(SNR_ratio(order_array,i),SNR_ratio(order_array+8,i));
%     [p3(i),h_SNR_water_longTR_shortTR(i)] = ranksum(SNR_ratio(order_array+4,i),SNR_ratio(order_array+8,i));
%     p_kw_SNR_water(i) = kruskalwallis([SNR_ratio(order_array,i),SNR_ratio(order_array+4,i),SNR_ratio(order_array+8,i)]);

end

% SNR in fat
median_SNR_SMURF_fat = median(SNR_ratio(order_array+12,:),1);
median_SNR_longTR_fat = median(SNR_ratio(order_array+16,:),1);
median_SNR_shortTR_fat = median(SNR_ratio(order_array+20,:),1);
median_SNR_fat = [median_SNR_SMURF_fat.',median_SNR_longTR_fat.',median_SNR_shortTR_fat.'];
MAD_SNR_SMURF_fat = mad(SNR_ratio(order_array+12,:),0,1);
MAD_SNR_longTR_fat = mad(SNR_ratio(order_array+16,:),0,1);
MAD_SNR_shortTR_fat = mad(SNR_ratio(order_array+20,:),0,1);
MAD_SNR_fat = [MAD_SNR_SMURF_fat.',MAD_SNR_longTR_fat.',MAD_SNR_shortTR_fat.'];

for i = 1:size(SNR_ratio(order_array,:),2)
    [p_SNR_fat_SMURF_longTR(i),h_SNR_fat_SMURF_longTR(i)] = ranksum(SNR_ratio(order_array+12,i),SNR_ratio(order_array+16,i));
    [p_SNR_fat_SMURF_shortTR(i),h_SNR_fat_SMURF_shortTR(i)] = ranksum(SNR_ratio(order_array+12,i),SNR_ratio(order_array+20,i));
end

% SNR efficiency in water
median_SNR_eff_SMURF_water = median(SNR_ratio(order_array,:)/sqrt(26.29),1);
median_SNR_eff_longTR_water = median(SNR_ratio(order_array+4,:)/sqrt(26.29),1);
median_SNR_eff_shortTR_water = median(SNR_ratio(order_array+8,:)/sqrt(16.01),1);
median_SNR_eff_water = [median_SNR_eff_SMURF_water.',median_SNR_eff_longTR_water.',median_SNR_eff_shortTR_water.'];
MAD_SNR_eff_SMURF_water = mad(SNR_ratio(order_array,:)/sqrt(26.29),0,1);
MAD_SNR_eff_longTR_water = mad(SNR_ratio(order_array+4,:)/sqrt(26.29),0,1);
MAD_SNR_eff_shortTR_water = mad(SNR_ratio(order_array+8,:)/sqrt(16.01),0,1);
MAD_SNR_eff_water = [MAD_SNR_eff_SMURF_water.',MAD_SNR_eff_longTR_water.',MAD_SNR_eff_shortTR_water.'];

for i = 1:size(SNR_ratio(order_array,:),2)
    [p_SNR_eff_water_SMURF_longTR(i),h_SNR_eff_water_SMURF_longTR(i)] = ranksum((SNR_ratio(order_array,i)/sqrt(26.29)),(SNR_ratio(order_array+4,i)/sqrt(26.29)));
    [p_SNR_eff_water_SMURF_shortTR(i),h_SNR_eff_water_SMURF_shortTR(i)] = ranksum((SNR_ratio(order_array,i)/sqrt(26.29)),(SNR_ratio(order_array+8,i)/sqrt(16.01)));
end

% SNR efficiency in fat
median_SNR_eff_SMURF_fat = median(SNR_ratio(order_array+12,:)/sqrt(26.29),1);
median_SNR_eff_longTR_fat = median(SNR_ratio(order_array+16,:)/sqrt(26.29),1);
median_SNR_eff_shortTR_fat = median(SNR_ratio(order_array+20,:)/sqrt(16.01),1);
median_SNR_eff_fat = [median_SNR_eff_SMURF_fat.',median_SNR_eff_longTR_fat.',median_SNR_eff_shortTR_fat.'];
MAD_SNR_eff_SMURF_fat = mad(SNR_ratio(order_array+12,:)/sqrt(26.29),0,1);
MAD_SNR_eff_longTR_fat = mad(SNR_ratio(order_array+16,:)/sqrt(26.29),0,1);
MAD_SNR_eff_shortTR_fat = mad(SNR_ratio(order_array+20,:)/sqrt(16.01),0,1);
MAD_SNR_eff_fat = [MAD_SNR_eff_SMURF_fat.',MAD_SNR_eff_longTR_fat.',MAD_SNR_eff_shortTR_fat.'];

for i = 1:size(SNR_ratio(order_array,:),2)
    [p_SNR_eff_fat_SMURF_longTR(i),h_SNR_eff_fat_SMURF_longTR(i)] = ranksum((SNR_ratio(order_array+12,i)/sqrt(26.29)),(SNR_ratio(order_array+16,i)/sqrt(26.29)));
    [p_SNR_eff_fat_SMURF_shortTR(i),h_SNR_eff_fat_SMURF_shortTR(i)] = ranksum((SNR_ratio(order_array+12,i)/sqrt(26.29)),(SNR_ratio(order_array+20,i)/sqrt(16.01)));
end

p_SNR_water = [zeros(size(p_SNR_water_SMURF_longTR));p_SNR_water_SMURF_longTR;p_SNR_water_SMURF_shortTR].';
p_SNR_fat = [zeros(size(p_SNR_fat_SMURF_longTR));p_SNR_fat_SMURF_longTR;p_SNR_fat_SMURF_shortTR].';
p_SNR_eff_water = [zeros(size(p_SNR_eff_water_SMURF_longTR));p_SNR_eff_water_SMURF_longTR;p_SNR_eff_water_SMURF_shortTR].';
p_SNR_eff_fat = [zeros(size(p_SNR_eff_fat_SMURF_longTR));p_SNR_eff_fat_SMURF_longTR;p_SNR_eff_fat_SMURF_shortTR].';

%% Plot
names_water = {'\fontsize{12}muscle\fontsize{9} (ROIs 1-5)','\fontsize{12}muscle\fontsize{9} (ROIs 6-10)','\fontsize{12}liver\fontsize{9} (ROIs 11-15)','\fontsize{12}kidney\fontsize{9} (ROIs 16-20)'};
names_fat = {'\fontsize{12}subcut.\fontsize{9} (ROIs 21-25)','\fontsize{12}subcut.\fontsize{9} (ROIs 26-30)','\fontsize{12}visceral\fontsize{9} (ROIs 31-35)','\fontsize{12}visceral\fontsize{9} (ROIs 36-40)'};

% names_water = {'muscle (left)','muscle (right)','liver','kidney'};
% names_fat = {'subcut. (left)','subcut. (right)','visceral (left)','visceral (right)'};

% median_SNR_water = median_SNR_water(:,1:2);
% median_SNR_fat = median_SNR_fat(:,1:2);
% median_SNR_eff_water = median_SNR_eff_water(:,1:2);
% median_SNR_eff_fat = median_SNR_eff_fat(:,1:2);
% MAD_SNR_water = MAD_SNR_water(:,1:2);
% MAD_SNR_fat = MAD_SNR_fat(:,1:2);
% MAD_SNR_eff_water = MAD_SNR_eff_water(:,1:2);
% MAD_SNR_eff_fat = MAD_SNR_eff_fat(:,1:2);
% p_SNR_water = p_SNR_water(:,1:2);
% p_SNR_fat = p_SNR_fat(:,1:2); 
% p_SNR_eff_water = p_SNR_eff_water(:,1:2); 
% p_SNR_eff_fat = p_SNR_eff_fat(:,1:2);

figure;
s11 = subplot(2,2,1);
b11 = bar(median_SNR_water);
ylabel('a.u.','FontSize',12);
ylim([0,87]);
s11.Position = s11.Position + [-0.09 0.04 0.0 0.0];
set(gca,'XTickLabel',names_water);
title('SNR of ROIs in water tissues','FontSize',14);
% title('Water','FontSize',14);

ay11 = gca;
ay11.YTick = 0:15:75;
set(ay11);
xBar11=cell2mat(get(b11,'XData')).' + [b11.XOffset];  % compute bar centers
hold on;
errorbar(xBar11,median_SNR_water,MAD_SNR_water,'k.');
for i = 1:length(p_SNR_water)
    sigstar({[xBar11(i,1),xBar11(i,2)], [xBar11(i,1),xBar11(i,3)]}, [p_SNR_water(i,2),p_SNR_water(i,3)])
%     sigstar({[xBar11(i,1),xBar11(i,2)]}, p_SNR_water(i,2))
end
legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);
% legend({'SMURF','Dixon'},'FontSize',12);


s12 = subplot(2,2,2);
b12 = bar(median_SNR_fat);
ylabel('a.u.','FontSize',12);
ylim([0,155]);
s12.Position = s12.Position + [-0.14 0.04 0.0 0.0];
set(gca,'XTickLabel',names_fat,'FontSize',12);
title('SNR of ROIs in fatty tissues','FontSize',14);
% title('Fat','FontSize',14);
ay12 = gca;
ay12.YTick = 0:30:150;
set(ay12);
xBar12=cell2mat(get(b12,'XData')).' + [b12.XOffset];  % compute bar centers
hold on;
errorbar(xBar12,median_SNR_fat,MAD_SNR_fat,'k.');
for i = 1:length(p_SNR_water)
    sigstar({[xBar12(i,1),xBar12(i,2)], [xBar12(i,1),xBar12(i,3)]}, [p_SNR_fat(i,2),p_SNR_fat(i,3)])
%       sigstar({[xBar11(i,1),xBar11(i,2)]}, p_SNR_fat(i,2))
end
legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);
% legend({'SMURF','Dixon'},'FontSize',12);

s21 = subplot(2,2,3);
b21 = bar(median_SNR_eff_water);
ylabel('a.u.','FontSize',12);
ylim([0,20]);
s21.Position = s21.Position + [-0.09 0.06 0.0 0.0];
set(gca,'XTickLabel',names_water,'FontSize',12);
title('SNR efficiency of ROIs in water tissues','FontSize',14);
ay21 = gca;
ay21.YTick = 0:3:20;
set(ay21);
xBar21=cell2mat(get(b21,'XData')).' + [b21.XOffset];  % compute bar centers
hold on;
errorbar(xBar21,median_SNR_eff_water,MAD_SNR_eff_water,'k.');
for i = 1:length(p_SNR_water)
    sigstar({[xBar21(i,1),xBar21(i,2)], [xBar21(i,1),xBar21(i,3)]}, [p_SNR_eff_water(i,2),p_SNR_eff_water(i,3)])
%     sigstar({[xBar11(i,1),xBar11(i,2)]}, p_SNR_eff_water(i,2))
end
legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);
% legend({'SMURF','Dixon'},'FontSize',12);

s22 = subplot(2,2,4);
b22 = bar(median_SNR_eff_fat);
ylabel('a.u.','FontSize',12);
ylim([0,34]);
s22.Position = s22.Position + [-0.14 0.06 0.0 0.0];
set(gca,'XTickLabel',names_fat,'FontSize',12);
title('SNR efficiency of ROIs in fatty tissues','FontSize',14);
ay22 = gca;
ay22.YTick = 0:6:30;
set(ay22);
xBar22=cell2mat(get(b22,'XData')).' + [b22.XOffset];  % compute bar centers
hold on;
errorbar(xBar22,median_SNR_eff_fat,MAD_SNR_eff_fat,'k.');
for i = 1:length(p_SNR_eff_fat)
    sigstar({[xBar22(i,1),xBar22(i,2)], [xBar22(i,1),xBar22(i,3)]}, [p_SNR_eff_fat(i,2),p_SNR_eff_fat(i,3)])
%     sigstar({[xBar11(i,1),xBar11(i,2)]}, p_SNR_eff_fat(i,2))
end
legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);legend({'SMURF','long-TR Dixon','short-TR Dixon'},'FontSize',12);
% legend({'SMURF','Dixon'},'FontSize',12);


b22(1).FaceColor = [136/255,204/255,255/255];
b22(2).FaceColor = [255/255,205/255,151/255];
b22(3).FaceColor = [255/255,153/255,151/255];
b11(1).FaceColor = b22(1).FaceColor;
b11(2).FaceColor = b22(2).FaceColor;
b11(3).FaceColor = b22(3).FaceColor;
b21(1).FaceColor = b22(1).FaceColor;
b21(2).FaceColor = b22(2).FaceColor;
b21(3).FaceColor = b22(3).FaceColor;
b12(1).FaceColor = b22(1).FaceColor;
b12(2).FaceColor = b22(2).FaceColor;
b12(3).FaceColor = b22(3).FaceColor;

disp('Finished');

warning on MATLAB:divideByZero
