clc,clear,close all;
data = load('/net/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/paper/additional_phase_influence/ROIs_GE_phase_comb_methods.mat');
data_trans = data.ROIs_GE_phase_comb_methods.';
means = data_trans (1:2:end,:); 
stds = data_trans (2:2:end,:); 

figure 
errorbar(1:10,means(1,:),stds(1,:),':.k','MarkerSize',15); % HIP
hold;
errorbar(1:10,means(2,:),stds(2,:),':.r','MarkerSize',15); % ROEMER (echo 1)
errorbar(1:10,means(3,:),stds(3,:),':.b','MarkerSize',15); % ROEMER (echo 2)
errorbar(1:10,means(4,:),stds(4,:),':.g','MarkerSize',15); % ASPIRE (echo 1)
errorbar(1:10,means(5,:),stds(5,:),':.y','MarkerSize',15); % ASPIRE (echo 2)

legend('HIP', 'ROEMER (echo 1)','ROEMER (echo 2)','ASPIRE (echo 1)','ASPIRE (echo 2)'); 
xlabel('ROI','FontSize',11);
ylabel('Mean susceptibility over ROI [ppm]','FontSize',11);
title('Comparison of estimated susceptibilities','FontSize',11);
xtix = {'1','2','3','4','5','6','7','8','9','10'};   % Your labels
xtixloc = [1 2 3 4 5 6 7 8 9 10];      % Your label locations
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);

anova1(means.');
xtix = {'HIP', 'ROEMER (echo 1)','ROEMER (echo 2)','ASPIRE (echo 1)','ASPIRE (echo 2)'};   % Your labels
xtixloc = [1 2 3 4 5];      % Your label locations
set(gca,'XTickMode','auto','XTickLabel',xtix,'XTick',xtixloc);
ylabel('Mean susceptibility over ROI [ppm]','FontSize',11);
title('Analysis of variance','FontSize',11);

