function [T2map_f,T2map_w] = T2mapping_fromVlado_func(fat, water, output_dir, TEs, is7T)
% BB: T2 mapping fucntion based on script from Vlado Juras

for i = 1:2
    if i == 1
        data = fat;
        name = 'fat';
        T2min = 0;
        if (is7T)
            T2max = 100;
        else
            T2max = 200;
        end

    else 
        data = water;   
        name = 'water';
        T2min = 0;
        if (is7T)
            T2max = 100;
        else
            T2max = 200;
        end
    end

    writefile = fullfile(output_dir,sprintf('t2Map_%s_%ito%i.nii',name,T2min,T2max));

    data = squeeze(sqrt(sum((abs(data).^2),1))); % combined over channels

    dimx = size(data,1);
    dimy = size(data,2);
    nrSlices = size(data,3);
    uEchoTimes = TEs';

    T2maps = zeros(dimx, dimy, nrSlices);
    disp('Calculating T2 maps...');    
   
    for S = 1:nrSlices
        slice = squeeze(data(:,:,S,:));
        sliceR = log(slice);
        sliceR = reshape(sliceR, [], size(uEchoTimes,1))'; % reshape and transpose to 16 by N
        X = uEchoTimes(:);
        X = X - mean(X);
        b_vectorized = [ones(size(uEchoTimes,1),1) X] \ sliceR; % solve all once       

        T2values = -1./b_vectorized(2,:);
        T2mapVec = reshape(T2values,dimx,dimy); 
        T2maps(:,:,S) = T2mapVec; 
    end
    
%     for X=1:dimx
%          for Y=1:dimy
%              for Z=1:nrSlices
%                   x = uEchoTimes;
%                   y = (squeeze(data(X,Y,Z,:)));
%                   f = @(b,x) b(1).*exp(-x./b(2));          % Objective Function (exponential)
%                   options = optimset('MaxFunEvals',1000, 'display','off');
%                   B = fminsearch(@(b) norm(y - f(b,x)), [x(1); 50.0], options)  ;                % Estimate Parameters 
%                   T2maps(X,Y,Z) = B(2);
%              end
%          end
%     end
                
    T2maps(isnan(T2maps)) = 0;
    T2maps(isinf(T2maps)) = 0;   
    T2maps(T2maps<0) = T2max;

    T2maps(T2maps>T2max) = T2max;
    T2maps(T2maps<T2min) = T2min;
        
%     save_nii(make_nii(T2maps), writefile);

    if i == 1
         T2map_f = T2maps;
    else
         T2map_w = T2maps;
    end
end






