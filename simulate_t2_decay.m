clear all

%% Simulation T2 relaxation
M0 = 1;
T2s = 15;
T2 = 30;

% T2 decay
TEs = 0:0.1:(5*T2);
for iTE = 1:length(TEs)    
    TE = TEs(iTE);
    t2sdecay = M0 .* exp(-TE./T2s);
    t2ssignal(iTE) = t2sdecay;
    t2decay = M0 .* exp(-TE./T2);
    t2signal(iTE) = t2decay;
end

figure;
plot(TEs,abs(t2signal),'color',[0/255,11/255,158/255],'LineWidth',1.5)
hold on
plot(TEs,abs(t2ssignal),'color',[10/255,150/255,255/255],'LineWidth',1.5)
xticks([T2 2*T2 3*T2 4*T2 5*T2])
xticklabels({'T_2','2T_2','3T_2','4T_2','5T_2'})
yticks(0:0.2:1)
ax = gca;
ax.FontSize = 12;
ylabel('M_x_y/M_0','FontSize',14)
legend('T_2 relaxation','T_2^* relaxation')




%% Simulation T1 relaxation
M0 = 1;
T1 = 1000;

% T1 decay
TRs = 0:0.1:5*T1;
for iTR = 1:length(TRs)    
    TR = TRs(iTR);
    t1decay = M0-M0 .* exp(-TR./T1);
    t1signal(iTR) = t1decay;
end

figure;
plot(TRs,abs(t1signal),'color',[0/255,11/255,158/255],'LineWidth',1.5)
xticks([T1 2*T1 3*T1 4*T1 5*T1])
xticklabels({'T_1','2T_1','3T_1','4T_1','5T_1'})
yticks(0:0.2:1)
ax = gca;
ax.FontSize = 12;
ylabel('M_z/M_0','FontSize',14)
legend('T_1 relaxation','Location','northwest')





%% Simulate inversion recovery
M0 = 1;
T1w = 1000;
T1f = 400;

% T1 decay
TRs = 0:10:5*T1w;
for iTR = 1:length(TRs)
    TR = TRs(iTR);
    t1wdecay = 2*M0.*(1-exp(-TR./T1w));
    t1wsignal(iTR) = t1wdecay;
    t1fdecay = 2*M0.*(1-exp(-TR./T1f));
    t1fsignal(iTR) = t1fdecay;
end

figure;
plot(TRs,abs(t1wsignal)-1,'color',[0/255,11/255,158/255],'LineWidth',1.5)
hold on;
plot(TRs,abs(t1fsignal)-1,'color',[10/255,150/255,255/255],'LineWidth',1.5)
plot(TRs,zeros(1,length(TRs)),'k')
xticklabels({})
yticks(-1:0.5:1)
ax = gca;
set(gca,'xtick',[])
ax.FontSize = 12;
ylabel('M_z/M_0','FontSize',14)
set(gca,'XColor','none')