clear, clc

%% Set common
% main_dir='/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/19860423TTKR_20190912/QSM/3D/relaxDiffsCorr/';
% TE = 12.2; %ms
% resolution = [1.1 1.1 1.1];

main_dir='/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/QSM_SMM/19850428BNHG_20201116_headneck/';
% TE = 9.1; %ms
resolution = [1 1 1];

phase_filename = 'combined_phase.nii';
% mask_filename = 'mask_nice.nii';
unwrapped_filename = 'combined_phase_laplacianUnwrapped.nii';
%fm_filename = 'fm_laplacian.nii';

%% Set different
for scan = 8
      
    switch scan 
        case 1 
            sub_dir='recombined_type1Type2RelaxCorr/';
        case 2
            sub_dir='recombined_type1Type2Corr/';
        case 3
            sub_dir='recombined_type2Corr/';
        case 4
            sub_dir='recombined_notCorr/';
        case 5
            sub_dir='TE9p1/';
        case 6
            sub_dir='TE9p1_rBW600/';
        case 7
            sub_dir='3echoes_first2TE/recombined_type1Type2RelaxCorr/';
        case 8
            sub_dir='aspire/5inPhaseEcho_interp/recombined_type2Corr_type1Corr/results/';
                        
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    data_dir = fullfile(main_dir, sub_dir);
    phase_load = fullfile(data_dir, phase_filename);
    phase_save = fullfile(data_dir, unwrapped_filename);
%     mask = fullfile(data_dir, mask_filename);

%     %% Make directory for results
%     if ~exist(output_dir, 'dir')
%         mkdir(fullfile(main_dir, output_dir))
%     end

    %% Load data
    phase_nii = load_nii(phase_load);
    phase = phase_nii.img();
%     mask_nii = load_nii(mask);
%     mask = double(mask_nii.img());

    %% Unwrap phase
    for iEcho = 1:size(phase,4)
    %     unwrapped = laplacianUnwrap(phase, mask);
        unwrapped(:,:,:,iEcho) = lp_unwrap_sr(phase(:,:,:,iEcho), resolution);
    end
    save_nii(make_nii(unwrapped), phase_save);

%     %% Rescale phase to fieldmap
%     fm = unwrapped ./ (2*pi*TE/1000);
%     save_nii(make_nii(fm), fullfile(output_dir, fm_filename));

    sprintf('***finished case %i ***\n',scan)

end 