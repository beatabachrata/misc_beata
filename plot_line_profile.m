% clear all
% 
% root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/superresolution/';
% subj = '19930606DABN_20211018_SR';
% figure; hold on
% 
% slice_nr = 110;
% y_pos = 90; %from bottom left corner with mricro    
% colors = {[0,128,255]/255,[0,200,0]/255,[255,0,128]/255};
% 
% for image = 1:3
%     
%     switch image 
%         case 1
%             file = 'gre/2D/mag.nii';
% %             file = 'QSM_new/gre/2D/orig/romeo_pdf_star_ivw/sepia_QSM.nii';
%             crop_x = 0;
%         case 2
%             file = 'gre/2D/sim_triplanar/average_mag_resample_MC_nii_zshift.nii';
% %             file = 'QSM_new/gre/2D/sim_triplanar/sr/minc_pipeline/romeo_pdf_star_ivw/sepia_QSM.nii';
%             crop_x = 128;
%         case 3
%             file = 'gre/2D/sim_triplanar/volgenmodel_nonlin/warpped_mag.nii';
% %             file = 'QSM_new/gre/2D/sim_triplanar/sr/volgenmodel_nonlin/romeo_pdf_star_ivw/sepia_QSM.nii';
%             crop_x = 128;
%     end
% 
%     filename=fullfile(root_dir,subj,file);   
%     nii=load_nii(filename);
%     img=nii.img;
%     
%     % Generate rectangular image
%     if (crop_x)
%         dims = size(img);
%         img_new=img((((dims(1)-crop_x)/2+1):((dims(1)-crop_x)/2+crop_x)),:,:,:);
%         img = img_new;
%     end
%     
%     % Plot of the profiles
%     plot(img(:,y_pos,slice_nr),':','Color',colors{image},'LineWidth',2)     
% end
% 
% ax = gca;
% ay = gca;
% ax.FontSize = 20;
% ay.FontSize = 20;
% legend({'Reference HR GRE (reference)','SR GRE from LR triplanar (lin)','SR GRE from LR triplanar (non-lin VM)'},'FontSize',22);
% ylabel('Image intensity [a.u.]','FontSize',30)
% title('Profile of image intensity values','FontSize',35),
% % ylabel('Susceptibility value [ppm]','FontSize',30)
% % title('Profile of susceptibility values','FontSize',35),
% xlabel('Voxel number along x-axis','FontSize',30)
% 




clear all

root_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/superresolution/';
subj = '19701007SMRB_20220412_SR';
figure; hold on

slice_nr = 133;
y_pos = 90; %from bottom left corner with mricro    
unzip = false;

colors = {[0,128,255]/255,[0,200,0]/255,[255,0,128]/255,[255,140,0]/255,[0,0,255]/255};


for image = 1:5
    
    switch image 
        case 1
%             file = 'gre/2D/mag_echo7.nii';
            file = 'QSM/epi/sr/3orient/median_lf/qsm_new_208.nii';
            crop_x = 0;
        case 2
%             file = 'epi/siemensMB/iso/sagittal_PA/epi_mag_DC_VSM.nii';
            file = 'QSM/epi/sr/3orient/mean_lf/qsm_new_208.nii';
            crop_x = 0;
        case 3
%             file = 'epi/siemensMB/average_epi_mag_DC_VSM_echo1_resample_MC_nii_zshift.nii';
            file = 'QSM/epi/sr/6orient/median_lf/qsm_new_208.nii';
            crop_x = 0;
        case 4
%             file = 'epi/siemensMB/volgenmodel_nonlin/warpped_mag.nii';
            file = 'QSM/epi/sr/6orient/mean_lf/qsm_new_208.nii';
            crop_x = 0;
        case 5
%             file = 'epi/siemensMB/volgenmodel_nonlin/warpped_mag.nii';
            file = 'QSM/gre/3D/romeo_sharp_star_ivw/sepia_QSM_rect.nii';
            crop_x = 0;
    end

    filename=fullfile(root_dir,subj,file);   
    
    if (unzip)
        unzip_command = sprintf('gunzip %s',  unix_format(filename));
        [res, message] = unix(unzip_command);
        if (res)
            error(message);
        end     
    end
    
    nii=load_nii(filename);
    img=nii.img;
    
    % Generate rectangular image
    if (crop_x)
        dims = size(img);
        img_new=img((((dims(1)-crop_x)/2+1):((dims(1)-crop_x)/2+crop_x)),:,:,:);
        img = img_new;
    end
%     if (image > 1) % correction of CMRR magnitude
%         img = img./10;
%     end
    
    % Plot of the profiles
    plot(img(:,y_pos,slice_nr),':','Color',colors{image},'LineWidth',2)     
end

ax = gca;
ay = gca;
ax.FontSize = 20;
ay.FontSize = 20;
legend({'median_lf_3orient','mean_lf_3orient','median_lf_6orient','mean_lf_6orient','gre'},'FontSize',22);
% ylabel('Image intensity [a.u.]','FontSize',30)
% title('Profile of image intensity values','FontSize',35),
ylabel('Susceptibility value [ppm]','FontSize',30)
title('Profile of susceptibility values','FontSize',35),
xlabel('Voxel number along x-axis','FontSize',30)




