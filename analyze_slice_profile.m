%% Housekeeping & Definitions
clear
close all;

addpath(genpath('/bilbo/home/bbachrata/matlab'))

%% Define data paths and acquisition orientation
for scan = 1:8
        
    orient = 1; % 1-sag, 2-trans, 3-cor
    PE_dir = 1; % 1 = HF, 2 = AP, 3 = RL
  
    switch scan  
         case 1 
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SINC_opt_5mm_BWTP4p6';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00121_FID32844_bb_caipigre_SINC_optimized_TE11_3_FA90_5mm_rBW4_6.dat';
         case 2
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SINC_opt_5mm_BWTP3p3';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00120_FID32843_bb_caipigre_SINC_optimized_TE11_3_FA90_5mm_rBW3_3.dat';
         case 3 
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SINC_opt_5mm_BWTP2';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00119_FID32842_bb_caipigre_SINC_optimized_TE11_3_FA90_5mm_rBW2.dat';
         case 4
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SINC_opt_8mm_BWTP2';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00122_FID32845_bb_caipigre_SINC_optimized_TE11_3_FA90_8mm_rBW2.dat';
         case 5
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SINC_opt_8mm_BWTP7p5';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00123_FID32846_bb_caipigre_SINC_optimized_TE11_3_FA90_8mm_rBW7_5.dat';
         case 6
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SINC_opt_3mm_BWTP2p5';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00134_FID32857_bb_caipigre_SINC_optimized_TE11_3_FA90_3mm_rBW2_5.dat';
         case 7
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SINC_normal_3mm_BWTP7';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00133_FID32856_bb_caipigre_SINC_normal_TE11_3_FA90_3mm_rBW7.dat';
         case 8
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/DBSS_3p5mm_BWTP2';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00139_FID32862_bb_caipigre_DBSS_TE11_3_FA90_3_5mm_rBW2.dat';
         case 9
            output_dir = '/ceph/mri.meduniwien.ac.at/projects/radiology/fmri/data/bbachrata/analysis/SMM/gre/p_bb_20191104_sliceProfiles/sliceProfiles/SBSS_3p5mm_BWTP2';
            kspace_path = '/ceph/mri.meduniwien.ac.at/projects/radiology/acqdata/data/BB_sorting_spot/p_bb_20191104_sliceProfiles/dats/meas_MID00141_FID32864_bb_caipigre_SBSS_TE11_3_FA90_3_5mm_rBW2.dat';
   end
    
    %% Create output directory
    [s,mess] = mkdir(output_dir);
    fprintf('Writing results to %s\n', output_dir);
    if s == 0
        error('No permission to make directory %s/m', output_dir);
    end

    %% Load data and reshape
    kspaceStruct = mapVBVD(kspace_path);
    kspace = kspaceStruct{1,2}.image();
    kspace = permute(kspace,[2,1,3]);   
                       
    %% Get an image  
%     kspace_cha10 = kspace(10,:,:);
%     imagesc(abs(squeeze(kspace_cha10)))
    kspace_centre = kspace(:,:,65);
    ima = FFTOfMRIData_bb(kspace_centre,0,[2],1); 
    ima_SoS = sqrt(sum((abs(ima).^2),1));
    ima_SoS = ima_SoS((size(ima_SoS,2)*0.25 +1):(size(ima_SoS,2)*0.75));
    figure;plot(ima_SoS)
    
    ima = FFTOfMRIData_bb(kspace,0,[2 3],1); 
    ima_SoS = squeeze(sqrt(sum((abs(ima).^2),1)));
    ima_SoS = ima_SoS((size(ima_SoS,1)*0.25 +1):(size(ima_SoS,1)*0.75),:);
    save_nii(make_nii(ima_SoS),fullfile(output_dir,'sliceProfile.nii'));

end